package usa.synergy.minigame.listener;

import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.Module;
import usa.synergy.minigame.listener.events.PlayerJoinListener;

public class ListenerManager extends Module {

    public ListenerManager(Core plugin){
        super(plugin, "Listener Manager", false);

        registerListener(
                new PlayerJoinListener()
        );
    }

    @Override
    public void reload(String s) {

    }
}
