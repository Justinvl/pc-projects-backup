package usa.synergy.minigame.assets.object;

public enum GameStates {
    LOBBY,
    STARTED,
    RESTARTING,
    BUILD,
    SERVICE
}
