package usa.synergy.minigame.assets.object;

import lombok.Getter;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.scoreboard.ScoreboardPolicy;

import java.util.List;

public abstract class GameState {

    @Getter
    private List<Flag> flags;
    private boolean active = true;

    public GameState(){
        Core.getPlugin().getScoreboardManager().setScoreboardPolicy(scoreboard());

        init();

        Core.getPlugin().getRunnableManager().runTaskTimer("move check tick "+this.getClass().getSimpleName(), synergy -> {
            if (moveToNext()){
                deInit();
                active = false;
                Core.getPlugin().getRunnableManager().getRunnables().get("move check tick "+this.getClass().getSimpleName()).cancel();
            }
        }, 20, 20L);

        Core.getPlugin().getRunnableManager().runTaskTimerAsynchronously("state tick "+this.getClass().getSimpleName(), synergy -> {
            if (!active){
                Core.getPlugin().getRunnableManager().getRunnables().get("state tick "+this.getClass().getSimpleName()).cancel();
            }
            tick(synergy);
        }, 20, 20L);
    }

    public abstract GameState nextState();
    public abstract ScoreboardPolicy scoreboard();
    public abstract boolean moveToNext();
    public abstract void init();
    public abstract void tick(Core synergy);
    public abstract void deInit();

}
