package usa.synergy.minigame;

import lombok.Getter;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.api.SynergyPlugin;
import usa.synergy.minigame.assets.MinigameSettings;
import usa.synergy.minigame.countdown.CountdownManager;
import usa.synergy.minigame.listener.ListenerManager;

public class MinigameAPI extends SynergyPlugin {

    @Getter
    public static MinigameAPI api;
    @Getter
    public CountdownManager countdownManager;
    @Getter
    private MinigameSettings settings;

    @Override
    public String name() {
        return "Synergy's MinigameAPI";
    }

    @Override
    public void init() {
        this.api = this;
        Core synergy = Core.getPlugin();

        this.countdownManager = new CountdownManager(synergy);
        this.settings = new MinigameSettings(synergy);
        new ListenerManager(synergy);
    }

    @Override
    public void deinit() {

    }


}
