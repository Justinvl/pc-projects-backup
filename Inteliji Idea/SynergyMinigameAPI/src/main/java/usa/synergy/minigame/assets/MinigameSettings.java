package usa.synergy.minigame.assets;

import lombok.Getter;
import lombok.Setter;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.Module;

public class MinigameSettings extends Module {

    @Getter @Setter
    public String joinMessage = "{P} has joined the team";
    @Getter
    public int maxPlayers = 4, minPlayers = 1;

    public MinigameSettings(Core plugin){
        super(plugin, "Minigame Settings Manager", false);
    }

    @Override
    public void reload(String s) {

    }

}
