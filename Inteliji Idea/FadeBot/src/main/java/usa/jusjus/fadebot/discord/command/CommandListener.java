package usa.jusjus.fadebot.discord.command;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import usa.jusjus.fadebot.discord.Core;
import usa.jusjus.fadebot.discord.command.object.DiscordCommand;

public class CommandListener extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        if (event.getAuthor().isBot()) return;
        Message message = event.getMessage();
        String content = message.getContentRaw();
        MessageChannel channel = event.getChannel();
        Member member = event.getMember();
//        System.out.println("1");
//        System.out.println(event.getMessage());
        for(DiscordCommand command : Core.getCommandManager().getCommands()){
//            System.out.println("4");
//            System.out.println(content.toLowerCase().split(" ")[0]);
            if (command.getAliases().contains(content.toLowerCase().split(" ")[0])){
//                System.out.println("5");
                if (member.hasPermission(command.getRank().getHighestPermission())) {
//                    System.out.println("6");
                    command.execute(member, channel, message);
                }
            }
        }
    }

}
