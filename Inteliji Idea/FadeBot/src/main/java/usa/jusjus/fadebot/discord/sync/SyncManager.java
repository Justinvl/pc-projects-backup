package usa.jusjus.fadebot.discord.sync;

import usa.jusjus.fadebot.discord.Core;
import usa.jusjus.fadebot.discord.DiscordModule;
import usa.jusjus.fadebot.discord.sync.command.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class SyncManager extends DiscordModule {

    public SyncManager(){
        super("Sync Manager");

        registerCommands(
                new CommandSync()
        );
    }

    public void setSynced(boolean synced, String username){
        Core.getDatabaseManager().update("syncdata", new HashMap<String, Object>(){{
            put("synced", true);
        }}, "username = '"+username+"'");
    }

    public boolean checkKey(String givenKey){
        try{
            ResultSet set = Core.getDatabaseManager().getResults("syncdata", "key=?", new HashMap<Integer, Object>(){{
                put(1, givenKey);
            }});
            if (set.next()){
                if (givenKey.matches(set.getString("key"))
                        &&!set.getBoolean("synced")){
                    System.out.println("Matches");
                    // Matches
                    return true;
                }else{
                    System.out.println("No Matches");
                    // Doesn't match
                    return false;
                }
            }else{
                System.out.println("No Key Found");
                // No key found
                return false;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

}
