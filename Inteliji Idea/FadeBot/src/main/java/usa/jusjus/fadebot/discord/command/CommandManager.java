package usa.jusjus.fadebot.discord.command;

import com.google.common.collect.Lists;
import lombok.Getter;
import usa.jusjus.fadebot.discord.DiscordModule;
import usa.jusjus.fadebot.discord.command.object.DiscordCommand;

import java.util.List;

public class CommandManager extends DiscordModule {

    @Getter
    private final List<DiscordCommand> commands = Lists.newArrayList();

    public CommandManager(){
        super("Command Manager");
        System.out.println("11");
        registerEventListeners(
            new CommandListener()
        );
    }

    public void addCommand(DiscordCommand discordCommand){
        System.out.println("Command added");
        if (!this.commands.contains(discordCommand)) {
            this.commands.add(discordCommand);
        }
    }

}
