package usa.jusjus.fadebot.spigot.sync.command;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import usa.jusjus.fadebot.spigot.Core;
import usa.synergy.utilities.assets.command.SynergyCommand;

public class CommandDiscord extends SynergyCommand {

    public CommandDiscord(Core core){
        super(core, "fade.discord", "", false, "discord");
    }

    @Override
    public void execute(Player player, String command, String[] args) {
        player.sendMessage("http://discord.gg/Tbt59dw");
    }

    @Override
    public void execute(ConsoleCommandSender sender, String command, String[] args) {

    }
}
