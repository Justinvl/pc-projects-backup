package usa.jusjus.fadebot.spigot.rewards.gui;

import usa.jusjus.fadebot.spigot.Core;
import usa.synergy.utilities.assets.gui.Gui;
import usa.synergy.utilities.assets.gui.object.GuiSize;

public class RewardGUI extends Gui {

    public RewardGUI(Core plugin){
        super(plugin, "", GuiSize.SIX_ROWS);
    }

    @Override
    public void setup() {

    }
}
