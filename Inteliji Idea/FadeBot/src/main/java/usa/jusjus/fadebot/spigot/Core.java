package usa.jusjus.fadebot.spigot;

import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;
import usa.jusjus.fadebot.spigot.sync.SyncManager;
import usa.synergy.utilities.SynergyUtilitiesAPI;
import usa.synergy.utilities.assets.YMLFile;
import usa.synergy.utilities.assets.runnable.RunnableManager;
import usa.synergy.utilities.service.SQLService;
import usa.synergy.utilities.service.sql.DatabaseManager;
import usa.synergy.utilities.service.sql.SQLDataType;
import usa.synergy.utilities.service.sql.SQLDefaultType;
import usa.synergy.utilities.service.sql.TableBuilder;

import java.sql.SQLException;

public class Core extends JavaPlugin {

    @Getter
    public YMLFile settings;
    @Getter
    public DatabaseManager databaseManager;
    @Getter
    public SynergyUtilitiesAPI synergyUtilitiesAPI;
    @Getter
    public RunnableManager runnableManager;
    @Getter
    public SyncManager syncManager;
    @Getter
    public static Core instance;

    @Override
    public void onEnable() {
        instance = this;
        this.synergyUtilitiesAPI = new SynergyUtilitiesAPI(this);

        settings = new YMLFile("", "settings");

        try{
            databaseManager = new DatabaseManager(new SQLService(
                    "jdbc:mysql://127.0.0.1:3306",
                    "fadebot",
                    "root",
                    ""
            ), "fade");
            databaseManager.connect();

            new TableBuilder("syncdata", this.databaseManager)
                    .addColumn("uuid", SQLDataType.VARCHAR, 300, false, SQLDefaultType.NO_DEFAULT, true)
                    .addColumn("username", SQLDataType.VARCHAR, 100, false, SQLDefaultType.NO_DEFAULT, false)
                    .addColumn("key", SQLDataType.VARCHAR, 500, false, SQLDefaultType.NO_DEFAULT, false)
                    .addColumn("synced", SQLDataType.BIT,  -1, false, SQLDefaultType.CUSTOM.setCustom(Boolean.FALSE), false)
                    .execute();
        }catch (SQLException e){
            e.printStackTrace();
        }

        this.syncManager = new SyncManager(this);
    }
}
