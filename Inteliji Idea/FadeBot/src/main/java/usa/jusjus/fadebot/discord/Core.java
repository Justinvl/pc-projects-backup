package usa.jusjus.fadebot.discord;

import lombok.Getter;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import usa.jusjus.fadebot.discord.command.CommandManager;
import usa.jusjus.fadebot.discord.sync.SyncManager;
import usa.synergy.utilities.assets.JSONFile;
import usa.synergy.utilities.service.SQLService;
import usa.synergy.utilities.service.sql.DatabaseManager;

import javax.security.auth.login.LoginException;
import java.sql.SQLException;

public class Core {

    @Getter
    public static JDA JDA;
    @Getter
    public static CommandManager commandManager;
    @Getter
    public static DatabaseManager databaseManager;
    @Getter
    public static SyncManager syncManager;

    @Getter
    public static JSONFile settings;

    public static void main(String[] args) {
        try{
            JDA = new JDABuilder(AccountType.BOT).setToken("NjEyMjc3Mjg1OTMxOTc0NzEx.XVgB2w.-y7lGSS5yKGAxzFLeE0WyhcZWCg").build().awaitReady();

            // OPTIONS
            getJDA().setAutoReconnect(true);

            settings = new JSONFile("", "settings");
            try{
                databaseManager = new DatabaseManager(new SQLService(
                        "jdbc:mysql://127.0.0.1:3306",
                        "fadebot",
                        "root",
                        ""
                ), "fade");
                databaseManager.connect();

            }catch (SQLException e){
                e.printStackTrace();
            }
            commandManager = new CommandManager();
            syncManager = new SyncManager();
        }catch (LoginException|InterruptedException e){
            e.printStackTrace();
        }
    }

}
