package usa.jusjus.fadebot.spigot.sync;

import org.bukkit.entity.Player;
import usa.jusjus.fadebot.spigot.Core;
import usa.jusjus.fadebot.spigot.sync.command.CommandSync;
import usa.synergy.utilities.Module;
import usa.synergy.utilities.service.sql.DatabaseManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Random;

public class SyncManager extends Module {

    public SyncManager(Core core){
        super(core, "Sync Manager");

        registerCommand(
                new CommandSync(core)
        );
    }

    public String generateString(String username){
        StringBuilder builder = new StringBuilder();
        Random r = new Random();
        for (int i = 0; i < 6; i++) {
            if (i==3)
                builder.append(username.substring(0, 3).toUpperCase());
            char randomChar = generateRandomChar(r);
            builder.append(randomChar);
        }
        return builder.toString();
    }

    private char generateRandomChar(Random r){
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toLowerCase();
        return chars.charAt(r.nextInt(chars.length()));
    }

}
