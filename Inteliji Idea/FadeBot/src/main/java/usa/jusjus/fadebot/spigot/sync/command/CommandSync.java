package usa.jusjus.fadebot.spigot.sync.command;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import usa.jusjus.fadebot.spigot.Core;
import usa.jusjus.fadebot.spigot.sync.SyncManager;
import usa.synergy.utilities.assets.command.SynergyCommand;
import usa.synergy.utilities.assets.utilities.UtilString;
import usa.synergy.utilities.service.sql.DatabaseManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class CommandSync extends SynergyCommand {

    public CommandSync(Core core){
        super(core, "fade.sync", "", false, "sync");
    }

    @Override
    public void execute(Player player, String command, String[] args) {
        SyncManager sync = Core.getInstance().getSyncManager();

        DatabaseManager db = Core.getInstance().getDatabaseManager();
        String key = null;
        Core.getInstance().getSynergyUtilitiesAPI().getRunnableManager().runTaskAsynchronously("check", plugin -> {
            try{
                player.sendMessage("§m«---------------------------»");
                player.sendMessage(" ");
                player.sendMessage("§e§lYour discord Key:");
                ResultSet set = db.getResults("syncdata", "uuid=?", new HashMap<Integer, Object>(){{
                    put(1, player.getUniqueId().toString());
                }});
                if (set.next()){
                    player.sendMessage("§7"+set.getString("key"));
                }else{
                    String generated = sync.generateString(player.getName());
                    db.insert("syncdata", new HashMap<String, Object>(){{
                        put("uuid", player.getUniqueId().toString());
                        put("username", player.getName());
                        put("key", generated);
                    }});
                    player.sendMessage("§7"+generated);
                }
                player.sendMessage(" ");
                player.sendMessage("§m«---------------------------»");
            }catch (SQLException e){
                e.printStackTrace();
                player.sendMessage("§cCannot perform your request. Contact a administrator!");
            }
        });
    }

    @Override
    public void execute(ConsoleCommandSender sender, String command, String[] args) {

    }
}
