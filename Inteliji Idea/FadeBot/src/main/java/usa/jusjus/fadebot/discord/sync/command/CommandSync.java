package usa.jusjus.fadebot.discord.sync.command;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import usa.jusjus.fadebot.discord.Core;
import usa.jusjus.fadebot.discord.MessageBuilder;
import usa.jusjus.fadebot.discord.command.DiscordRank;
import usa.jusjus.fadebot.discord.command.object.DiscordCommand;

import java.awt.*;

public class CommandSync extends DiscordCommand {

    public CommandSync(){
        super("", DiscordRank.NONE, "/", "sync");
    }

    @Override
    public void execute(Member member, MessageChannel channel, Message message) {
        System.out.println(message.getContentRaw());

        String[] args = message.getContentRaw().split(" ");

        if (args.length == 3){
            String username = args[1], key = args[2];
            if (Core.getSyncManager().checkKey(key)){
                channel.sendMessage(new MessageBuilder(
                        "Successfully Synced with your minecraft account!",
                        "All of your ranks are synced with your discord rank. " +
                                "(So you should have your discord rank by now) If not? Contact an administrator!")
                        .overwriteColor(Color.GREEN)
                        .build()).queue();
                Core.getSyncManager().setSynced(true, username);
            }else{
                channel.sendMessage(new MessageBuilder(
                        "Cannot perform your request!",
                        "Or you have already synced your account? " +
                                "Or you don't have typed /sync in the server? " +
                                "Or you mistyped something? Which one is it?")
                        .overwriteColor(Color.RED)
                        .build()).queue();
            }
        }else{
            channel.sendMessage(new MessageBuilder("Cannot perform your request!", "/sync <minecraft username> <key>")
                    .overwriteColor(Color.RED)
                    .build()).queue();
        }
    }
}
