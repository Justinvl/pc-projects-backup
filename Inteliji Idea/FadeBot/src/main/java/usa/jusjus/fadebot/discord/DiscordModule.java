package usa.jusjus.fadebot.discord;

import lombok.Getter;
import usa.jusjus.fadebot.discord.command.object.DiscordCommand;

/*
Created by JusJus to make life easier
Copyright © 2019 - JusJus
*/

public abstract class DiscordModule {

    @Getter
    private String name;
    private Object[] listeners;

    public DiscordModule(String name){
        this.name = name;
    }

    public void registerEventListeners(Object... listeners){
        Core.getJDA().addEventListener(listeners);
    }

    public void registerCommands(DiscordCommand... discordCommands){
        for (DiscordCommand command : discordCommands){
            Core.getCommandManager().addCommand(command);
        }
    }

}
