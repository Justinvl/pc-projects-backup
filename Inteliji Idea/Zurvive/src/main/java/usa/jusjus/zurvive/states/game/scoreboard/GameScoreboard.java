package usa.jusjus.zurvive.states.game.scoreboard;

import usa.devrocoding.synergy.spigot.scoreboard.ScoreboardPolicy;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.wave.object.Wave;

import java.util.ArrayList;
import java.util.List;

public class GameScoreboard implements ScoreboardPolicy {

    @Override
    public List<String> getSidebar(SynergyUser synergyUser) {
        final int remaining = Zurvive.getInstance().getWaveManager().getCurrentWave().getRemainingEnemies();
        return new ArrayList<String>(){{
            add("   §726/09/2019");
            add(" ");
            add("§c§lWave "+ Wave.getWaveNumber());
            add("§7Enemies: §e"+ ( remaining <= 15 ? remaining : "???" ));
            add("§7Enemy Level: §cLevel 1");
            add("  ");
            add("§c"+"play.zurvive.net");
        }};
    }

    @Override
    public String getPrefix(SynergyUser synergyUser, SynergyUser synergyUser1) {
        return null;
    }

    @Override
    public String getSuffix(SynergyUser synergyUser, SynergyUser synergyUser1) {
        return null;
    }

    @Override
    public String getUndername(SynergyUser synergyUser) {
        return null;
    }

    @Override
    public int getUndernameScore(SynergyUser synergyUser, SynergyUser synergyUser1) {
        return 0;
    }

    @Override
    public String getTablist(SynergyUser synergyUser) {
        return null;
    }

    @Override
    public int getTablistScore(SynergyUser synergyUser, SynergyUser synergyUser1) {
        return 0;
    }
}
