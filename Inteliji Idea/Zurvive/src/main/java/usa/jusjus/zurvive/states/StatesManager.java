package usa.jusjus.zurvive.states;

import lombok.Getter;
import org.bukkit.Bukkit;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.Module;
import usa.devrocoding.synergy.spigot.utilities.UtilTime;
import usa.jusjus.zurvive.states.game.listener.MechanicListener;
import usa.jusjus.zurvive.states.lobby.LobbyState;
import usa.jusjus.zurvive.states.lobby.listener.UserLoadListener;
import usa.jusjus.zurvive.states.lobby.scoreboard.LobbyScoreboard;
import usa.synergy.minigame.assets.object.GameState;

public class StatesManager extends Module {

    @Getter
    public GameState state;

    public StatesManager(Core plugin){
        super(plugin, "States Manager", false);

        registerListener(
                new UserLoadListener(),
                new MechanicListener()
        );

        this.state = new LobbyState();
    }

    @Override
    public void reload(String s) {

    }

    public LobbyState getLobbyState(){
        try{
            return ((LobbyState) this.state);
        }catch (Exception e){
            Synergy.error("Trying to get Lobbystate while not initialized");
            return null;
        }
    }

}
