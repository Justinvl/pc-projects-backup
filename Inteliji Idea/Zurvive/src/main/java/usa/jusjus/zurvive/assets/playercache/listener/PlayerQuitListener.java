package usa.jusjus.zurvive.assets.playercache.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import usa.jusjus.zurvive.assets.playercache.object.PlayerCache;
import usa.jusjus.zurvive.cosmetics.CosmeticManager;

public class PlayerQuitListener implements Listener {

    private CosmeticManager cosmeticManager;

    public PlayerQuitListener(CosmeticManager cosmeticManager){
        this.cosmeticManager = cosmeticManager;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e){
        PlayerCache playerCache = this.cosmeticManager.getPlayerCacheManager().getPlayerCache(e.getPlayer().getUniqueId());
        this.cosmeticManager.getPlayerCacheManager().getAllPlayerCaches().remove(e.getPlayer().getUniqueId());
        this.cosmeticManager.getOutfitManager().updateOutfits(playerCache);
    }

}
