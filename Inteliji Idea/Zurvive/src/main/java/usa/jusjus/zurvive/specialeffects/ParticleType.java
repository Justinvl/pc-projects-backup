package usa.jusjus.zurvive.specialeffects;

import org.bukkit.*;
import java.util.*;

public enum ParticleType
{
    Enchanted("Enchanted"), 
    FlameRings("Flame Rings"), 
    Hearts("In Love"),
    RainCloud("Rain Cloud"), 
    Sparklez("Sparklez"), 
    Vortex("Vortex");
    
    private String name;
    public static final Map<ParticleType, List<Location>> locs;
    
    private ParticleType(final String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void Activate(final Location loc) {
        final List<Location> list = this.getLocations();
//        if (this == ParticleType.Rainbow) {
//            list.clear();
//        }
        if (!this.isActive(loc)) {
            list.add(new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ()));
            ParticleType.locs.put(this, list);
        }
    }
    
    public void deActivate() {
        ParticleType.locs.remove(this);
    }
    
    public static String serialize(final List<ParticleType> particles) {
        String s = "";
        for (int i = 0; i < particles.size(); ++i) {
            String add = ":";
            if (i == 0) {
                add = "";
            }
            s = s + add + particles.get(i);
        }
        return s;
    }
    
    public boolean isActive(final Location loc) {
        return ParticleType.locs.containsKey(this) && ParticleType.locs.get(this).contains(loc);
    }
    
    public List<Location> getLocations() {
        if (!ParticleType.locs.containsKey(this) || ParticleType.locs.get(this) == null) {
            return new ArrayList<Location>();
        }
        return ParticleType.locs.get(this);
    }
    
    public static List<ParticleType> deserialize(final String s) {
        final List<ParticleType> l = new ArrayList<ParticleType>();
        final List<String> tmp = Arrays.asList(s.split(":"));
        for (final String cur : tmp) {
            if (cur == null) {
                continue;
            }
            if (cur.equals("")) {
                continue;
            }
            final ParticleType particleType = zylemValueOf(cur);
            if (particleType == null) {
                continue;
            }
            l.add(particleType);
        }
        return l;
    }
    
    public static ParticleType zylemValueOf(final String name) {
        for (final ParticleType particleType : values()) {
            if (particleType.name().equals(name)) {
                return particleType;
            }
        }
        return null;
    }
    
    static {
        locs = new HashMap<ParticleType, List<Location>>();
    }
}
