package usa.jusjus.zurvive.guns.listener;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.guns.GunManager;
import usa.jusjus.zurvive.guns.event.EnemyHitEvent;
import usa.jusjus.zurvive.guns.object.Gun;

import java.util.ArrayList;
import java.util.List;

public class GunShootListener implements Listener {

    @EventHandler
    public void onGunShoot(PlayerInteractEvent e){
        GunManager gm = Zurvive.getInstance().getGunManager();
        if (e.getItem() == null||!e.getItem().hasItemMeta()||!e.getItem().getItemMeta().hasDisplayName()) return;

        if (gm.getGuns().containsKey(e.getItem().getItemMeta().getDisplayName())) {

            // Cancel the use of the item
            if (e.getAction() == Action.RIGHT_CLICK_BLOCK )
                e.setCancelled(true);

            // Get the gun the player is holding
            Gun gun = gm.getGuns().get(e.getItem().getItemMeta().getDisplayName());


            // Reload the gun when they right click
            if (e.getAction() == Action.LEFT_CLICK_BLOCK
                    || e.getAction() == Action.LEFT_CLICK_AIR){
                if (gun.isReloading(e.getPlayer())
                        || e.getItem().getAmount() >= gun.magazineAmmo()){
                    return;
                }
                e.setCancelled(true);
                gun.reloadGun(e.getPlayer(), e.getItem());
            }

            if (e.getAction() != Action.RIGHT_CLICK_AIR) return;

            if (!(gun.getPlayerCooldown().containsKey(e.getPlayer())) ||
                    (gun.getPlayerCooldown().containsKey(e.getPlayer())&&!gun.getPlayerCooldown().get(e.getPlayer()).contains(gun))){

                SynergyUser user = Core.getPlugin().getUserManager().getUser(e.getPlayer().getUniqueId());

                // Set the clip ammo
                if (e.getItem().getAmount() <= 1){

                    // Show the reload animation
                    if (gun.isReloading(e.getPlayer())){
                        return;
                    }
                    gun.reloadGun(e.getPlayer(), e.getItem());
                }else{
                    e.getItem().setAmount(e.getItem().getAmount()-1);
                }

                // Shoot the projectile
                {
                    Location eye = e.getPlayer().getEyeLocation();
                    double length = gun.range()*2;
                    for(double d = 0; d <= length; d++){
                        eye.add(eye.getDirection().multiply(0.7));
                        e.getPlayer().getWorld().spawnParticle(gun.particleBullet(), eye, 0);
                    }
                    e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_FIREWORK_ROCKET_BLAST, 1, 1);

                    LivingEntity entity = Zurvive.getInstance().getGunManager().getTarget(e.getPlayer(), length);
                    if (entity != null){
                        Zurvive.getInstance().getServer().getPluginManager().callEvent(new EnemyHitEvent(
                                entity, e.getPlayer(), gun, user
                        ));
                    }
                }


                // Initiate the cooldown
                List<Gun> guns = gun.getPlayerCooldown().containsKey(e.getPlayer())
                        ? gun.getPlayerCooldown().get(e.getPlayer())
                        : new ArrayList<>();

                guns.add(gun);
                gun.getPlayerCooldown().put(e.getPlayer(), guns);

                // Remove the cooldown async
                Core.getPlugin().getRunnableManager()
                        .runTaskLaterAsynchronously("remove player cooldown", javaPlugin -> {
                    if (gun.getPlayerCooldown().containsKey(e.getPlayer())){
                        gun.getPlayerCooldown().get(e.getPlayer()).remove(gun);
                    }
                }, (long)(20*gun.cooldown()));
            }
        }
    }

}
