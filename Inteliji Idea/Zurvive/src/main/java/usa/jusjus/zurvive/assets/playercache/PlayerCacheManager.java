package usa.jusjus.zurvive.assets.playercache;

import lombok.Getter;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.Module;
import usa.jusjus.zurvive.assets.playercache.object.PlayerCache;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerCacheManager extends Module {

    @Getter
    private Map<UUID, PlayerCache> allPlayerCaches = new HashMap<>();

    public PlayerCacheManager(Core plugin){
        super(plugin, "PlayerCache Manager", false);
    }

    @Override
    public void reload(String s) {

    }

    public PlayerCache getPlayerCache(UUID uuid){
        System.out.println(allPlayerCaches);
        return this.allPlayerCaches.get(uuid);
    }

    public void addCache(UUID uuid, PlayerCache playerCache){
        Synergy.debug("ADDED TO CACHE");
        this.allPlayerCaches.putIfAbsent(uuid, playerCache);
    }
}
