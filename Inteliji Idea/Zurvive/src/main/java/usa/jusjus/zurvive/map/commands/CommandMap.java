package usa.jusjus.zurvive.map.commands;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.assets.C;
import usa.devrocoding.synergy.spigot.command.SynergyCommand;
import usa.devrocoding.synergy.spigot.user.object.MessageModification;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.map.MapManager;
import usa.jusjus.zurvive.map.object.Area;
import usa.jusjus.zurvive.map.object.Map;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class CommandMap extends SynergyCommand {

    public CommandMap(Core plugin){
        super(plugin, "", false, "map");

        setPlayerUsage("[setting]");
    }

    /*
        - /map check
        - /map arealist
        - /map addarea
        - /map editarea <id>
        - /map setlobbyspawn
        - /map setgamespawn
        - /map setshowcasespawn
     */

    @Override
    public void execute(SynergyUser synergyUser, Player player, String s, String[] strings) {
        if (strings.length > 0 && strings.length < 4){
            MapManager mapManager = Zurvive.getInstance().getMapManager();

            //TODO: Is map in maintenance mode, disable this command

            if (strings[0].equalsIgnoreCase("arealist")) {
                List<String> builder = new ArrayList<>();

                builder.add(C.getLineWithName("Areas"));
                if (mapManager.getMap().getAreas().size() <= 0) {
                    builder.add("There are no area's! Use §c/map addarea");
                } else {
                    for (Area area : mapManager.getMap().getAreas()) {
                        builder.add("Area §e#" + area.getId());
                    }
                }
                builder.add(C.getLine());

                synergyUser.sendModifactionMessage(
                        MessageModification.RAW,
                        builder
                );
            }else if (strings[0].equalsIgnoreCase("check")){
                List<String> builder = new ArrayList<>();

                builder.add(
                        mapManager.getMap().getGameSpawn() == null
                                ? "§c× Game Spawn hasn't been set yet"
                                : "§a√ The game spawn has been set"
                );
                builder.add(
                        mapManager.getMap().getLobbySpawn() == null
                                ? "§c× Wait Lobby hasn't been set yet"
                                : "§a√ The wait spawn has been set"
                );
                builder.add(
                        mapManager.getMap().getShowcaseLocation() == null
                                ? "§c× Showcase Spawn hasn't been set yet"
                                : "§a√ The showcase spawn has been set"
                );
                builder.add(
                        mapManager.getMap().getAreas().size() <= 0
                        ? "§c× There are no areas found"
                        : "§a√ Found "+mapManager.getMap().getAreas().size()+" areas in this map"
                );

                int zombieSpawns = 0;
                for(Area area : mapManager.getMap().getAreas()){
                    if (area.getEnemySpawns().size() > 0){
                        zombieSpawns =+ area.getEnemySpawns().size();
                    }
                }
                builder.add(
                        mapManager.getMap().getAreas().size() <= 0
                                ? "§c× There are no enemy spawns set yet"
                                : "§a√ Found "+zombieSpawns+" enemyspawns in total"
                );

                synergyUser.sendModifactionMessage(
                        MessageModification.RAW,
                        C.getLineWithName("Map Status"),
                        "§9Map: §eZurvive",
                        " "
                );
                synergyUser.sendModifactionMessage(MessageModification.RAW, builder);
                synergyUser.sendModifactionMessage(MessageModification.RAW,C.getLine());
            }else if (strings[0].equalsIgnoreCase("setlobbyspawn")){
                mapManager.getMap().setLobbySpawn(player.getLocation());
                synergyUser.info("Lobby Spawn configured!");
                mapManager.getMap().saveToCache();
            }else if (strings[0].equalsIgnoreCase("setgamespawn")){
                mapManager.getMap().setGameSpawn(player.getLocation());
                synergyUser.info("Game Spawn configured!");
                mapManager.getMap().saveToCache();
            }else if (strings[0].equalsIgnoreCase("setshowcasespawn")){
                mapManager.getMap().setShowcaseLocation(player.getLocation());
                synergyUser.info("Showcase Spawn configured!");
                mapManager.getMap().saveToCache();
            }else if (strings[0].equalsIgnoreCase("addarea")){
                if (strings.length == 2){
                    String id = strings[1];
                    Area area = new Area(id);
                    mapManager.getAreaCreators().put(synergyUser, area);
                    synergyUser.sendModifactionMessage(
                            MessageModification.RAW,
                            CommandArea.getHelp(area)
                    );
                }
            }else if (strings[0].equalsIgnoreCase("editarea")){
                if (strings.length == 2){
                    String id = strings[1];
                    Area area = mapManager.getAreaByID(id);
                    if (area == null){
                        synergyUser.error("Cannot find that area!");
                        return;
                    }
                    mapManager.getAreaCreators().put(synergyUser, area);
                    synergyUser.sendModifactionMessage(
                            MessageModification.RAW,
                            CommandArea.getHelp(area)
                    );
                }
            }
        }else{
            synergyUser.sendModifactionMessage(
                    MessageModification.RAW,
                    C.getLineWithName("Map Editor"),
                    "§e/map check §f- §7Checks the status of the map and gives info",
                    "§e/map arealist §f- §7Gives a list of areas",
                    "§e/map addarea <name>§f- §7Adds a new area",
                    "§e/map editarea <name> §f- §7Edit's an area with the given id",
                    "§e/map setlobbyspawn §f- §7Set's the wait lobby spawn",
                    "§e/map setgamespawn §f- §7Set's the game spawn",
                    "§e/map setshowcasespawn §f- §7Set's the showcase spawn",
                    C.getLine()
            );
        }
    }

    @Override
    public void execute(ConsoleCommandSender consoleCommandSender, String s, String[] strings) {

    }
}
