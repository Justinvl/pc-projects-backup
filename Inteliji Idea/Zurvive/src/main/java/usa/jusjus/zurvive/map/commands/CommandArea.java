package usa.jusjus.zurvive.map.commands;

import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.assets.C;
import usa.devrocoding.synergy.spigot.assets.wand.object.SelectorWand;
import usa.devrocoding.synergy.spigot.command.SynergyCommand;
import usa.devrocoding.synergy.spigot.hologram.Hologram;
import usa.devrocoding.synergy.spigot.hologram.object.HologramLine;
import usa.devrocoding.synergy.spigot.user.object.MessageModification;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.map.MapManager;
import usa.jusjus.zurvive.map.object.Area;

import java.util.ArrayList;
import java.util.List;

public class CommandArea extends SynergyCommand {

    public CommandArea(Core plugin){
        super(plugin, "", false, "area");

        setPlayerUsage("[setting]");
    }

    @Override
    public void execute(SynergyUser synergyUser, Player player, String s, String[] strings) {
        MapManager mapManager = Zurvive.getInstance().getMapManager();
        if (!mapManager.getAreaCreators().containsKey(synergyUser)){
            synergyUser.error("You're not in the area edit mode!");
            return;
        }
        Area area = mapManager.getAreaCreators().get(synergyUser);

        if (strings.length > 0 && strings.length < 4) {
            if (strings[0].equalsIgnoreCase("cancel")) {
                mapManager.getAreaCreators().remove(synergyUser);
                synergyUser.error("Disabled area edit mode and removed area '"+area.getId()+"'.");
            }else if (strings[0].equalsIgnoreCase("save")) {
                if (mapManager.saveArea(area)){
                    synergyUser.info("Saved area "+area.getId());
                    mapManager.getAreaCreators().remove(synergyUser);
                    return;
                }
                synergyUser.error("Cannot save '"+area.getId()+"' because everything is not been setup!");
            }else if (strings[0].equalsIgnoreCase("sethologram")) {
//                area.setHologram(getPlugin().getHologramManager().createPublicHologram(player.getLocation(), area.getHologramLines(), player1 -> true));
                area.setHologram(getPlugin().getHologramManager().createHologram(
                        player.getLocation(),
                        new HologramLine() {
                            @Override
                            public String getMessage(SynergyUser synergyUser) {
                                return "Click to open";
                            }
                        },
                null
                ));
                synergyUser.info("Hologram has been setup for area "+area.getId());
            }else if (strings[0].equalsIgnoreCase("setdoor")) {
                SelectorWand wand = new SelectorWand();
                wand.give(synergyUser);
                synergyUser.info("Select the door for your area with the 'Selection Wand'");
            }else if (strings[0].equalsIgnoreCase("addenemyspawn")) {
                area.getEnemySpawns().add(player.getLocation());
                synergyUser.info("Added your location as an enemy spawn.");
            }
        }else{
            synergyUser.sendModifactionMessage(
                    MessageModification.RAW,
                    getHelp(area)
            );
        }
    }

    @Override
    public void execute(ConsoleCommandSender consoleCommandSender, String s, String[] strings) {

    }

    public static List<String> getHelp(Area area){
        return new ArrayList<String>(){{
            boolean a = area.getId().equalsIgnoreCase("start");
            add(C.getLineWithName("Area Editor"));
            if (a){
                add("§c§lThis area is the START area!");
            }
            add("§9Area: §e"+area.getId());
            add(" ");
            if (!a) {
                add("§e/area sethologram §f- §7Sets the location for the hologram");
            }
            add("§e/area addenemyspawn §f- §7Add's an enemy spawn at your location");
            if (!a) {
                add("§e/area setdoor §f- §7Gives you the wand to select the door");
            }
            add("§e/area save §f- §7Saves the area");
            add("§e/area cancel §f- §7Cancels the area editor");
            add(C.getLine());
        }};
    }
}
