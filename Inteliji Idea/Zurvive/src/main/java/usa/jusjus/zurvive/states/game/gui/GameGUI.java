package usa.jusjus.zurvive.states.game.gui;

import org.bukkit.Material;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.gui.Gui;
import usa.devrocoding.synergy.spigot.gui.GuiElement;
import usa.devrocoding.synergy.spigot.gui.object.GuiSize;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.devrocoding.synergy.spigot.utilities.ItemBuilder;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.guns.gun.AK47;
import usa.jusjus.zurvive.guns.gun.Pistol;

public class GameGUI extends Gui {

    public GameGUI(Core plugin){
        super(plugin, "Player Inventory", GuiSize.SIX_ROWS);
    }

    @Override
    public void setup() {
        addElement(0, new GuiElement() {
            @Override
            public ItemStack getIcon(SynergyUser synergyUser) {
                return new ItemBuilder(Material.WOODEN_SWORD)
                        .setName("§c§lKnife")
                        .build();
            }

            @Override
            public void click(SynergyUser synergyUser, ClickType clickType) {

            }
        });
        addElement(1, new GuiElement() {
            @Override
            public ItemStack getIcon(SynergyUser synergyUser) {
                return Zurvive.getInstance().getGunManager().getGunByClass(AK47.class).getDynamicItem();
            }

            @Override
            public void click(SynergyUser synergyUser, ClickType clickType) {

            }
        });
        addElement(2, new GuiElement() {
            @Override
            public ItemStack getIcon(SynergyUser synergyUser) {
                return new ItemBuilder(Material.BARRIER)
                        .setName("§cWeapon Slot #2")
                        .build();
            }

            @Override
            public void click(SynergyUser synergyUser, ClickType clickType) {

            }
        });
        addElement(3, new GuiElement() {
            @Override
            public ItemStack getIcon(SynergyUser synergyUser) {
                return new ItemBuilder(Material.BARRIER)
                        .setName("§cWeapon Slot #3")
                        .build();
            }

            @Override
            public void click(SynergyUser synergyUser, ClickType clickType) {

            }
        });
    }
}
