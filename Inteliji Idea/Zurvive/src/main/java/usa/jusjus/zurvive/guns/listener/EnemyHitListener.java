package usa.jusjus.zurvive.guns.listener;

import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.assets.object.SynergyPeriod;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.assets.CooldownManager;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.guns.event.EnemyDeadEvent;
import usa.jusjus.zurvive.guns.event.EnemyHitEvent;

public class EnemyHitListener implements Listener {

    @EventHandler
    public void onEnemyHit(EnemyHitEvent e){
        LivingEntity entity = e.getEnemy();
        entity.damage((e.getGun().damage()*2)*3);
        entity.setCustomName(Zurvive.getInstance().getEntitiesManager().getHealthBarBasedOnHealth(entity.getHealth()));
        entity.setCustomNameVisible(true);

        CooldownManager cooldown = Core.getPlugin().getCooldownManager();

        if (e.getEnemy().getHealth() <= 0){
            if (cooldown.isOnCooldown(e.getEnemy())){
                return;
            }
            EnemyDeadEvent event = new EnemyDeadEvent(e.getEnemy(), e.getPlayer(), e.getGun(), e.getSynergyUser());
            Zurvive.getInstance().getServer().getPluginManager().callEvent(event);
            cooldown.addCooldown(e.getEnemy(), SynergyPeriod.SECOND.getPeriod()*10);
            //TODO: Make something that it won't call twice
        }
    }

}
