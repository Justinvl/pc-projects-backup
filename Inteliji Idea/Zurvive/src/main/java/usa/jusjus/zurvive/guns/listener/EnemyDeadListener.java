package usa.jusjus.zurvive.guns.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.guns.event.EnemyDeadEvent;
import usa.synergy.minigame.assets.object.GameStates;

public class EnemyDeadListener implements Listener {

    @EventHandler
    public void onEnemyDead(EnemyDeadEvent e){
        if (Zurvive.getInstance().getGameState() == GameStates.STARTED) {
            e.getPlayer().sendMessage("§cYou've killed §e"+  e.getEnemy().getType().name());
            Zurvive.getInstance().getWaveManager().getCurrentWave().withdrawRemaining();
        }
    }

}
