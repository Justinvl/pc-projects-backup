package usa.jusjus.zurvive.entities.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;

public class HealthRegenListener implements Listener {

    @EventHandler
    public void onEntityHealthRegen(EntityRegainHealthEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onEntityFallDamage(EntityDamageEvent e){
        if (e.getCause() == EntityDamageEvent.DamageCause.FALL){
            e.setCancelled(true);
            e.setDamage(0);
        }
    }

}
