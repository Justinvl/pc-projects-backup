package usa.jusjus.zurvive.entities;

import com.mojang.datafixers.DataFixUtils;
import com.mojang.datafixers.types.Type;
import lombok.Getter;
import net.minecraft.server.v1_14_R1.*;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_14_R1.CraftWorld;
import org.bukkit.event.entity.CreatureSpawnEvent;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.Module;
import usa.jusjus.zurvive.entities.listener.HealthRegenListener;
import usa.jusjus.zurvive.entities.enemies.ZurviveZombie;

import java.util.Map;

public class EntitiesManager extends Module {

    @Getter
    private String customName = "zurvive_zombie";

    @Getter
    public EntityTypes ZURVIVE_ZOMBIE;

    public EntitiesManager(Core plugin){
        super(plugin, "Entities Manager", false);

        registerListener(
                new HealthRegenListener()
        );

        ZURVIVE_ZOMBIE = register("zombie", EnumCreatureType.MONSTER, customName);
    }

    @Override
    public void reload(String s) {

    }

    @SuppressWarnings("unchecked")
    private EntityTypes register(String type, EnumCreatureType enumCreatureType, String customName){
        Map<String, Type<?>> types = (Map<String, Type<?>>) DataConverterRegistry.a().getSchema(DataFixUtils.makeKey(SharedConstants.a().getWorldVersion())).findChoiceType(DataConverterTypes.ENTITY).types();
        types.put("minecraft:" + customName, types.get("minecraft:"+type));
        EntityTypes.a<Entity> a = EntityTypes.a.a(ZurviveZombie::new, enumCreatureType);
        return IRegistry.a(IRegistry.ENTITY_TYPE, customName, a.a(customName));
    }

    public String getHealthBarBasedOnHealth(double health){
        String character = "❤";
        final int hp = ((int) health)/2;
        StringBuilder line = new StringBuilder();
        for(int i=0;i<5;i++){
            ChatColor color = ChatColor.RED;
            if (i>hp&&hp<=1){
                color = ChatColor.WHITE;
            }
            line.append(color+character);
        }
        return line.toString();
    }

    public void spawn(Location location, EntityTypes entityTypes){
        try{
            WorldServer world = ((CraftWorld)location.getWorld()).getHandle();
            Entity entity = entityTypes.b(
                    world,
                    null,
                    null, // custom name of entity
                    null, // player reference. used to know if player is OP to apply EntityTag NBT compound
                    new BlockPosition(location.getBlockX(), location.getBlockY(), location.getBlockZ()),
                    EnumMobSpawn.COMMAND,
                    true, // center entity on BlockPosition and correct Y position for Entity's height
                    false // not sure. alters the Y position. this is only ever true when using spawn egg and clicked face is UP
            );
            world.addEntity(entity, CreatureSpawnEvent.SpawnReason.CUSTOM);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
