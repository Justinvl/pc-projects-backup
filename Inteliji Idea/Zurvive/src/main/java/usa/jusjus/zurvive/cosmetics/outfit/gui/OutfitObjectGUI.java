package usa.jusjus.zurvive.cosmetics.outfit.gui;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.gui.Gui;
import usa.devrocoding.synergy.spigot.gui.GuiElement;
import usa.devrocoding.synergy.spigot.gui.object.GuiSize;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.devrocoding.synergy.spigot.utilities.ItemBuilder;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.assets.playercache.object.PlayerCache;
import usa.jusjus.zurvive.cosmetics.CosmeticManager;
import usa.jusjus.zurvive.cosmetics.outfit.object.Outfit;
import usa.jusjus.zurvive.cosmetics.outfit.object.OutfitCategory;

public class OutfitObjectGUI extends Gui {

    private OutfitCategory outfitCategory;
    private SynergyUser user;
    private CosmeticManager cosmeticManager;
    private PlayerCache playerCache;
    private int row = 2, column = 2;

    public OutfitObjectGUI(Core plugin, OutfitCategory outfitCategory, SynergyUser synergyUser, CosmeticManager cosmeticManager){
        super(plugin, ChatColor.stripColor(outfitCategory.getName()), GuiSize.SIX_ROWS, false);

        this.outfitCategory = outfitCategory;
        this.user = synergyUser;
        this.cosmeticManager = cosmeticManager;
        this.playerCache = cosmeticManager
                .getPlayerCacheManager()
                .getPlayerCache(synergyUser.getUuid());

        setup();
    }

    @Override
    public void setup() {
        line(0, 8, new GuiElement() {
            @Override
            public ItemStack getIcon(SynergyUser synergyUser) {
                return new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE)
                        .setName(" ")
                        .build();
            }

            @Override
            public void click(SynergyUser synergyUser, ClickType clickType) {

            }
        });
        addElement(0, getBackGuiElement(new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE)));

        for(Outfit outfit : this.outfitCategory.getOutfits()){
            addElement(row, new GuiElement() {
                @Override
                public ItemStack getIcon(SynergyUser synergyUser) {
                    ItemBuilder item = new ItemBuilder(Material.RED_STAINED_GLASS_PANE)
                            .setName("§cNot unlocked yet");
                    if (playerCache.getOutfits().contains(outfit)){
                        item = new ItemBuilder(Material.GREEN_STAINED_GLASS_PANE)
                                .setName("§a§lUNLOCKED");
                    }
                    return item.build();
                }

                @Override
                public void click(SynergyUser synergyUser, ClickType clickType) {

                }
            });
            for(ItemStack content : outfit.getContents(playerCache.getOutfits().contains(outfit))){
                row+=9;
                addElement(row, new GuiElement() {
                    @Override
                    public ItemStack getIcon(SynergyUser synergyUser) {
                        return content;
                    }

                    @Override
                    public void click(SynergyUser synergyUser, ClickType clickType) {
                        if (playerCache.getOutfits().contains(outfit)) {
                            if (playerCache.selectOutfit(outfit, synergyUser)) {
                                synergyUser.getPlayer().closeInventory();
                                synergyUser.info("Enjoy your new outfit ;)");
                            }
                        }else{
                            // Buy outfit
                        }
                    }
                });
            }

            if (playerCache.isOutfitSelected(outfit)){
                row+=9;
                addElement(row, new GuiElement() {
                    @Override
                    public ItemStack getIcon(SynergyUser synergyUser) {
                        return new ItemBuilder(Material.BARRIER)
                                .setName("§c§lRemove this outfit")
                                .addLore("So you will be wearing no outfit anymore ;)")
                                .build();
                    }

                    @Override
                    public void click(SynergyUser synergyUser, ClickType clickType) {
                        playerCache.removeSelectedOutfit(synergyUser.getPlayer().getInventory());
                        new OutfitObjectGUI(getPlugin(), outfitCategory, synergyUser, cosmeticManager)
                                .setBackGui(getBackGui())
                                .open(synergyUser.getPlayer());
                    }
                });
            }
            column++;
            row = column;
        }
    }

}
