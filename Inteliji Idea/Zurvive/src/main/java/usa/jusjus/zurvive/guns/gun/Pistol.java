package usa.jusjus.zurvive.guns.gun;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import usa.jusjus.zurvive.guns.object.Gun;

public class Pistol extends Gun {

    @Override
    public String name() {
        return "Pistol";
    }

    @Override
    public String[] description() {
        return new String[]{
                "This is your default weapon.",
                "Use it wisely!"
        };
    }

    @Override
    public Integer maxAmmo() {
        return -1;
    }

    @Override
    public Integer magazineAmmo() {
        return 10;
    }

    @Override
    public Sound sound() {
        return Sound.ENTITY_FIREWORK_ROCKET_BLAST;
    }

    @Override
    public double cooldown() {
        return 0.5;
    }

    @Override
    public long reloadSpeed() {
        return 3;
    }

    @Override
    public double range() {
        return 20;
    }

    @Override
    public double damage() {
        return 4;
    }

    @Override
    public Material material() {
        return Material.WOODEN_HOE;
    }

    @Override
    public Particle particleBullet() {
        return Particle.CRIT;
    }
}
