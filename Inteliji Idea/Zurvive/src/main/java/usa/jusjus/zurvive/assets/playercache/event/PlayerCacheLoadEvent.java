package usa.jusjus.zurvive.assets.playercache.event;

import lombok.Getter;
import usa.devrocoding.synergy.spigot.listeners.SynergyEvent;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.jusjus.zurvive.assets.playercache.object.PlayerCache;

public class PlayerCacheLoadEvent  extends SynergyEvent {

    @Getter
    private final PlayerCache playerCache;
    @Getter
    private SynergyUser synergyUser;

    public PlayerCacheLoadEvent(PlayerCache playerCache, SynergyUser synergyUser) {
        this.playerCache = playerCache;
        this.synergyUser = synergyUser;
    }

}
