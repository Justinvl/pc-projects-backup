package usa.jusjus.zurvive.cosmetics.outfit.outfits.monsters;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import usa.devrocoding.synergy.spigot.utilities.ItemBuilder;
import usa.devrocoding.synergy.spigot.utilities.LeatherArmorBuilder;
import usa.devrocoding.synergy.spigot.utilities.SkullItemBuilder;
import usa.jusjus.zurvive.cosmetics.outfit.object.Outfit;
import usa.jusjus.zurvive.cosmetics.outfit.object.OutfitCategory;

public class Zombie extends Outfit {

    @Override
    public OutfitCategory outfitCategory() {
        return OutfitCategory.CLASSICS;
    }

    @Override
    public int coins() {
        return 500;
    }

    @Override
    public String[] description() {
        return new String[]{
                "Part of the official classic update.",
                "We all know this small green thing called \"Zombie\".",
                "Don't try to eat players while wearing this...."
        };
    }

    @Override
    public ItemStack head() {
        return new ItemBuilder(Material.ZOMBIE_HEAD).build();
    }

    @Override
    public ItemStack chestplate() {
        return new LeatherArmorBuilder(LeatherArmorBuilder.LeatherArmor.CHESTPLATE, Color.GREEN).build();
    }

    @Override
    public ItemStack leggs() {
        return new LeatherArmorBuilder(LeatherArmorBuilder.LeatherArmor.LEGGINGS, Color.GREEN).build();
    }

    @Override
    public ItemStack boots() {
        return new LeatherArmorBuilder(LeatherArmorBuilder.LeatherArmor.BOOTS, Color.GREEN).build();
    }
}
