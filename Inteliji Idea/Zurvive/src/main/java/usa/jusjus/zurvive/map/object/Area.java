package usa.jusjus.zurvive.map.object;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import usa.devrocoding.synergy.spigot.hologram.Hologram;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;

import java.util.ArrayList;
import java.util.List;

public class Area {

    @Getter @Setter
    private Location doorLocation1 = null, doorLocation2 = null;
    @Getter @Setter
    private List<Location> enemySpawns = new ArrayList<>();
    @Getter @Setter
    private Hologram hologram = null;
    @Getter
    private String id;

    public Area(String id){
        this.id = id;
    }

    public List<String> getHologramLines(){
        return new ArrayList<String>(){{
            add("§6§lUnlock area '§e"+id+"§6'");
            add("§e$500");
        }};
    }


//    public boolean isInside(final SynergyUser user) {
//        final ZoneVector curr = new ZoneVector(user.getPlayer().getLocation().getBlockX(), user.getPlayer().getLocation().getBlockY(), user.getPlayer().getLocation().getBlockZ());
//        final ZoneVector min = new ZoneVector(Math.min(this.location1.getBlockX(), this.location2.getBlockX()), Math.min(this.location1.getBlockY(), this.location2.getBlockY()), Math.min(this.location1.getBlockZ(), this.location2.getBlockZ()));
//        final ZoneVector max = new ZoneVector(Math.max(this.location1.getBlockX(), this.location2.getBlockX()), Math.max(this.location1.getBlockY(), this.location2.getBlockY()), Math.max(this.location1.getBlockZ(), this.location2.getBlockZ()));
//        return curr.isInAABB(min, max);
//    }

}
