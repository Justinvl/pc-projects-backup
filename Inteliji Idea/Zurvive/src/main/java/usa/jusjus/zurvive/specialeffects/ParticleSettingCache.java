package usa.jusjus.zurvive.specialeffects;

import org.bukkit.event.*;
import java.util.*;

public class ParticleSettingCache implements Listener
{
    private static Set<String> blacklist;
    
    @EventHandler
    public static boolean isBlacklisted(final String player) {
        return ParticleSettingCache.blacklist.contains(player);
    }
    
    static {
        ParticleSettingCache.blacklist = new HashSet<String>();
    }
}
