package usa.jusjus.zurvive.map.object;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import usa.jusjus.zurvive.Zurvive;

import java.util.ArrayList;
import java.util.List;

public class Map {

    @Getter
    private List<Area> areas = new ArrayList<>();
    @Getter @Setter
    private Location gameSpawn = null, lobbySpawn = null, showcaseLocation = null;

    public void saveToCache(){
        Zurvive.getInstance().getMapManager().saveMap(this);
    }

}
