package usa.jusjus.zurvive.wave;

import lombok.Getter;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.Module;
import usa.jusjus.zurvive.wave.object.Wave;

public class WaveManager extends Module {

    @Getter
    private Wave currentWave = null;

    public WaveManager(Core plugin){
        super(plugin, "Wave Manager", false);
    }

    @Override
    public void reload(String s) {

    }

    public void nextWave(){
        this.currentWave = new Wave();
    }
}
