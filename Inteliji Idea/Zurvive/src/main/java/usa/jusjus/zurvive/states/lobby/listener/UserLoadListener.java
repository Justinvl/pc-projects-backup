package usa.jusjus.zurvive.states.lobby.listener;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.user.event.UserLoadEvent;
import usa.devrocoding.synergy.spigot.user.object.MessageModification;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.states.lobby.gui.LobbyGUI;

public class UserLoadListener implements Listener {

    @EventHandler
    public void onUserLoadLobbyGUI(UserLoadEvent e){
        Synergy.debug("LOADED EVENT 1");
        e.getUser().getPlayer().getInventory().clear();
        e.getUser().getPlayer().setGameMode(GameMode.ADVENTURE);
        Synergy.debug("LOADED EVENT 2");
        new LobbyGUI(Core.getPlugin()).insert(e.getUser().getPlayer().getInventory(), e.getUser());

        Core.getPlugin().getUserManager().getOnlineUsers().iterator().forEachRemaining(user -> {
            user.sendModifactionMessage(
                    MessageModification.RAW,
                    "§e§l"+e.getUser().getName()+" §6has joined §7["+ Bukkit.getOnlinePlayers().size()+"/4]"
            );
        });
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e){
        e.getPlayer().teleport(Zurvive.getInstance().getMapManager().getMap().getLobbySpawn());
    }

}
