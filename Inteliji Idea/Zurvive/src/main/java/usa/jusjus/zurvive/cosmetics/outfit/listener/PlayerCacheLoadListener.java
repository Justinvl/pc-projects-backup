package usa.jusjus.zurvive.cosmetics.outfit.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.jusjus.zurvive.assets.playercache.event.PlayerCacheLoadEvent;
import usa.jusjus.zurvive.cosmetics.outfit.gui.VisualOutfitGUI;

public class PlayerCacheLoadListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onLoad(PlayerCacheLoadEvent e){
        Synergy.debug("CACHE LOADED EVENT 1");
        if (e.getPlayerCache().getSelectedOutfit() != null) {
            Synergy.debug("CACHE LOADED EVENT 2");
            new VisualOutfitGUI(Core.getPlugin(), e.getPlayerCache().getSelectedOutfit())
                    .insert(e.getSynergyUser().getPlayer().getInventory(), e.getSynergyUser());
        }
    }


    @EventHandler
    public void onClick(InventoryClickEvent e){
        Synergy.debug(e.getSlot()+"");
    }
}
