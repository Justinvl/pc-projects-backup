package usa.jusjus.zurvive.guns.object;

import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.utilities.ItemBuilder;
import usa.jusjus.zurvive.Zurvive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Gun {

    @Getter
    private Map<Player, List<Gun>> playerCooldown = new HashMap<>();
    @Getter
    private Map<Player, List<Gun>> reloading = new HashMap<>();

    public abstract String name();
    public abstract Material material();
    public abstract String[] description();

    public abstract Integer maxAmmo();
    public abstract Integer magazineAmmo();
    public abstract double cooldown();
    public abstract long reloadSpeed();
    public abstract double damage();
    public abstract double range();

    public abstract Particle particleBullet();
    public abstract Sound sound();

    public ItemStack getDynamicItem(){
        ItemStack item = new ItemBuilder(material())
                .setName("§6§l"+name())
                .setAmount(magazineAmmo())
                .addLore(description())
                .addLore(
                        " ",
                        " • Max Ammo: §e"+(maxAmmo()<0?"Unlimited":maxAmmo()),
                        " • Clip Ammo: §e"+magazineAmmo(),
                        " • Fire Rate: §e"+cooldown()+"s",
                        " • Reload Time: §e"+reloadSpeed()+"s",
                        " • Damage: §e"+damage(),
                        " • Range: §e"+range()+" blocks",
                        " ",
                        "§dLEFT CLICK §7to reload.",
                        "§dRIGHT CLICK §7to shoot."
                )
                .build();
        return item;
    }

    public ItemStack getItem(){
        ItemStack item = new ItemBuilder(material())
                .setName("§6§l"+name())
                .build();
        return item;
    }

    public void reloadGun(Player player, ItemStack item){
        final Gun gun = this;

        if (!isReloading(player)){
            List<Gun> guns = this.reloading.containsKey(player)
                    ? this.reloading.get(player)
                    : new ArrayList<>();
            guns.add(gun);

            item.setDurability((short)(item.getType().getMaxDurability()-1));
            this.reloading.put(player, guns);


            new BukkitRunnable(){
                @Override
                public void run() {
                    if (item.getDurability() <= 0){
                        // Put back the clip ammo
                        item.setAmount(magazineAmmo());

                        // Remove the reloading cooldown
                        reloading.get(player).remove(gun);

                        cancel();
                    }else{
                        item.setDurability((short)(item.getDurability()-5));
                    }
                }
            }.runTaskTimerAsynchronously(Core.getPlugin(), 2L, reloadSpeed());
        }
    }

    public boolean isReloading(Player player){
        List<Gun> guns = this.reloading.containsKey(player)
                ? this.reloading.get(player)
                : new ArrayList<>();
         return guns.contains(this);
    }
}
