package usa.jusjus.zurvive.map.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import usa.devrocoding.synergy.spigot.assets.wand.event.SelectorWandCompletionEvent;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.map.MapManager;
import usa.jusjus.zurvive.map.object.Area;

public class DoorSelectionListener implements Listener {

    @EventHandler
    public void onDoorSelectionCompletion(SelectorWandCompletionEvent e){
        MapManager mapManager = Zurvive.getInstance().getMapManager();
        Area area = mapManager.getAreaCreators().get(e.getSynergyUser());

        area.setDoorLocation1(e.getSelectorWand().getFirst().getLocation());
        area.setDoorLocation2(e.getSelectorWand().getSecond().getLocation());
        e.getSynergyUser().info("§eDoor has been selected and saved.");
    }

}
