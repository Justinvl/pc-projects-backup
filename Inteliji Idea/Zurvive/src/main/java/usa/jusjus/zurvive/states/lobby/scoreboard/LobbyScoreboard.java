package usa.jusjus.zurvive.states.lobby.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import usa.devrocoding.synergy.spigot.scoreboard.ScoreboardPolicy;
import usa.devrocoding.synergy.spigot.user.object.Rank;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.devrocoding.synergy.spigot.utilities.UtilString;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.states.lobby.LobbyState;

import java.util.ArrayList;
import java.util.List;

public class LobbyScoreboard implements ScoreboardPolicy {

    @Override
    public List<String> getSidebar(SynergyUser synergyUser) {
        LobbyState lobbyState = Zurvive.getInstance().getStatesManager().getLobbyState();
        return new ArrayList<String>(){{
            add("   §726/09/2019");
            add(" ");
            add("§e§lPlayers");
            add(Bukkit.getOnlinePlayers().size()+"/"+lobbyState.getMaxPlayers());
            add("  ");
            add(lobbyState.getLobbyTimer());
            add("   ");
            add("§c"+"play.zurvive.net");
        }};
    }

    @Override
    public String getPrefix(SynergyUser synergyUser, SynergyUser synergyUser1) {
        Rank rank = synergyUser1.getRank();
        if (rank != Rank.NONE) {
            return rank.getColor() + rank.getPrefix() + " " + rank.getTextColor();
        }
        return rank.getTextColor()+"";
    }

    @Override
    public String getSuffix(SynergyUser synergyUser, SynergyUser synergyUser1) {
        return null;
    }

    @Override
    public String getUndername(SynergyUser synergyUser) {
        return null;
    }

    @Override
    public int getUndernameScore(SynergyUser synergyUser, SynergyUser synergyUser1) {
        return 0;
    }

    @Override
    public String getTablist(SynergyUser synergyUser) {
        return null;
    }

    @Override
    public int getTablistScore(SynergyUser synergyUser, SynergyUser synergyUser1) {
        return 0;
    }
}
