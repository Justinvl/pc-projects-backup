package usa.jusjus.zurvive.states.game;

import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.scoreboard.ScoreboardPolicy;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.assets.playercache.object.PlayerCache;
import usa.jusjus.zurvive.cosmetics.outfit.gui.VisualOutfitGUI;
import usa.jusjus.zurvive.states.game.gui.GameGUI;
import usa.jusjus.zurvive.states.game.scoreboard.GameScoreboard;
import usa.jusjus.zurvive.wave.object.Wave;
import usa.synergy.minigame.assets.object.GameStates;

public class GameState extends usa.synergy.minigame.assets.object.GameState {

    @Override
    public usa.synergy.minigame.assets.object.GameState nextState() {
        return null;
    }

    @Override
    public boolean moveToNext() {
        return false;
    }

    @Override
    public ScoreboardPolicy scoreboard() {
        return new GameScoreboard();
    }

    @Override
    public void init() {
        Zurvive.getInstance().setGameState(GameStates.STARTED);
        Zurvive.getInstance().getWaveManager().nextWave();
        // Teleport the players to the map s
        for(SynergyUser user : Core.getPlugin().getUserManager().getOnlineUsers()){
            PlayerCache playerCache = Zurvive.getInstance().getCosmeticManager().getPlayerCacheManager().getPlayerCache(user.getUuid());

            user.getPlayer().getInventory().clear();
            user.getPlayer().teleport(Zurvive.getInstance().getMapManager().getMap().getGameSpawn());
            new GameGUI(Core.getPlugin()).insert(user.getPlayer().getInventory(), user);
            new VisualOutfitGUI(Core.getPlugin(), playerCache.getSelectedOutfit())
                    .insert(user.getPlayer().getInventory(), user);
        }
    }

    @Override
    public void tick(Core core) {
        core.getScoreboardManager().update();

        Wave wave = Zurvive.getInstance().getWaveManager().getCurrentWave();
        if (wave.getRemainingEnemies() <= 0){
            Zurvive.getInstance().getWaveManager().nextWave();
        }
    }

    @Override
    public void deInit() {

    }
}
