package usa.jusjus.zurvive.entities.enemies;

import net.minecraft.server.v1_14_R1.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_14_R1.util.UnsafeList;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.scheduler.BukkitRunnable;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.utilities.ItemBuilder;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.entities.object.ZurviveEnemy;

import java.lang.reflect.Field;

public class ZurviveZombie extends EntityZombie implements ZurviveEnemy {

    private float damage = 1.5f;

    @Override
    public void asdas() {

    }

    public ZurviveZombie(EntityTypes<? extends EntityMonster> entitytypes, World world) {
        super(EntityTypes.ZOMBIE, world);

        // Setting the options
        this.setBaby(false);
        this.setAirTicks(0);
        this.setCanPickupLoot(false);
        this.setJumping(true);
        this.setPersistent();
        this.setEquipment(EnumItemSlot.MAINHAND, null);

        new BukkitRunnable(){
            @Override
            public void run() {
                if (!ZurviveZombie.this.isAlive()){
                    cancel();
                    return;
                }
                LivingEntity closest = null;
                for(Entity entity : getBukkitEntity().getNearbyEntities(100, 60, 100)){
                    if (entity instanceof Player){
                        LivingEntity player = ((Player) entity);
                        closest = player;
                        break;
                    }
                }

                if (closest != null){
                    EntityLiving entityLiving = ((CraftPlayer) closest).getHandle();
                    Location loc = closest.getLocation();
                    Location enemyLoc = new Location(loc.getWorld(), ZurviveZombie.this.locX, ZurviveZombie.this.locY, ZurviveZombie.this.locZ);

                    // Let the entity walk to the nearest player
                    ZurviveZombie.this.getNavigation().a(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(), 1D);

                    // Check if entity is close to player, like hugging him
                    if (loc.distance(enemyLoc) < 1.8){
                        ZurviveZombie.this.setGoalTarget(entityLiving, EntityTargetEvent.TargetReason.CLOSEST_ENTITY, false);
                        entityLiving.damageEntity(DamageSource.mobAttack(ZurviveZombie.this), ZurviveZombie.this.damage);
                    }
                }
            }
        }.runTaskTimer(Zurvive.getInstance(), 20, 10L);
    }

    @Override
    protected void initAttributes() {
        super.initAttributes();

        this.getAttributeInstance(GenericAttributes.MAX_HEALTH).setValue(40D);
        this.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(0.296D);

        // Attack
        this.getAttributeInstance(GenericAttributes.ATTACK_KNOCKBACK).setValue(3);
        this.getAttributeInstance(GenericAttributes.ATTACK_DAMAGE).setValue(3);
    }

    @Override
    protected void initPathfinder() {
        // Disable the minecraft default pathfinding
//        super.initPathfinder();

        this.goalSelector.a(8, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 8.0F));
        this.goalSelector.a(5, new PathfinderGoalMoveTowardsRestriction(this, 400D));
        this.targetSelector.a(2, new PathfinderGoalNearestAttackableTarget(this, EntityHuman.class, true));
    }

    @Override
    protected void l() {
        this.goalSelector.a(2, new PathfinderGoalZombieAttack(this, 1.0D, false));
    }
}
