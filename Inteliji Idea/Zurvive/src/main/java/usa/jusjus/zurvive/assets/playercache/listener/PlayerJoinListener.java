package usa.jusjus.zurvive.assets.playercache.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.user.event.UserLoadEvent;
import usa.jusjus.zurvive.assets.playercache.PlayerCacheManager;
import usa.jusjus.zurvive.assets.playercache.event.PlayerCacheLoadEvent;
import usa.jusjus.zurvive.assets.playercache.object.PlayerCache;
import usa.jusjus.zurvive.cosmetics.CosmeticManager;
import usa.jusjus.zurvive.cosmetics.outfit.OutfitManager;
import usa.jusjus.zurvive.cosmetics.outfit.gui.VisualOutfitGUI;
import usa.jusjus.zurvive.cosmetics.outfit.object.Outfit;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PlayerJoinListener implements Listener {

    private CosmeticManager cosmeticManager;

    public PlayerJoinListener(CosmeticManager cosmeticManager){
        this.cosmeticManager = cosmeticManager;
    }

    @EventHandler
    public void onUserJoin(UserLoadEvent e){
        Core.getPlugin().getRunnableManager().runTaskAsynchronously(
            "Load Zurvive Player Cache",
            core -> {
                try{
                    Synergy.debug("1");
                    ResultSet resultSet = Core.getPlugin().getDatabaseManager().getResults(
                            "outfits ", "uuid=?", new HashMap<Integer, Object>(){{
                                put(1, e.getUser().getUuid().toString());
                            }}
                    );
                    Synergy.debug("2");
                    List<Outfit> outfits = new ArrayList<>();
                    Outfit selectedOutfit = null;
                    while (resultSet.next()){
                        Synergy.debug("3");
                        String outfitName = resultSet.getString("outfit");
                        try{
                            Outfit outfit = this.cosmeticManager.getOutfitManager().getOutfitByName(outfitName);
                            if (resultSet.getBoolean("selected")){
                                Synergy.debug("3 - 1");
                                selectedOutfit = outfit;
                            }
                            outfits.add(outfit);
                        }catch (Exception ex){
                            Synergy.error("Trying to add an outfit which doesn't exists! ["+outfitName+"]");
                        }
                    }
                    Synergy.debug("4");
                    PlayerCache playerCache = new PlayerCache(
                            e.getUser().getUuid(),
                            0,
                            outfits
                    );
                    resultSet.close();
                    Synergy.debug("5");
                    playerCache.setSelectedOutfit(selectedOutfit);
                    Synergy.debug("6 = "+e.getUser().getUuid().toString());
                    this.cosmeticManager.getPlayerCacheManager().addCache(e.getUser().getUuid(), playerCache);
                    Core.getPlugin().getRunnableManager().runTask("init playerCAche event", core1 -> {
                        core1.getServer().getPluginManager().callEvent(new PlayerCacheLoadEvent(playerCache, e.getUser()));
                    });
                }catch (SQLException ex){
                    ex.printStackTrace();
                }
            }
        );
    }

}
