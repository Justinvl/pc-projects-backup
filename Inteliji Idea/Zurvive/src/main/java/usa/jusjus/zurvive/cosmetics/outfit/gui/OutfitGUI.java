package usa.jusjus.zurvive.cosmetics.outfit.gui;

import lombok.Getter;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.gui.Gui;
import usa.devrocoding.synergy.spigot.gui.GuiElement;
import usa.devrocoding.synergy.spigot.gui.object.GuiSize;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.devrocoding.synergy.spigot.utilities.ItemBuilder;
import usa.jusjus.zurvive.cosmetics.CosmeticManager;
import usa.jusjus.zurvive.cosmetics.outfit.object.OutfitCategory;

public class OutfitGUI extends Gui {

    @Getter
    private CosmeticManager cosmeticManager;

    public OutfitGUI(Core plugin, CosmeticManager cosmeticManager){
        super(plugin, "Zurvive Outfits", GuiSize.FIVE_ROWS, false);

        this.cosmeticManager = cosmeticManager;

        setup();
    }

    @Override
    public void setup() {
        for(OutfitCategory outfitCategory : OutfitCategory.values()) {
            addElement(outfitCategory.getInventoryPlace(), new GuiElement() {
                @Override
                public ItemStack getIcon(SynergyUser synergyUser) {
                    return outfitCategory.getItem();
                }

                @Override
                public void click(SynergyUser synergyUser, ClickType clickType) {
                    new OutfitObjectGUI(getPlugin(), outfitCategory, synergyUser, getCosmeticManager())
                            .setBackGui(getOuterClazz())
                            .open(synergyUser.getPlayer());
                }
            });
        }
    }
}
