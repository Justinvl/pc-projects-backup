package usa.jusjus.zurvive.states.lobby.gui;

import org.bukkit.Material;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.gui.object.GuiInteract;
import usa.devrocoding.synergy.spigot.gui.object.GuiInteractElement;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.devrocoding.synergy.spigot.utilities.ItemBuilder;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.cosmetics.outfit.gui.OutfitGUI;
import usa.jusjus.zurvive.guns.gun.AK47;
import usa.jusjus.zurvive.guns.gun.Pistol;

public class LobbyGUI extends GuiInteract {

    public LobbyGUI(Core plugin){
        super(plugin);
    }

    @Override
    public void setup() {
        addElement(new GuiInteractElement() {
            @Override
            public ItemStack getIcon(SynergyUser synergyUser) {
                return Zurvive.getInstance().getGunManager().getGunByClass(Pistol.class).getDynamicItem();
            }

            @Override
            public void click(SynergyUser synergyUser, Action action) {

            }
        }, 0);
        addElement(new GuiInteractElement() {
            @Override
            public ItemStack getIcon(SynergyUser synergyUser) {
                return new ItemBuilder(Material.BARRIER)
                        .setName("§c§lLeave Game")
                        .addLore("Right click to leave this game")
                        .build();
            }

            @Override
            public void click(SynergyUser synergyUser, Action action) {
                new OutfitGUI(getPlugin(), Zurvive.getInstance().getCosmeticManager()).open(synergyUser.getPlayer());
            }
        }, 8);
    }
}
