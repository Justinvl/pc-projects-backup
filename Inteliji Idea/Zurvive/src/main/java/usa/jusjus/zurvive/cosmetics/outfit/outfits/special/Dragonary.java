package usa.jusjus.zurvive.cosmetics.outfit.outfits.special;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import usa.devrocoding.synergy.spigot.utilities.ItemBuilder;
import usa.devrocoding.synergy.spigot.utilities.LeatherArmorBuilder;
import usa.jusjus.zurvive.cosmetics.outfit.object.Outfit;
import usa.jusjus.zurvive.cosmetics.outfit.object.OutfitCategory;

public class Dragonary extends Outfit {

    @Override
    public OutfitCategory outfitCategory() {
        return OutfitCategory.SPECIAL;
    }

    @Override
    public int coins() {
        return 1500;
    }

    @Override
    public String[] description() {
        return new String[]{
                ""
        };
    }

    @Override
    public ItemStack head() {
        return new ItemBuilder(Material.DRAGON_HEAD).build();
    }

    @Override
    public ItemStack chestplate() {
        return new LeatherArmorBuilder(LeatherArmorBuilder.LeatherArmor.CHESTPLATE, Color.BLACK).build();
    }

    @Override
    public ItemStack leggs() {
        return new LeatherArmorBuilder(LeatherArmorBuilder.LeatherArmor.LEGGINGS, Color.BLACK).build();
    }

    @Override
    public ItemStack boots() {
        return new ItemBuilder(Material.GOLDEN_BOOTS).build();
    }
}
