package usa.jusjus.zurvive.map;

import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.Module;
import usa.devrocoding.synergy.spigot.assets.wand.object.SelectorWand;
import usa.devrocoding.synergy.spigot.files.yml.YMLFile;
import usa.devrocoding.synergy.spigot.hologram.Hologram;
import usa.devrocoding.synergy.spigot.hologram.object.HologramLine;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.devrocoding.synergy.spigot.utilities.UtilConfiguration;
import usa.devrocoding.synergy.spigot.utilities.UtilLoc;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.map.commands.CommandArea;
import usa.jusjus.zurvive.map.commands.CommandMap;
import usa.jusjus.zurvive.map.exception.MapNotConfiguredException;
import usa.jusjus.zurvive.map.listener.DoorSelectionListener;
import usa.jusjus.zurvive.map.listener.UserJoinMapListener;
import usa.jusjus.zurvive.map.object.Area;
import usa.jusjus.zurvive.map.object.Map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapManager extends Module {

    @Getter
    private Map map;
    @Getter
    private YMLFile cache;
    @Getter
    private java.util.Map<SynergyUser, Area> areaCreators = new HashMap<>();

    public MapManager(Core plugin){
        super(plugin, "Map Manager", false);

        this.cache = new YMLFile(Zurvive.getInstance().getDataFolder().toString(), "map");
        this.cache.save();

        this.map = new Map();
        loadData();

        registerCommand(
                new CommandMap(plugin),
                new CommandArea(plugin)
        );

        registerListener(
                new UserJoinMapListener(),
                new DoorSelectionListener()
        );
    }

    @Override
    public void reload(String s) {

    }

    /*
        @Returns: NULL if map is configured or STRING what is missing.
     */
    public boolean isMapConfigured() throws MapNotConfiguredException {
        if (this.cache.exists() && this.cache.get().contains("map")){
            if (!this.cache.get().contains("map.area")){
                throw new MapNotConfiguredException("No Area exists!");
            }
            if (!this.cache.get().contains("map.lobbyspawn")){
                throw new MapNotConfiguredException("No Wait Lobby Spawn exists!");
            }
            if (!this.cache.get().contains("map.gamespawn")){
                throw new MapNotConfiguredException("No Game Spawn exists!");
            }
            if (!this.cache.get().contains("map.showcasespawn")){
                throw new MapNotConfiguredException("No Game Spawn exists!");
            }
            return true;
        }
        throw new MapNotConfiguredException("Everything needs to be configured!");
    }

    private void loadData(){
        if (this.cache.exists() && this.cache.get().contains("map")){
            List<Area> areas;

            if (this.cache.get().contains("map.gamespawn")){
                getMap().setGameSpawn(UtilConfiguration.getFromFile("map.gamespawn", this.cache.get()));
            }
            if (this.cache.get().contains("map.lobbyspawn")){
                getMap().setLobbySpawn(UtilConfiguration.getFromFile("map.lobbyspawn", this.cache.get()));
            }
            if (this.cache.get().contains("map.showcasespawn")){
                getMap().setShowcaseLocation(UtilConfiguration.getFromFile("map.showcasespawn", this.cache.get()));
            }

            if (this.cache.get().contains("map.area")){
                for(String id : this.cache.get().getConfigurationSection("map.area").getKeys(false)){
                    Area area = new Area(id);
                    String key = "map.area."+id;

                    if (this.cache.get().contains(key+".hologram")){
                        getPlugin().getHologramManager().createHologram(
                                UtilConfiguration.getFromFile(key + ".hologram", this.cache.get()),
                                new HologramLine() {
                                    @Override
                                    public String getMessage(SynergyUser synergyUser) {
                                        return "Click to open";
                                    }
                                },
                                player -> player.getInventory().contains(Material.BOOKSHELF)
                        );
                    }
                    if (this.cache.get().contains(key+".door.1")&&this.cache.get().contains(key+".door.2")){
                        area.setDoorLocation1(UtilConfiguration.getFromFile(key+".door.1", this.cache.get()));
                        area.setDoorLocation2(UtilConfiguration.getFromFile(key+".door.2", this.cache.get()));
                    }
                    if (this.cache.get().contains(key+".enemyspawns")){
                        List<Location> locations = new ArrayList<>();

                        for(String sloc : this.cache.get().getStringList(key+".enemyspawns")){
                            Synergy.debug(sloc);

                            locations.add(UtilLoc.deserialize(sloc));
                        }
                        area.setEnemySpawns(locations);
                        System.out.println(area.getEnemySpawns());
                    }
                    getMap().getAreas().add(area);
                }
            }
        }
    }

    public void saveMap(Map map){
        if (map.getLobbySpawn() != null) {
            UtilConfiguration.locationToCleanStructure("map.lobbyspawn", getCache().get(), map.getLobbySpawn());
        }
        if (map.getGameSpawn() != null) {
            UtilConfiguration.locationToCleanStructure("map.gamespawn", getCache().get(), map.getGameSpawn());
        }

        if (map.getShowcaseLocation() != null) {
            UtilConfiguration.locationToCleanStructure("map.showcasespawn", getCache().get(), map.getShowcaseLocation());
        }

        for(Area area : map.getAreas()){
            String key = "map.area."+area.getId();

            if (area.getHologram() != null) {
                UtilConfiguration.locationToCleanStructure(key + ".hologram", getCache().get(), area.getHologram().getLocation());
            }

            if (area.getDoorLocation1() != null) {
                UtilConfiguration.locationToCleanStructure(key + ".door.1", getCache().get(), area.getDoorLocation1());
                UtilConfiguration.locationToCleanStructure(key + ".door.2", getCache().get(), area.getDoorLocation2());
            }

            List<String> locations = new ArrayList<>();

            for(Location loc : area.getEnemySpawns()){
                locations.add(UtilLoc.serialize(loc));
            }
            if (locations.size() > 0)
                getCache().get().set(key+".enemyspawns", locations);
        }

        getCache().save();
    }

    public boolean saveArea(Area area){
        if ((area.getId().equalsIgnoreCase("start")&& area.getEnemySpawns().size() > 0)
                || (area.getDoorLocation1() != null && area.getDoorLocation2() != null)
                && area.getHologram() != null && area.getEnemySpawns().size() > 0
        ){
            getMap().getAreas().add(area);
            saveMap(getMap());
            return true;
        }
        return false;
    }

    public Area getAreaByID(String id){
        for(Area area : getMap().getAreas()){
            if (area.getId().equalsIgnoreCase(id)){
                return area;
            }
        }
        return null;
    }


}
