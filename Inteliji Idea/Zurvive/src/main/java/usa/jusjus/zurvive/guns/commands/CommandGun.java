package usa.jusjus.zurvive.guns.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftCreature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.command.SynergyCommand;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.entities.EntitiesManager;
import usa.jusjus.zurvive.guns.object.Gun;

public class CommandGun extends SynergyCommand {

    public CommandGun(Core plugin){
        super(plugin, "", false, "guns");
    }

    @Override
    public void execute(SynergyUser synergyUser, Player player, String s, String[] strings) {
        for(Gun gun : Zurvive.getInstance().getGunManager().getGuns().values()){
            player.getInventory().addItem(gun.getDynamicItem());
        }
    }

    @Override
    public void execute(ConsoleCommandSender sender, String command, String[] args) {

    }
}
