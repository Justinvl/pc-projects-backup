package usa.jusjus.zurvive;

import lombok.Getter;
import lombok.Setter;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.api.SynergyPlugin;
import usa.jusjus.zurvive.assets.CooldownManager;
import usa.jusjus.zurvive.character.CharacterManager;
import usa.jusjus.zurvive.cosmetics.CosmeticManager;
import usa.jusjus.zurvive.entities.EntitiesManager;
import usa.jusjus.zurvive.guns.GunManager;
import usa.jusjus.zurvive.map.MapManager;
import usa.jusjus.zurvive.specialeffects.ParticleEffectActivator;
import usa.jusjus.zurvive.states.StatesManager;
import usa.jusjus.zurvive.wave.WaveManager;
import usa.synergy.minigame.assets.object.GameStates;

public class Zurvive extends SynergyPlugin {

    @Getter
    public static Zurvive instance;
    @Getter @Setter
    private GameStates gameState = GameStates.LOBBY;

    @Getter
    private GunManager gunManager;
    @Getter
    private CooldownManager cooldownManager;
    @Getter
    private MapManager mapManager;
    @Getter
    private StatesManager statesManager;
    @Getter
    private WaveManager waveManager;
    @Getter
    private EntitiesManager entitiesManager;
    @Getter
    private CosmeticManager cosmeticManager;
    @Getter
    private CharacterManager characterManager;

    @Override
    public String name() {
        return "Zurvive Game";
    }

    @Override
    public void init() {
        instance = this;
        Core synergy = Core.getPlugin();

        this.cooldownManager = new CooldownManager(synergy);
        this.gunManager = new GunManager(synergy);
        this.mapManager = new MapManager(synergy);
        this.statesManager = new StatesManager(synergy);
        this.waveManager = new WaveManager(synergy);
        this.entitiesManager = new EntitiesManager(synergy);
        this.characterManager = new CharacterManager(synergy);

        new ParticleEffectActivator().activateThread();

        // IMPORTANT Cosmetic Manager will load all of the cosmetic managers!
        this.cosmeticManager = new CosmeticManager(synergy);
    }

    @Override
    public void deinit() {

    }

}
