package usa.jusjus.zurvive.cosmetics.outfit.outfits.monsters;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import usa.devrocoding.synergy.spigot.utilities.ItemBuilder;
import usa.devrocoding.synergy.spigot.utilities.LeatherArmorBuilder;
import usa.jusjus.zurvive.cosmetics.outfit.object.Outfit;
import usa.jusjus.zurvive.cosmetics.outfit.object.OutfitCategory;

public class Skeleton extends Outfit {

    @Override
    public OutfitCategory outfitCategory() {
        return OutfitCategory.CLASSICS;
    }

    @Override
    public int coins() {
        return 500;
    }

    @Override
    public String[] description() {
        return new String[]{
                ""
        };
    }

    @Override
    public ItemStack head() {
        return new ItemBuilder(Material.SKELETON_SKULL).build();
    }

    @Override
    public ItemStack chestplate() {
        return new LeatherArmorBuilder(LeatherArmorBuilder.LeatherArmor.CHESTPLATE, Color.WHITE).build();
    }

    @Override
    public ItemStack leggs() {
        return new LeatherArmorBuilder(LeatherArmorBuilder.LeatherArmor.LEGGINGS, Color.WHITE).build();
    }

    @Override
    public ItemStack boots() {
        return new LeatherArmorBuilder(LeatherArmorBuilder.LeatherArmor.BOOTS, Color.WHITE).build();
    }
}
