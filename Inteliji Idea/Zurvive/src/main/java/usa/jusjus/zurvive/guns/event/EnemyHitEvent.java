package usa.jusjus.zurvive.guns.event;

import lombok.Getter;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import usa.devrocoding.synergy.spigot.listeners.SynergyEvent;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.jusjus.zurvive.guns.object.Gun;

public class EnemyHitEvent extends SynergyEvent {

    @Getter
    private LivingEntity enemy;
    @Getter
    private Player player;
    @Getter
    private Gun gun;
    @Getter
    private SynergyUser synergyUser;

    public EnemyHitEvent(LivingEntity hit, Player player, Gun gun, SynergyUser synergyUser){
        this.enemy = hit;
        this.player = player;
        this.gun = gun;
        this.synergyUser = synergyUser;
    }

}
