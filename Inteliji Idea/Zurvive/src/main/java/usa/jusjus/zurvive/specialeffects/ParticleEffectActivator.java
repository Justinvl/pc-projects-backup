package usa.jusjus.zurvive.specialeffects;

import org.bukkit.*;
import org.bukkit.plugin.*;
import org.bukkit.scheduler.*;
import org.bukkit.util.Vector;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.utilities.UtilLoc;

public class ParticleEffectActivator
{
    public void activateThread() {
        this.activateRainCloud();
        this.activateEnchanted();
        this.activateFlameRings();
        this.activateVortex();
        this.activateEmeralds();
        this.activateHearts();
    }
    
    private void activateFlameRings() {
        final ParticleType type = ParticleType.FlameRings;
        Bukkit.getScheduler().scheduleSyncRepeatingTask((Plugin) Core.getPlugin(), (Runnable)new Runnable() {
            double angle = 0.0;
            
            @Override
            public void run() {
                if (this.angle >= 6.283185307179586) {
                    this.angle = 0.0;
                }
                this.angle += 0.25132741228718347;
                final double x = 0.8 * Math.cos(this.angle);
                final double y = 0.8 * Math.sin(this.angle);
                final double z = 0.8 * Math.sin(this.angle);
                final double x2 = 0.8 * Math.cos(6.283185307179586 - this.angle);
                final double y2 = 0.8 * Math.sin(6.283185307179586 - this.angle);
                final double z2 = 0.8 * Math.sin(6.283185307179586 - this.angle);
                for (final Location loc : type.getLocations()) {
                    final Location la = new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ()).add(0.0, 0.0, 0.0);
                    final Location l = la.add(0, 1, 0);
                    final Location a = UtilLoc.add(l, x, y, z);
                    final Location b = UtilLoc.add(l, x2, -y2, z2);
                    a.getWorld().spawnParticle(Particle.FLAME, a, 0);
                    b.getWorld().spawnParticle(Particle.FLAME, b, 0);
                }
            }
        }, 0L, 2L);
    }
    
    private void activateVortex() {
        final ParticleType type = ParticleType.Vortex;
        new BukkitRunnable() {
            float step = 0.0f;
            
            public void run() {
                final Vector vector = new Vector();
                for (int i = 0; i < 10; ++i) {
                    ++this.step;
                    final float t = 0.020943953f * this.step;
                    final float r = (float)(Math.sin(t) * 1.0);
                    final float s = 6.283186f * t;
                    vector.setX(1.0f * r * Math.cos(s) + 0.0);
                    vector.setZ(1.0f * r * Math.sin(s) + 0.0);
                    vector.setY(2.0 * Math.cos(t) + 0.0);
                    for (final Location loc : type.getLocations()) {
                        final Location l = new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ()).add(0.0, 2.0, 0.0);
                        l.getWorld().spawnParticle(Particle.SPELL_WITCH, l.add(vector), 0);
                        l.subtract(vector);
                    }
                }
            }
        }.runTaskTimer((Plugin)Core.getPlugin(), 0L, 2L);
    }
    
    private void activateHearts() {
        final ParticleType type = ParticleType.Hearts;
        Bukkit.getScheduler().scheduleSyncRepeatingTask((Plugin)Core.getPlugin(), (Runnable)new Runnable() {
            double angle = 0.0;
            final double radius = 0.6;
            final int HEART_AMOUNT = 1;
            final double angleInc = 0.6283185307179586;
            
            @Override
            public void run() {
                final double[] x = { 0.0 };
                final double[] y = { 0.0 };
                final double[] z = { 0.0 };
                this.angle += 0.6283185307179586;
                if (this.angle > 6.283185307179586) {
                    this.angle -= 6.283185307179586;
                }
                for (int i = 0; i < 1; ++i) {
                    x[i] = 0.6 * Math.cos(this.angle);
                    y[i] = 0.0;
                    z[i] = 0.6 * Math.sin(this.angle);
                }
                for (final Location loc : type.getLocations()) {
                    for (int j = 0; j < 1; ++j) {
                        final Location l = UtilLoc.add(loc, x[j], y[j] + 2.0, z[j]);
                        l.getWorld().spawnParticle(Particle.HEART, l, 0);
                    }
                }
            }
        }, 0L, 5L);
    }
    
    private void activateEmeralds() {
        final ParticleType type = ParticleType.Sparklez;
        Bukkit.getScheduler().scheduleSyncRepeatingTask((Plugin)Core.getPlugin(), (Runnable)new Runnable() {
            double angle = 0.0;
            final double radius = 0.7;
            final double angleInc = 0.7853981633974483;
            final double maxY = 2.0;
            double y = 0.0;
            final double yInc = 0.30000001192092896;
            
            @Override
            public void run() {
                final double x = 0.7 * Math.cos(this.angle);
                final double z = 0.7 * Math.sin(this.angle);
                this.angle += 0.7853981633974483;
                if (this.angle > 6.283185307179586) {
                    this.angle -= 6.283185307179586;
                    this.y += 0.30000001192092896;
                    if (this.y >= 2.0) {
                        this.y = 0.0;
                    }
                }
                for (final Location loc : type.getLocations()) {
                    final Location l = new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ()).add(x, this.y, z);
                    l.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, l, 0);
                }
            }
        }, 0L, 2L);
    }
    
    private void activateEnchanted() {
        final ParticleType type = ParticleType.Enchanted;
        Bukkit.getScheduler().runTaskTimer((Plugin)Core.getPlugin(), (Runnable)new Runnable() {
            float step = 0.0f;
            float step2 = 0.0f;
            final double radialsPerStep = 0.19634954084936207;
            
            @Override
            public void run() {
                for (final Location locatin : type.getLocations()) {
                    final Location finalloc = new Location(locatin.getWorld(), locatin.getX(), locatin.getY(), locatin.getZ());
                    if (finalloc.getY() > 68.0) {
                        finalloc.setY(64.0);
                    }
                    final Location loc = finalloc.add(0.0, 1.0, 0.0);
                    final Location loc2 = finalloc.add(0.0, 0.5, 0.0);
                    loc.add(Math.sin(0.19634954084936207 * this.step), 0.0, Math.cos(0.19634954084936207 * this.step));
                    loc2.add(Math.sin(0.19634954084936207 * this.step), 0.0, Math.cos(0.19634954084936207 * this.step2));

                    loc.getWorld().spawnParticle(Particle.ENCHANTMENT_TABLE, loc, 0);
                    loc.getWorld().spawnParticle(Particle.ENCHANTMENT_TABLE, loc2, 0);
                    loc.getWorld().spawnParticle(Particle.PORTAL, loc, 0);
                    loc.getWorld().spawnParticle(Particle.PORTAL, loc2, 0);

                    this.step += 0.4;
                    this.step2 -= 0.4;
                }
            }
        }, 0L, 2L).getTaskId();
    }
    
    private void activateRainCloud() {
        final ParticleType type = ParticleType.RainCloud;
        Bukkit.getScheduler().runTaskTimer((Plugin)Core.getPlugin(), (Runnable)new Runnable() {
            @Override
            public void run() {
                for (final Location locatin : type.getLocations()) {
                    final Location loc = new Location(locatin.getWorld(), locatin.getX(), locatin.getY(), locatin.getZ());
                    loc.getWorld().spawnParticle(Particle.DRIP_WATER, loc.add(0.0, 4.0, 0.0), 0);
                    loc.getWorld().spawnParticle(Particle.CLOUD, loc.add(0.0, 4.0, 0.0), 0);
                }
            }
        }, 0L, 2L);
    }
}