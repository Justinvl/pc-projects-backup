package usa.jusjus.zurvive.cosmetics.outfit.object;

import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import usa.devrocoding.synergy.spigot.utilities.ItemBuilder;

import java.util.ArrayList;
import java.util.List;

public enum OutfitCategory {

    CLASSICS(
            "§cClassic Outfits",
            Material.ZOMBIE_HEAD,
            11,
            "Checkout all the classic outfits"
    ),
    SPECIAL(
            "§6§lSpecial Outfits",
            Material.DRAGON_HEAD,
            15,
            "Checkout all the special outfits"
    ),
    EVENT(
            "§b§lEvent",
            Material.BLAZE_POWDER,
            31,
            "Checkout all the event outfits"
    );

    @Getter
    private String name;
    private Material item;
    @Getter
    private int inventoryPlace;
    @Getter
    private String[] description;
    @Getter
    public List<Outfit> outfits = new ArrayList<>();

    OutfitCategory(String name, Material item, int inventoryPlace, String... description){
        this.name = name;
        this.item = item;
        this.inventoryPlace = inventoryPlace;
        this.description = description;
    }

    public ItemStack getItem(){
        return new ItemBuilder(this.item)
                .setName(this.name)
                .addLore(this.description)
                .build();
    }

}
