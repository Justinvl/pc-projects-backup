package usa.jusjus.zurvive.guns;

import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.Module;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.guns.commands.CommandGun;
import usa.jusjus.zurvive.guns.commands.CommandZurvive;
import usa.jusjus.zurvive.guns.gun.AK47;
import usa.jusjus.zurvive.guns.gun.Pistol;
import usa.jusjus.zurvive.guns.listener.EnemyDeadListener;
import usa.jusjus.zurvive.guns.listener.EnemyHitListener;
import usa.jusjus.zurvive.guns.listener.GunShootListener;
import usa.jusjus.zurvive.guns.object.Gun;

import java.util.*;

public class GunManager extends Module {

    @Getter
    public Map<String, Gun> guns = new HashMap<>();

    public GunManager(Core plugin){
        super(plugin, "Gun Manager", true);

        registerListener(
                new GunShootListener(),
                new EnemyHitListener(),
                new EnemyDeadListener()
        );

        registerCommand(
                new CommandGun(plugin),
                new CommandZurvive(plugin)
        );

        registerGuns(
                new Pistol(),
                new AK47()
        );
    }

    @Override
    public void reload(String s) {

    }

    public void registerGuns(Gun... guns){
        Arrays.asList(guns).forEach(gun -> getGuns().put(gun.getItem().getItemMeta().getDisplayName(), gun));
    }

    public Gun getGunByClass(Class<? extends Gun> gunClass){
        try{
            return (Gun) gunClass.newInstance();
        }catch (Exception e){
            Synergy.error(e.getMessage());
            e.printStackTrace();
        }
        Synergy.error("Critical ERROR! Trying to give a gun but doesn't exists! Debug: '"+gunClass.getSimpleName()+"'");
        //TODO: Fix this later to null, because otherwise we get a nullpointer
        return this.guns.values().iterator().next();
    }

    public Gun getGunByName(String name){
        for(Gun gun : this.guns.values()){
            if (gun.getClass().getSimpleName().equalsIgnoreCase(name)){
                return gun;
            }
        }
        Synergy.error("Critical ERROR! Trying to give a gun but doesn't exists! Debug: '"+name+"'");
        //TODO: Fix this later to null, because otherwise we get a nullpointer
        return this.guns.values().iterator().next();
    }

    public LivingEntity getTarget(Player source, double range) {
                List<Block> blocksInSight = source.getLineOfSight(null, (int)range);
        List<Entity> nearEntities = source.getNearbyEntities(range, range, range);
        if (blocksInSight != null && nearEntities != null) {
            for (Block block : blocksInSight) {
                int xBlock = block.getX();
                int yBlock = block.getY();
                int zBlock = block.getZ();

                for (Entity entity : nearEntities) {
                    if (entity instanceof LivingEntity) {
                        Location entityLocation = entity.getLocation();
                        int xEntity = entityLocation.getBlockX();
                        int yEntity = entityLocation.getBlockY();
                        int zEntity = entityLocation.getBlockZ();
                        if (xEntity == xBlock && (yEntity <= yBlock && yEntity >= yBlock - 1) && zEntity == zBlock) {
                            return (LivingEntity) entity;
                        }
                    }
                }
            }
        }
        return null;
    }

}
