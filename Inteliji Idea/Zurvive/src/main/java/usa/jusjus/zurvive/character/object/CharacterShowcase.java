package usa.jusjus.zurvive.character.object;

import lombok.Getter;
import net.minecraft.server.v1_14_R1.*;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_14_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.hologram.object.HologramLine;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.devrocoding.synergy.spigot.utilities.UtilPlayer;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.cosmetics.outfit.object.Outfit;
import usa.jusjus.zurvive.guns.gun.Pistol;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

public class CharacterShowcase {

    @Getter
    private Location location;
    private EntityArmorStand entityArmorStand;
    @Getter
    private Predicate<Player> shouldShow;
    private HologramLine hologramLine;
    private Outfit outfitCache;

    public CharacterShowcase(Location location, HologramLine hologramLine, Predicate<Player> shouldShow) {
        this.location = location;
        this.hologramLine = hologramLine;
        this.shouldShow = shouldShow;
    }

    public boolean shouldShow(Player player) {
        return this.shouldShow != null ? this.shouldShow.test(player) : true;
    }

    public void send(SynergyUser synergyUser, Outfit outfit){
        if (shouldShow(synergyUser.getPlayer())) {
            if (!isValid()) {
                create(synergyUser, outfit);
            } else {
                if (needsUpdate(outfit)){
                    this.outfitCache = outfit;
                    send(entityArmorStand, synergyUser.getPlayer(), outfit);
                }
            }
        }else{
            remove(synergyUser.getPlayer());
        }
    }


    private void create(SynergyUser synergyUser, Outfit outfit){
        String line = hologramLine.getMessage(synergyUser);
        EntityArmorStand entityArmorStand = new EntityArmorStand(EntityTypes.ARMOR_STAND, ((CraftWorld) this.location.getWorld()).getHandle());
        entityArmorStand.setLocation(this.location.getX(), this.location.getY(), this.location.getZ(), this.location.getYaw(), this.location.getPitch());
        entityArmorStand.setCustomNameVisible(true);
        entityArmorStand.setCustomName(new ChatMessage(line));
        entityArmorStand.setNoGravity(true);
        entityArmorStand.setSilent(true);

        Field disabledSlots;
        try {
            disabledSlots = entityArmorStand.getClass().getDeclaredField("bE");
            disabledSlots.setAccessible(true);
            disabledSlots.set(entityArmorStand, 2039583);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
        this.outfitCache = outfit;
        this.entityArmorStand = entityArmorStand;
        send(entityArmorStand, synergyUser.getPlayer(), outfit);
    }

    private boolean needsUpdate(Outfit outfit){
        return outfit != null && outfitCache == null || (outfitCache != null &&  outfitCache != outfit);
    }

    private boolean isValid(){
        return entityArmorStand != null;
    }

    private void send(EntityArmorStand armorStand, Player player, Outfit outfit){
        PacketPlayOutSpawnEntityLiving packetPlayOutSpawnEntity = new PacketPlayOutSpawnEntityLiving(armorStand);
        UtilPlayer.sendPacket(player, packetPlayOutSpawnEntity);

        Map<EnumItemSlot, ItemStack> map = new HashMap<EnumItemSlot, ItemStack>(){{
            put(EnumItemSlot.MAINHAND, Zurvive.getInstance().getGunManager().getGunByClass(Pistol.class).getDynamicItem());
            put(EnumItemSlot.HEAD, outfit == null ? new ItemStack(org.bukkit.Material.AIR) : outfit.head());
            put(EnumItemSlot.CHEST, outfit == null ? new ItemStack(org.bukkit.Material.AIR) : outfit.chestplate());
            put(EnumItemSlot.LEGS, outfit == null ? new ItemStack(org.bukkit.Material.AIR) : outfit.leggs());
            put(EnumItemSlot.FEET, outfit == null ? new ItemStack(Material.AIR) : outfit.boots());
        }};

        for(EnumItemSlot slot : map.keySet()){
            PacketPlayOutEntityEquipment packet = new PacketPlayOutEntityEquipment(
                    armorStand.getId(),
                    slot,
                    CraftItemStack.asNMSCopy(map.get(slot))
            );
            UtilPlayer.sendPacket(player, packet);
        }

//        PacketPlayOutEntityMetadata packetPlayOutEntityMetadata = new PacketPlayOutEntityMetadata(armorStand.getId(), armorStand.getDataWatcher(), false);
//        UtilPlayer.sendPacket(player, packetPlayOutEntityMetadata);
    }

    public void remove(Player player) {
        if (isValid()){
            UtilPlayer.sendPacket(player, new PacketPlayOutEntityDestroy(this.entityArmorStand.getId()));
            this.entityArmorStand = null;
        }
    }

}
