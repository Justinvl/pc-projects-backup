package usa.jusjus.zurvive.map.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.assets.C;
import usa.devrocoding.synergy.spigot.user.event.UserLoadEvent;
import usa.devrocoding.synergy.spigot.user.object.MessageModification;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.map.MapManager;

public class UserJoinMapListener implements Listener {

    @EventHandler
    public void onPlayerJoin(UserLoadEvent e){
        MapManager mapManager = Zurvive.getInstance().getMapManager();

        try{
            if (mapManager.isMapConfigured()){

            }
        }catch (Exception ex){
            //TODO: Put in maintenance mode
            e.getUser().sendModifactionMessage(
                    MessageModification.CENTERED,
                    C.getLine(),
                    "§9Tis map is not configured properly!",
                    "§9Configure this map by typing §e/z configure",
                    C.getLine()
            );
        }
    }

}
