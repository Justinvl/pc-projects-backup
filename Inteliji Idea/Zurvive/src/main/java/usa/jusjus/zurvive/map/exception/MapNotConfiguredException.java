package usa.jusjus.zurvive.map.exception;

public class MapNotConfiguredException extends Exception{

    public MapNotConfiguredException(String message){
        super(message);
    }

}
