package usa.jusjus.zurvive.states.game.listener;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import usa.devrocoding.synergy.assets.Synergy;
import usa.jusjus.zurvive.Zurvive;
import usa.synergy.minigame.assets.object.GameStates;

// This class wil have a bunch of events to either be cancelled or returned? Why? Because I'm lazy.
public class MechanicListener implements Listener {

    @EventHandler
    public void onItemPickUp(PlayerPickupItemEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onItemPickUp(PlayerDropItemEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onItemPickUp(BlockBreakEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onItemPickUp(BlockPlaceEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onItemPickUp(PlayerExpChangeEvent e){
        e.setAmount(0);
    }

    @EventHandler
    public void onItemPickUp(FoodLevelChangeEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onItemPickUp(CreatureSpawnEvent e){
        if (e.getSpawnReason() != CreatureSpawnEvent.SpawnReason.CUSTOM){
            e.getEntity().remove();
        }
    }

    @EventHandler
    public void onItemPickUp(PlayerItemHeldEvent e){
        if (Zurvive.getInstance().getGameState() == GameStates.STARTED) {
            ItemStack stack = e.getPlayer().getInventory().getItem(e.getNewSlot());
            if (stack == null || stack.getType() == Material.AIR) {
                e.setCancelled(true);
                return;
            }
            e.setCancelled(false);
        }
    }

}
