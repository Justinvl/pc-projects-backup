package usa.jusjus.zurvive.guns.commands;

import net.minecraft.server.v1_14_R1.PathEntity;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftCreature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.scheduler.BukkitRunnable;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.assets.C;
import usa.devrocoding.synergy.spigot.command.SynergyCommand;
import usa.devrocoding.synergy.spigot.user.object.MessageModification;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.entities.EntitiesManager;
import usa.jusjus.zurvive.map.MapManager;
import usa.jusjus.zurvive.wave.WaveManager;
import usa.jusjus.zurvive.wave.object.Wave;

public class CommandZurvive extends SynergyCommand {

    public CommandZurvive(Core plugin){
        super(plugin, "", true, "zurvive", "z");
    }

    /*
        - /z forcestart
        - /z unlockall
        - /z tempmaxplayers <maxplayers> (Change the max player temporarily)
     */

    @Override
    public void execute(SynergyUser synergyUser, Player player, String s, String[] strings) {
        EntitiesManager entManager = Zurvive.getInstance().getEntitiesManager();
        MapManager mapManager = Zurvive.getInstance().getMapManager();

        int spawns = mapManager.getMap().getAreas().get(0).getEnemySpawns().size();
        int enemies = Wave.getEnemiesSize() / spawns;

        Synergy.debug(spawns+" =SPAWNS");
        Synergy.debug(enemies+" =enemies");
//        entManager.spawn(mapManager.getMap().getAreas().get(0).getEnemySpawns().get(0), entManager.getZURVIVE_ZOMBIE());
        for(Location location : mapManager.getMap().getAreas().get(0).getEnemySpawns()){
            for(int i=0;i<enemies;i++) {
                entManager.spawn(location, entManager.getZURVIVE_ZOMBIE());
            }
        }

        if (strings.length > 0) {
            if (strings[0].equalsIgnoreCase("configure")) {
                //TODO: If map is already setup ask for confirmation, because people can't join while in setup mode
                //TODO: Setup mode = maintenance mode

                //TODO: Put server in maintenance mode
            }
        }else{
            synergyUser.sendModifactionMessage(
                    MessageModification.RAW,
                    C.getLineWithName("Zurvive Settings"),
                    "§e/z forcestart §f- §7Force start the game",
                    "§e/z unlockall §f- §7Coming Soon...",
                    "§e/z tempmaxplayers <maxplayers>§f- §7Coming Soon...",
                    "§e/z configure §f- §7Configure the map and enable maintenance mode",
                    C.getLine()
            );
        }
    }

    @Override
    public void execute(ConsoleCommandSender consoleCommandSender, String s, String[] strings) {

    }
}
