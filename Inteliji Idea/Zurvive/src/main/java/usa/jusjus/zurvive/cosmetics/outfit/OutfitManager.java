package usa.jusjus.zurvive.cosmetics.outfit;

import lombok.Getter;
import usa.devrocoding.synergy.services.sql.SQLDataType;
import usa.devrocoding.synergy.services.sql.SQLDefaultType;
import usa.devrocoding.synergy.services.sql.TableBuilder;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.Module;
import usa.jusjus.zurvive.assets.playercache.object.PlayerCache;
import usa.jusjus.zurvive.cosmetics.outfit.listener.PlayerCacheLoadListener;
import usa.jusjus.zurvive.cosmetics.outfit.object.Outfit;
import usa.jusjus.zurvive.cosmetics.outfit.object.OutfitCategory;
import usa.jusjus.zurvive.cosmetics.outfit.outfits.monsters.Skeleton;
import usa.jusjus.zurvive.cosmetics.outfit.outfits.monsters.Zombie;
import usa.jusjus.zurvive.cosmetics.outfit.outfits.special.Dragonary;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OutfitManager extends Module {

    public OutfitManager(Core plugin){
        super(plugin, "OutfitManager", false);

        initDatabase();

        registerOutfits(
                // Monsters
                new Zombie(),
                new Skeleton(),

                // Special
                new Dragonary()
        );

        registerListener(
                new PlayerCacheLoadListener()
        );
    }

    @Override
    public void reload(String s) {

    }

    private void initDatabase(){
        new TableBuilder("outfits", getPlugin().getDatabaseManager())
                .addColumn("uuid", SQLDataType.VARCHAR, 300,false, SQLDefaultType.NO_DEFAULT, false)
                .addColumn("outfit", SQLDataType.VARCHAR, 100,false, SQLDefaultType.NO_DEFAULT, false)
                .addColumn("selected", SQLDataType.BIT, -1,false, SQLDefaultType.CUSTOM.setCustom(false), false)
                .addColumn("unlocked_on", SQLDataType.DATE, -1, false, SQLDefaultType.NO_DEFAULT, false)
                .execute();
    }

    public void updateOutfits(PlayerCache playerCache){
        Core.getPlugin().getRunnableManager().runTaskAsynchronously("update outfits", core -> {
                for(Outfit outfit : playerCache.getRecentlyUnlockedOutfits()){
                    Core.getPlugin().getDatabaseManager().insert("outfits", new HashMap<String, Object>(){{
                        put("uuid", playerCache.getUuid());
                        put("outfit", outfit.getName());
                        put("selected", playerCache.isOutfitSelected(outfit));
                        put("unlocked_on", new Date(System.currentTimeMillis()));
                    }});
                }
        });
    }

//    public Outfit getOutfitByClass(Class<?extends Outfit> clazz){
//        try{
//            return clazz.newInstance();
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    public Outfit getOutfitByName(String name){
//        try{
//            return Class.forName(Zombie.class.getPackage().getName() + "." + name)
//                    .asSubclass(Outfit.class)
//                    .newInstance();
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return null;
//    }

    public Outfit getOutfitByName(String name){
        for(OutfitCategory outfitCategory : OutfitCategory.values()){
            for(Outfit outfit : outfitCategory.getOutfits()){
                if (outfit.getName().equalsIgnoreCase(name)){
                    return outfit;
                }
            }
        }
        return null;
    }

//    private void registerOutfits(Outfit... outfits){
//        for(Outfit outfit : outfits){
//            if (this.outfits.containsKey(outfit.outfitCategory())){
//                List<Outfit> list = this.outfits.get(outfit.outfitCategory());
//                list.add(outfit);
//                this.outfits.put(outfit.outfitCategory(), list);
//            }else{
//                this.outfits.put(outfit.outfitCategory(), new ArrayList<Outfit>(){{add(outfit);}});
//            }
//        }
//    }

    private void registerOutfits(Outfit... outfits){
        for(Outfit outfit : outfits){
            outfit.outfitCategory().getOutfits().add(outfit);
        }
    }

}
