package usa.jusjus.zurvive.cosmetics;

import lombok.Getter;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.Module;
import usa.jusjus.zurvive.assets.playercache.PlayerCacheManager;
import usa.jusjus.zurvive.assets.playercache.listener.PlayerJoinListener;
import usa.jusjus.zurvive.assets.playercache.listener.PlayerQuitListener;
import usa.jusjus.zurvive.cosmetics.outfit.OutfitManager;

public class CosmeticManager extends Module {

    @Getter
    private OutfitManager outfitManager;
    @Getter
    private PlayerCacheManager playerCacheManager;

    public CosmeticManager(Core plugin){
        super(plugin, "Cosmetic Manager", false);

        this.playerCacheManager = new PlayerCacheManager(plugin);
        this.outfitManager = new OutfitManager(plugin);

        registerListener(
                new PlayerJoinListener(this),
                new PlayerQuitListener(this)
        );
    }

    @Override
    public void reload(String s) {

    }
}
