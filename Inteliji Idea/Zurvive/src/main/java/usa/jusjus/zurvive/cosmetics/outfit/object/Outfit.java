package usa.jusjus.zurvive.cosmetics.outfit.object;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import usa.devrocoding.synergy.spigot.utilities.ItemBuilder;

import java.util.ArrayList;
import java.util.List;

public abstract class Outfit {

    private ChatColor outfitColorName = ChatColor.RED;
    private String[] notUnlocked = new String[]{
            "§cYou haven't unlocked this outfit yet!",
            "§cClick to unlock this outfit with your coins. "
    }, unlocked = new String[]{
            "§aYou have unlocked this cool outfit.",
            "§aClick to wear this outfit anywhere you go."
    };

    public abstract OutfitCategory outfitCategory();
    public abstract int coins();
    public abstract String[] description();
    public abstract ItemStack head();
    public abstract ItemStack chestplate();
    public abstract ItemStack leggs();
    public abstract ItemStack boots();

    public List<ItemStack> getContents(boolean unlocked){
        return new ArrayList<ItemStack>(){{
            add(getHead(unlocked));
            add(getChestplate(unlocked));
            add(getLeggings(unlocked));
            add(getBoots(unlocked));
        }};
    }

    public List<ItemStack> getRawContents(){
        return new ArrayList<ItemStack>(){{
            add(head());
            add(chestplate());
            add(leggs());
            add(boots());
        }};
    }

    public ItemStack getHead(boolean unlocked){
        return build(head(), "Head", unlocked);
    }

    public ItemStack getChestplate(boolean unlocked){
        return build(chestplate(), "Shirt", unlocked);
    }

    public ItemStack getLeggings(boolean unlocked){
        return build(leggs(), "Pants", unlocked);
    }

    public ItemStack getBoots(boolean unlocked){
        return build(boots(), "Shoes", unlocked);
    }

    private ItemStack build(ItemStack item, String type, boolean unlocked){
        ItemBuilder builder = new ItemBuilder(item);

        builder.setName(outfitColorName+getName()+" "+type);

        if (!unlocked){
            builder.addLore("§e§lCOINS: §a"+coins(), ChatColor.ITALIC+"(Price for the entire outfit)", " ");
        }

        builder.addLore(description());
        builder.addLore(" ", "§6Status: "+(unlocked ? "§a§lUNLOCKED" : "§c§lNOT UNLOCKED"), " ");
        builder.addLore(unlocked ? this.unlocked : this.notUnlocked);
        builder.addItemFlags(ItemFlag.values());
        return builder.build();
    }

    @Override
    public String toString() {
        return super.getClass().getSimpleName();
    }

    public String getName(){
        return this.toString();
    }
}
