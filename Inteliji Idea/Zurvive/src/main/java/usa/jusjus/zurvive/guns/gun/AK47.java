package usa.jusjus.zurvive.guns.gun;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import usa.jusjus.zurvive.guns.object.Gun;

public class AK47 extends Gun {

    @Override
    public String name() {
        return "AK-47";
    }

    @Override
    public String[] description() {
        return new String[]{"This is your AK-47"};
    }

    @Override
    public Integer maxAmmo() {
        return -1;
    }

    @Override
    public Integer magazineAmmo() {
        return 30;
    }

    @Override
    public Sound sound() {
        return Sound.ENTITY_FIREWORK_ROCKET_BLAST;
    }

    @Override
    public double cooldown() {
        return 0.2;
    }

    @Override
    public long reloadSpeed() {
        return 1;
    }

    @Override
    public double range() {
        return 30;
    }

    @Override
    public double damage() {
        return 7;
    }

    @Override
    public Material material() {
        return Material.IRON_HOE;
    }

    @Override
    public Particle particleBullet() {
        return Particle.FLAME;
    }
}
