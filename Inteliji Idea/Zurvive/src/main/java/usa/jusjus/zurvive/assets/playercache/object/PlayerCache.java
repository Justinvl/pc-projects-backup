package usa.jusjus.zurvive.assets.playercache.object;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.jusjus.zurvive.cosmetics.outfit.gui.VisualOutfitGUI;
import usa.jusjus.zurvive.cosmetics.outfit.object.Outfit;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PlayerCache {

    @Getter
    private UUID uuid;
    @Getter
    private double coins;
    @Getter
    private List<Outfit> outfits;
    @Getter
    private List<Outfit> recentlyUnlockedOutfits = new ArrayList<>();
    @Getter @Setter
    private Outfit selectedOutfit = null;

    public PlayerCache(UUID uuid, double coins, List<Outfit> outfits){
        this.uuid = uuid;
        this.coins = coins;
        this.outfits = outfits;
    }

    public boolean isOutfitSelected(Outfit outfit){
        return selectedOutfit != null && selectedOutfit.equals(outfit);
    }

    public boolean selectOutfit(Outfit outfit, SynergyUser synergyUser){
        // Is outfit unlocked
        if (!this.outfits.contains(outfit)){
            return false;
        }
        // Is outfit already selected
        if (!isOutfitSelected(outfit)) {
            setSelectedOutfit(outfit);
            new VisualOutfitGUI(Core.getPlugin(), outfit).insert(synergyUser.getPlayer().getInventory(), synergyUser);
            return true;
        }
        return false;
    }

    public void removeSelectedOutfit(PlayerInventory inventory){
        this.selectedOutfit = null;
        // Clear their outfit
        inventory.setArmorContents(new ItemStack[inventory.getArmorContents().length]);
    }

}
