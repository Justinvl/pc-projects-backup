package usa.jusjus.zurvive.cosmetics.outfit.gui;

import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.gui.Gui;
import usa.devrocoding.synergy.spigot.gui.GuiElement;
import usa.devrocoding.synergy.spigot.gui.object.GuiSize;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.jusjus.zurvive.cosmetics.outfit.object.Outfit;

public class VisualOutfitGUI extends Gui {

    private final Outfit outfit;
    private int place = 39;

    public VisualOutfitGUI(Core plugin, Outfit outfit){
        super(plugin, "Player Inventory", GuiSize.SIX_ROWS, false);

        this.outfit = outfit;

        setup();
    }

    @Override
    public void setup() {
        Synergy.debug("Vis GUI - 1");
        for(ItemStack item : outfit.getRawContents()){
            Synergy.debug("Vis GUI - 2");
            addElement(place, new GuiElement() {
                @Override
                public ItemStack getIcon(SynergyUser synergyUser) {
                    return item;
                }

                @Override
                public void click(SynergyUser synergyUser, ClickType clickType) {

                }
            });
            Synergy.debug("Vis GUI - "+place);
            place--;
        }
    }
}
