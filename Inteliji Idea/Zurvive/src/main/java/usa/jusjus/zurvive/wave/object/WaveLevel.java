package usa.jusjus.zurvive.wave.object;

import lombok.Getter;

public enum WaveLevel {

    LEVEL_1(1, 150, 2),
    LEVEL_2(2, 200, 3),
    LEVEL_3(3, 350, 3),
    LEVEL_4(4, 400, 3),
    LEVEL_5(5, 520, 3);

    @Getter
    private int level, enemies, enemyDamage;

    private WaveLevel(int level, int enemies, int enemyDamage){
        this.level = level;
        this.enemies = enemies;
        this.enemyDamage = enemyDamage;
    }

}
