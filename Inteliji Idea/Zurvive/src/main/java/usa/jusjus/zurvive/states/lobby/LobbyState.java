package usa.jusjus.zurvive.states.lobby;

import lombok.Getter;
import org.bukkit.Bukkit;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.scoreboard.ScoreboardPolicy;
import usa.devrocoding.synergy.spigot.user.object.MessageModification;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.devrocoding.synergy.spigot.utilities.UtilTime;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.states.lobby.scoreboard.LobbyScoreboard;
import usa.synergy.minigame.assets.object.GameState;

import java.util.ArrayList;
import java.util.List;

public class LobbyState extends GameState {

    private boolean configured;
    private int timer = 34;
    @Getter
    public int count = timer;
    @Getter
    public int maxPlayers = 4;
    @Getter
    public int minPlayers = 1;

    private List<Integer> announcers = new ArrayList<Integer>(){{
        add(30);add(15);add(10);add(5);add(4);add(3);add(2);add(1);
    }};

    @Override
    public ScoreboardPolicy scoreboard() {
        return new LobbyScoreboard();
    }

    @Override
    public GameState nextState() {
        return new usa.jusjus.zurvive.states.game.GameState();
    }

    @Override
    public boolean moveToNext() {
        return count <= 0;
    }

    @Override
    public void init() {
        try{

            if (Zurvive.getInstance().getMapManager().isMapConfigured()){
                System.out.println(Zurvive.getInstance().getMapManager().isMapConfigured());
                this.configured = true;
            }
        }catch (Exception e){
            this.configured = false;
        }
    }

    @Override
    public void tick(Core synergy) {
        if (!configured){
            return;
        }
        if (count > 0 && Bukkit.getOnlinePlayers().size() > 0) {

            // Count to zero
            count--;

            for(SynergyUser user : synergy.getUserManager().getOnlineUsers()){
                if (announcers.contains(count)) {
                    user.sendModifactionMessage(
                            MessageModification.RAW,
                            "§cGame starts in §e" + count + " seconds!"
                    );
                    user.getDisplay().sendTitleAndSubTitle("§e"+count, "§cseconds until game starts", 0, 25,
                            // If the timer is not between 1 - 5 then give it a fadeOut
                            (count > 5 || count <= 1 ? 2 : 0)
                    );
                }
            }

            // Update scoreboard
            synergy.getScoreboardManager().update();
        }else{
            count = timer;
        }
    }

    @Override
    public void deInit() {
        Zurvive.getInstance().getStatesManager().state = nextState();
    }

    public String getLobbyTimer(){
        if (Bukkit.getOnlinePlayers().size() > 0
                && Bukkit.getOnlinePlayers().size() >= getMinPlayers()){
            int timer = getCount();
            if (timer > 0) {
                return "Starting in §e"+UtilTime.formatTime(timer);
            }
        }
        return "Waiting...";
    }

}
