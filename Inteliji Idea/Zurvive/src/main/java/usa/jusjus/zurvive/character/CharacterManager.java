package usa.jusjus.zurvive.character;

import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.Module;
import usa.devrocoding.synergy.spigot.hologram.object.HologramLine;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.devrocoding.synergy.spigot.utilities.UtilLoc;
import usa.jusjus.zurvive.Zurvive;
import usa.jusjus.zurvive.assets.playercache.event.PlayerCacheLoadEvent;
import usa.jusjus.zurvive.assets.playercache.object.PlayerCache;
import usa.jusjus.zurvive.character.object.CharacterShowcase;
import usa.jusjus.zurvive.cosmetics.outfit.object.Outfit;
import usa.jusjus.zurvive.map.exception.MapNotConfiguredException;
import usa.jusjus.zurvive.specialeffects.ParticleType;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CharacterManager extends Module {

    @Getter
    private Map<UUID, CharacterShowcase> showcases = new HashMap<>();

    public CharacterManager(Core plugin){
        super(plugin, "Character Manager", false);

        registerListener(
                this
        );

        ParticleType.Sparklez.Activate(Zurvive.getInstance().getMapManager().getMap().getShowcaseLocation());

        plugin.getRunnableManager().runTaskTimerAsynchronously("Character Update", (echo) -> {
            for(PlayerCache playerCache : Zurvive.getInstance().getCosmeticManager().getPlayerCacheManager().getAllPlayerCaches().values()){
                SynergyUser synergyUser = echo.getUserManager().getUser(playerCache.getUuid());
                if (getShowcases().containsKey(playerCache.getUuid())) {
                    getShowcases().get(playerCache.getUuid()).send(
                            synergyUser,
                            playerCache.getSelectedOutfit()
                    );
                }
            }
        }, 40L, 20L);
    }

    @Override
    public void reload(String s) {

    }


    private CharacterShowcase createShowcase(SynergyUser synergyUser, Outfit outfit){
        CharacterShowcase characterShowcase = new CharacterShowcase(
                Zurvive.getInstance().getMapManager().getMap().getShowcaseLocation(),
                new HologramLine() {
                    @Override
                    public String getMessage(SynergyUser synergyUser) {
                        return "§e"+synergyUser.getName()+"'s Character §7(Click to modify)";
                    }
                },
                // null = always true
                null
        );

        characterShowcase.send(synergyUser, outfit);
        return characterShowcase;
    }

    @EventHandler
    public void onUserJoin(PlayerCacheLoadEvent e){
        try{
            if (Zurvive.getInstance().getMapManager().isMapConfigured()){
                showcases.put(e.getSynergyUser().getUuid(), createShowcase(e.getSynergyUser(), e.getPlayerCache().getSelectedOutfit()));
            }
        }catch (MapNotConfiguredException disabled){}
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e){
        showcases.remove(e.getPlayer().getUniqueId());
    }
}
