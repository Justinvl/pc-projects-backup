package usa.jusjus.zurvive.wave.object;

import lombok.Getter;
import org.bukkit.Sound;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.assets.C;
import usa.devrocoding.synergy.spigot.user.object.MessageModification;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;

public class Wave {

    @Getter
    static protected int waveNumber = 0;
    @Getter
    static int enemiesSize = 20;
    @Getter
    static int damage;
    @Getter
    static boolean special;
    @Getter
    static int specialEnemiesSize;
    @Getter
    public int remainingEnemies;

    public Wave(){
        waveNumber++;

        special = checkSpecial();

        if (waveNumber > 0)
            enemiesSize += 10;
        remainingEnemies = enemiesSize;

        if (isSpecial()){
            playerSpecialWaveAnimation();
        }else {
            playerWaveAnimation();
        }
    }

    public void withdrawRemaining(){
        this.remainingEnemies--;
    }

    public boolean checkSpecial(){
        for(int i=10;i<500;i+=10){
            if (waveNumber == i){
                return true;
            }
        }
        return false;
    }

//    public WaveLevel getLevel(){
//
//    }

    private void playerWaveAnimation(){
        for(SynergyUser user : Core.getPlugin().getUserManager().getOnlineUsers()){
            String update = "§a+20 §7Enemies | §7Enemies: §a"+getEnemiesSize();

            user.getDisplay().sendTitleAndSubTitle(
                    "§c§lWave "+getWaveNumber(),
                    update,
                    20, 20*4, 20*2
            );
            user.sendModifactionMessage(
                    MessageModification.CENTERED,
                    C.getLine(),
                    "",
                    "§c§lWave "+getWaveNumber()+" has started",
                    update,
                    "",
                    C.getLine()
            );
            user.getPlayer().playSound(user.getPlayer().getLocation(), Sound.EVENT_RAID_HORN, 10, 1);
        }
    }

    private void playerSpecialWaveAnimation(){
        for(SynergyUser user : Core.getPlugin().getUserManager().getOnlineUsers()) {
            user.getDisplay().sendTitleAndSubTitle(
                    "§c§lSpecial Wave - Prepare yourself",
                    "§cEnemies: §b"+getSpecialEnemiesSize(),
                    20, 20 * 4, 20 * 2
            );
        }
    }

}
