package usa.jusjus.mapmaker;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.map.MinecraftFont;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Core extends JavaPlugin { ;

    @Override
    public void onEnable() {

    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase("map")){
            if (sender instanceof Player){
                Player player = ((Player) sender);
                if (args.length > 1 && args.length < 4) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            player.sendMessage("§eMaking the map for you..... One moment....");
                            try {
                                boolean outputOne = Boolean.valueOf(args[1]);
                                String url = args[0];
                                BufferedImage image = ImageIO.read(new URL(url));

                                System.out.println(outputOne);

                                List<ItemStack> items = new MapBuilder() //Initializing the MapBuilder class
                                        .setRenderOnce(true) //Since this will be a static image, we only want it rendered once
                                        .setImage(image, outputOne) //Setting an image from a URL as background
                                        //                                        .addText(0, 0, MinecraftFont.Font, "Did you know a block is not round?") //Adding some text with the Minecraft default font at 0, 0
                                        //                                        .addCursor(20, 20, MapBuilder.CursorDirection.EAST, MapBuilder.CursorType.WHITE_DOT) //Adding a cursor (in our case a white dot) to the map
                                        .build(); //Finally using the build method to generate an ItemStack

                                for(ItemStack stack : items){
                                    player.getInventory().addItem(stack);
                                }
                                player.sendMessage("§eEnjoy your map(s).");
                            } catch (Exception e) {
                                player.sendMessage("§cSomething went wrong, check console or try another URL!");
                                e.printStackTrace();
                            }
                        }
                    }.runTaskAsynchronously(this);
                }
            }
        }
        return true;
    }


}
