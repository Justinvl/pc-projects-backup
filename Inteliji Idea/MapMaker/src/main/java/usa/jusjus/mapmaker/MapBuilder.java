package usa.jusjus.mapmaker;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.MapMeta;
import org.bukkit.map.*;
import org.bukkit.plugin.AuthorNagException;

import javax.annotation.Nonnull;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class MapBuilder {

    private final int WIDTH = 128;
    private final int HEIGHT = 128;
    private int lines;
    private int columns;

    public static final String VERSION = "1.5";
    private List<Text> texts;
    private BufferedImage image;
    private MapCursorCollection cursors;
    private List<BufferedImage> images = new ArrayList<>();
    
    private boolean rendered;
    private boolean renderOnce;
    private boolean isOnePointFourteen;
    private boolean outputOne;

    public MapBuilder() {
        cursors = new MapCursorCollection();
        texts = new ArrayList<>();
        rendered = false;
        renderOnce = true;
        outputOne = true;
        isOnePointFourteen = Bukkit.getVersion().contains("1.14");
    }

    /**
     * Get the image that's being used
     *
     * @return the image used
     */
    public BufferedImage getImage() {
        return image;
    }

    /**
     * Set an image to be used
     *
     * @param image the buffered image to use
     * @return the instance of this class
     */
    public MapBuilder setImage(@Nonnull BufferedImage image, boolean outputOne) {
        this.image = image;
        this.outputOne = outputOne;

        if (!outputOne){
            cutOutImages(image);
        }else{
            images.add(image);
        }
        return this;
    }

    /**
     * Set and image to be used
     *
     * @param x, y the coordinates to add the text
     * @param font the font to be used
     * @param text the string that will be displayed
     * @return the instance of this class
     */
    public MapBuilder addText(@Nonnull int x, @Nonnull int y, @Nonnull MapFont font, @Nonnull String text) {
        this.texts.add(new Text(x, y, font, text));
        return this;
    }

    /**
     * Gets the list of all the texts used
     *
     * @return a List of all the texts
     */
    public List<Text> getTexts() {
        return texts;
    }

    /**
     * Adds a cursor to the map
     *
     * @param x, y the coordinates to add the cursor
     * @param direction the direction to display the cursor
     * @param type the type of the cursor
     * @return the instance of this class
     */
    @SuppressWarnings("deprecation")
    public MapBuilder addCursor(@Nonnull int x, @Nonnull int y, @Nonnull CursorDirection direction, @Nonnull CursorType type) {
        cursors.addCursor(x, y, (byte) direction.getId(), (byte) type.getId());
        return this;
    }

    /**
     * Gets all the currently used cursors
     *
     * @return a MapCursorCollection with all current cursors
     */
    public MapCursorCollection getCursors() {
        return cursors;
    }

    /**
     * Sets whether the image should only be rendered once.
     * Good for static images and reduces lag.
     *
     * @param renderOnce the value to determine if it's going to be rendered once
     * @return the instance of this class
     */
    public MapBuilder setRenderOnce(@Nonnull boolean renderOnce) {
        this.renderOnce = renderOnce;
        return this;
    }

    private BufferedImage resize(BufferedImage img, int newW, int newH) {
        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        return dimg;
    }

    private void cutOutImages(@Nonnull final BufferedImage imgh) {
        final int originalWidth = image.getWidth();
        final int originalHeight = image.getHeight();
        BufferedImage image = imgh;

        System.out.println(originalWidth + " =originalWidth");
        System.out.println(originalHeight + " =originalHeight");

        int rows = (int) Math.ceil(originalWidth / WIDTH);
        int lines = (int) Math.ceil(originalHeight / HEIGHT);

        System.out.println(rows + " =ROWS");
        System.out.println(lines + " =LINES");

//        double rawX = originalWidth/rows;
//        double rawY = originalWidth/rows;

        int x = (int) Math.ceil(originalWidth/rows);
        int y = (int) Math.ceil(originalHeight/lines);
        System.out.println(x + " =X");
        System.out.println(y + " =Y");

        int xCount = 0, yCount = 0;
        System.out.println("-------------");

        for(int i = 0;i<lines;i++){
            System.out.println(yCount + " =yCount");
            if (originalHeight > yCount){
                for(int line=0;line<rows;line++){
                    System.out.println(xCount + " =xCount");
                    if (xCount > originalWidth){
                        break;
                    }
                    BufferedImage subImage = image.getSubimage(xCount, yCount, x, y);
                    image = imgh;
                    images.add(subImage);
                    xCount += x;
                }
            }
            xCount = 0;
            yCount += y;
        }
        System.out.println("-------------");
    }

    /**
     * Builds an ItemStack of the map.
     *
     * @return the ItemStack of the map containing what's been set from the above methods
     */
    @SuppressWarnings("deprecation")
    public List<ItemStack> build() {
        List<ItemStack> items = new ArrayList<>();

        for(BufferedImage img : this.images) {
            System.out.println("------ IMG CREATE -------");
            MapView map = Bukkit.getServer().createMap(Bukkit.getWorlds().get(0));
            ItemStack item = new ItemStack(isOnePointFourteen ? Material.MAP : Material.valueOf("MAP"));

            System.out.println(img.getType());

            map.setScale(MapView.Scale.FAR);
            map.getRenderers().forEach(map::removeRenderer);
            map.addRenderer(new MapRenderer() {
                @Override
                public void render(MapView mapView, MapCanvas mapCanvas, Player player) {
                    if (rendered && renderOnce) {
                        return;
                    }

                    if (player != null && player.isOnline()) {
                        if (img != null) {
                            mapCanvas.drawImage(0, 0, resize(img, 128, 128));
                        }

                        if (!texts.isEmpty()) {
                            texts.forEach(text -> mapCanvas.drawText(text.getX(), text.getY(), text.getFont(), text.getMessage()));
                        }

                        if (cursors.size() > 0) {
                            mapCanvas.setCursors(cursors);
                        }

                        rendered = true;
                    }
                }
            });

            if (isOnePointFourteen) {
                MapMeta mapMeta = (MapMeta) item.getItemMeta();
                mapMeta.setMapView(map);
                mapMeta.setScaling(true);
                item.setItemMeta(mapMeta);
            } else {
                item.setDurability(getMapId(map));
            }
            items.add(item);

        }
        return items;
    }


    /**
     * Gets a map id cross-version using reflection
     *
     * @param mapView the map to get the id
     * @return the instance of this class
     */
    private short getMapId(@Nonnull MapView mapView) {
        try {
            return (short) mapView.getId();
        } catch (NoSuchMethodError ex) {
            try {
                return (short) Class.forName("org.bukkit.map.MapView").getMethod("getId", (Class<?>[]) new Class[0])
                        .invoke(mapView, new Object[0]);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
                    | NoSuchMethodException | SecurityException | ClassNotFoundException e) {
                e.printStackTrace();
                return -1;
            }
        }
    }

    /**
     * An enum containing user friendly cursor directions. Instead of using the integers, you can instead use this enum
     */
    public enum CursorDirection {
        SOUTH(0), SOUTH_WEST_SOUTH(1), SOUTH_WEST(2), SOUTH_WEST_WEST(3), WEST(4), NORTH_WEST_WEST(5), NORTH_WEST(6),
        NORTH_WEST_NORTH(7), NORTH(8), NORTH_EAST_NORTH(9), NORTH_EAST(10), NORTH_EAST_EAST(11), EAST(12),
        SOUTH_EAST_EAST(13), SOUNT_EAST(14), SOUTH_EAST_SOUTH(15);

        private final int id;

        private CursorDirection(@Nonnull int id) {
            this.id = id;
        }

        /**
         * Returns the actual integer to use
         *
         * @return the integer of the specified enum type 
         */
        public int getId() {
            return this.id;
        }
    }

    /**
     * An enum containing user friendly cursor types. Instead of using the integers, you can instead use this enum
     */
    public enum CursorType {
        WHITE_POINTER(0), GREEN_POINTER(1), RED_POINTER(2), BLUE_POINTER(3), WHITE_CLOVER(4), RED_BOLD_POINTER(5),
        WHITE_DOT(6), LIGHT_BLUE_SQUARE(7);

        private final int id;

        private CursorType(@Nonnull int id) {
            this.id = id;
        }

        /**
         * Returns the actual integer to use
         *
         * @return the integer of the specified enum type 
         */
        public int getId() {
            return this.id;
        }
    }
}

/**
 * A storage class to save text information to later be used in order to write in maps
 */
class Text {
	
    private int x;
    private int y;
    private MapFont font;
    private String message;

    public Text(@Nonnull int x, @Nonnull int y, @Nonnull MapFont font, @Nonnull String message) {
        setX(x);
        setY(y);
        setFont(font);
        setMessage(message);
    }

    /**
     * Gets the x position for the text to be displayed
     *
     * @return the x position
     */
    public int getX() {
        return x;
    }

    /**
    * Sets the x position of the text to display it
    *
    * @param x the x postion
    */
    public void setX(@Nonnull int x) {
        this.x = x;
    }

    /**
     * Gets the y position for the text to be displayed
     *
     * @return the y position
     */
    public int getY() {
        return y;
    }

    /**
     * Sets the y position of the text to display it
     *
     * @param y the y position
     */
    public void setY(@Nonnull int y) {
        this.y = y;
    }

    /**
     * Gets the font to be used
     *
     * @return the MapFont that is used
     */
    public MapFont getFont() {
        return font;
    }

    /**
     * Sets what font should be used
     *
     * @param font the actual font
     */
    public void setFont(@Nonnull MapFont font) {
        this.font = font;
    }

    /**
     * Gets what test will be displayed
     *
     * @return the text
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets what text will be displayed
     *
     * @param message the actual text
     */
    public void setMessage(@Nonnull String message) {
        this.message = message;
    }
}