package core.jusjus.usa.suppliers;

import com.earth2me.essentials.*;
import core.jusjus.usa.clickable.Value;
import core.jusjus.usa.clickable.ValueSupplier;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.*;
import org.bukkit.entity.*;
import com.earth2me.essentials.commands.*;
import net.ess3.api.*;
import java.util.*;
import org.bukkit.plugin.java.*;
import org.bukkit.*;

public final class EWarpSupplier implements ValueSupplier<Essentials>
{
    private static final Essentials essentials;
    
    @Override
    public Essentials getPlugin() {
        return EWarpSupplier.essentials;
    }
    
    @Override
    public String getCommand() {
        return "warp";
    }
    
    @Override
    public String[] getStrings() {
        return new String[0];
    }
    
    @Override
    public String getExec() {
        return "/warp ";
    }
    
    @Override
    public String getPrefix() {
        return "";
    }
    
    @Override
    public String getSuffix() {
        return "";
    }
    
    @Override
    public BaseComponent[] empty() {
        return new ComponentBuilder("There are no warps").color(ChatColor.RED).create();
    }
    
    @Override
    public Collection<Value> getValuesFor(final Essentials essentials, final Player player) {
        final List<Value> list = new ArrayList<Value>();
        final Collection<String> warps = (Collection<String>)essentials.getWarps().getList();
        for (final String warp : warps) {
            boolean invalidWorld = false;
            try {
                essentials.getWarps().getWarp(warp);
            }
            catch (WarpNotFoundException e) {
                continue;
            }
            catch (InvalidWorldException e2) {
                invalidWorld = true;
            }
            list.add(new Value(warp, !invalidWorld));
        }
        return list;
    }
    
    @Override
    public BaseComponent[] createHeader() {
        return new ComponentBuilder("List of warps: ").color(ChatColor.DARK_GREEN).create();
    }
    
    @Override
    public BaseComponent[] createFooter() {
        return new ComponentBuilder("You can click on a warp name to teleport").color(ChatColor.GOLD).create();
    }
    
    static {
        essentials = (Essentials)Bukkit.getServer().getPluginManager().getPlugin("Essentials");
    }
}