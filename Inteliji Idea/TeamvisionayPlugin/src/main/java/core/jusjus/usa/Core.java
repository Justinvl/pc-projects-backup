package core.jusjus.usa;

import core.jusjus.usa.clickable.CWListener;
import core.jusjus.usa.suppliers.*;
import core.jusjus.usa.suppliers.MultiverseSupplier;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Core extends JavaPlugin implements Listener
{
    public void onEnable() {
        System.out.println(this.getServer().getPluginManager().isPluginEnabled("Essentials"));
        if (this.getServer().getPluginManager().isPluginEnabled("Essentials")) {
            System.out.println("2");
            this.getServer().getPluginManager().registerEvents(new CWListener(new EWarpSupplier()), this);
            this.getServer().getPluginManager().registerEvents(new CWListener(new EHomeSupplier()), this);
        }
        if (this.getServer().getPluginManager().isPluginEnabled("Multiverse-Core")) {
            this.getServer().getPluginManager().registerEvents(new CWListener(new MultiverseSupplier()), this);
        }
    }
}
