package core.jusjus.usa.suppliers;

import com.earth2me.essentials.*;
import com.earth2me.essentials.IUser;
import core.jusjus.usa.clickable.Value;
import core.jusjus.usa.clickable.ValueSupplier;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.*;
import org.bukkit.entity.*;
import net.ess3.api.*;
import java.util.*;
import org.bukkit.plugin.java.*;
import org.bukkit.*;

public final class EHomeSupplier implements ValueSupplier<Essentials>
{
    private static final Essentials essentials;
    
    @Override
    public Essentials getPlugin() {
        return EHomeSupplier.essentials;
    }
    
    @Override
    public String getCommand() {
        return "home";
    }
    
    @Override
    public String[] getStrings() {
        return new String[0];
    }
    
    @Override
    public String getExec() {
        return "/home ";
    }
    
    @Override
    public String getPrefix() {
        return "";
    }
    
    @Override
    public String getSuffix() {
        return "";
    }
    
    @Override
    public BaseComponent[] empty() {
        return new ComponentBuilder("You have no homes").color(ChatColor.RED).create();
    }
    
    @Override
    public Collection<Value> getValuesFor(final Essentials essentials, final Player player) {
        final IUser user = (IUser)essentials.getUser(player);
        final List<Value> list = new ArrayList<Value>();
        final Collection<String> homes = (Collection<String>)user.getHomes();
        for (final String home : homes) {
            boolean invalidWorld = false;
            try {
                user.getHome(home);
            }
            catch (InvalidWorldException e) {
                invalidWorld = true;
            }
            catch (Exception e2) {
                continue;
            }
            list.add(new Value(home, !invalidWorld));
        }
        return list;
    }
    
    @Override
    public BaseComponent[] createHeader() {
        return new ComponentBuilder("List of homes: ").color(ChatColor.DARK_GREEN).create();
    }
    
    @Override
    public BaseComponent[] createFooter() {
        return new ComponentBuilder("You can click on a home name to teleport").color(ChatColor.GOLD).create();
    }
    
    static {
        essentials = (Essentials)Bukkit.getServer().getPluginManager().getPlugin("Essentials");
    }
}
