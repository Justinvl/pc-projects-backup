package core.jusjus.usa.suppliers;

import com.onarandombox.MultiverseCore.*;
import core.jusjus.usa.clickable.Value;
import core.jusjus.usa.clickable.ValueSupplier;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.*;
import org.bukkit.entity.*;
import com.onarandombox.MultiverseCore.api.*;
import org.bukkit.command.*;
import java.util.*;
import org.bukkit.plugin.java.*;
import org.bukkit.*;

public final class MultiverseSupplier implements ValueSupplier<MultiverseCore>
{
    private static final MultiverseCore essentials;
    
    @Override
    public MultiverseCore getPlugin() {
        return MultiverseSupplier.essentials;
    }
    
    @Override
    public String getCommand() {
        return null;
    }
    
    @Override
    public String[] getStrings() {
        return new String[] { "mv list", "mvl", "mvlist" };
    }
    
    @Override
    public String getExec() {
        return "/mv tp ";
    }
    
    @Override
    public String getPrefix() {
        return "";
    }
    
    @Override
    public String getSuffix() {
        return "";
    }
    
    @Override
    public BaseComponent[] empty() {
        return new ComponentBuilder("There are no worlds you can teleport to").color(ChatColor.RED).create();
    }
    
    @Override
    public Collection<Value> getValuesFor(final MultiverseCore multiverse, final Player player) {
        final List<Value> list = new ArrayList<Value>();
        for (final MultiverseWorld world : multiverse.getMVWorldManager().getMVWorlds()) {
            if (multiverse.getMVPerms().canEnterWorld(player, world)) {
                if (world.isHidden() && !multiverse.getMVPerms().hasPermission((CommandSender)player, "multiverse.core.modify", true)) {
                    continue;
                }
                list.add(new Value(world.getName(), true));
            }
        }
        for (final String world2 : multiverse.getMVWorldManager().getUnloadedWorlds()) {
            if (!multiverse.getMVPerms().hasPermission((CommandSender)player, "multiverse.access." + world2, true)) {
                continue;
            }
            list.add(new Value(world2, false));
        }
        return list;
    }
    
    @Override
    public BaseComponent[] createHeader() {
        return new ComponentBuilder("List of worlds: ").color(ChatColor.DARK_GREEN).create();
    }
    
    @Override
    public BaseComponent[] createFooter() {
        return new ComponentBuilder("You can click on a world name to teleport").color(ChatColor.GOLD).create();
    }
    
    static {
        essentials = (MultiverseCore)Bukkit.getServer().getPluginManager().getPlugin("Multiverse-Core");
    }
}