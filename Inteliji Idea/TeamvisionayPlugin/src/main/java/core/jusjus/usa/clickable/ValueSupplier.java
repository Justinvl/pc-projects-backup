package core.jusjus.usa.clickable;

import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

public interface ValueSupplier<P extends JavaPlugin>
{
    P getPlugin();
    
    String getCommand();
    
    String[] getStrings();
    
    String getPrefix();
    
    String getSuffix();
    
    String getExec();
    
    BaseComponent[] empty();
    
    Collection<Value> getValuesFor(final P p0, final Player p1);
    
    BaseComponent[] createHeader();
    
    BaseComponent[] createFooter();
}
