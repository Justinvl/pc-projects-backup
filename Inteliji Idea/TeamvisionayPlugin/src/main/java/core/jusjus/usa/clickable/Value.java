package core.jusjus.usa.clickable;

public final class Value
{
    private final String name;
    private final boolean valid;
    
    public Value(final String name, final boolean valid) {
        this.name = name;
        this.valid = valid;
    }
    
    public String getName() {
        return this.name;
    }
    
    public boolean isValid() {
        return this.valid;
    }
}
