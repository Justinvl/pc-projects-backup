package core.jusjus.usa.clickable;

import org.bukkit.command.*;
import org.bukkit.event.player.*;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.chat.*;
import java.util.*;
import org.bukkit.event.*;

public final class CWListener implements Listener
{
    private final ValueSupplier valueSupplier;
    private final List<String> commands;
    private final List<String> strings;
    
    public CWListener(ValueSupplier valueSupplier) {
        this.valueSupplier = valueSupplier;
        if (valueSupplier.getCommand() != null) {
            final PluginCommand command = valueSupplier.getPlugin().getCommand(valueSupplier.getCommand());
            (this.commands = new ArrayList<String>(command.getAliases())).add(valueSupplier.getPrefix() + command.getName() + valueSupplier.getSuffix());
            this.strings = null;
        }
        else {
            this.strings = Arrays.asList(valueSupplier.getStrings());
            this.commands = null;
        }
    }
    
    @EventHandler
    public void onCommand(final PlayerCommandPreprocessEvent event) {
        final String message = event.getMessage().substring(1).toLowerCase();
        Label_0059: {
            if (event.getMessage().length() == 0 || this.commands != null) {
                if (this.commands.contains(message)) {
                    break Label_0059;
                }
            }
            else if (this.strings.contains(message)) {
                break Label_0059;
            }
            return;
        }
        event.setCancelled(true);
        final Collection<Value> values = this.valueSupplier.getValuesFor(this.valueSupplier.getPlugin(), event.getPlayer());
        if (values.isEmpty()) {
            event.getPlayer().spigot().sendMessage(this.valueSupplier.empty());
            return;
        }
        final BaseComponent[] header = this.valueSupplier.createHeader();
        final List<BaseComponent[]> builders = new LinkedList<BaseComponent[]>();
        builders.add(header);
        ComponentBuilder builder = new ComponentBuilder("");
        int i = 0;
        for (final Value value : values) {
            if (i % 10 == 0 && i != 0) {
                builders.add(builder.create());
                builder = new ComponentBuilder("");
            }
            final String name = value.getName();
            builder.append(name).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Teleport to ").color(ChatColor.GREEN).append(name).color(ChatColor.YELLOW).create())).color(value.isValid() ? ChatColor.YELLOW : ChatColor.RED).event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, this.valueSupplier.getExec() + name));
            if (++i != values.size()) {
                builder.append(", ").color(ChatColor.BLUE);
            }
        }
        builders.add(builder.create());
        builders.add(this.valueSupplier.createFooter());
        for (final BaseComponent[] message2 : builders) {
            event.getPlayer().spigot().sendMessage(message2);
        }
    }
}
