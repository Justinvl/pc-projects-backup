package usa.jusjus.zurvive.hub.lobby.listener;

import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.user.event.UserLoadEvent;
import usa.devrocoding.synergy.spigot.warp.object.Warp;
import usa.jusjus.zurvive.hub.lobby.gui.LobbyGUI;

public class UserLoadListener implements Listener {

    @EventHandler
    public void onUserLoadLobby(UserLoadEvent e){
        e.getUser().clean();
        e.getUser().getPlayer().setGameMode(GameMode.ADVENTURE);

        e.getUser().getPlayer().setPlayerListHeaderFooter(
                "§eWelcome to §c§lZURVIVE §d</> §eTry to zurvive"+"\n"+
                    "§7play.zurvive.net",
                "§eStore: §7shop.zurvive.net"+"\n"+
                    "§eWebsite §7www.zurvive.net"+"\n"+
                    "§eDiscord: §7discord.gg/zurvive"
        );

        new LobbyGUI(Core.getPlugin()).insert(e.getUser().getPlayer().getInventory(), e.getUser());

        try{
            Warp warp = Core.getPlugin().getWarpManager().getWarp("spawn");
            warp.teleportTo(e.getUser());
        }catch (NullPointerException ex){}
    }

}
