package usa.jusjus.zurvive.hub.lobby;

import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.Module;
import usa.jusjus.zurvive.hub.lobby.listener.ServerListener;
import usa.jusjus.zurvive.hub.lobby.listener.UserLoadListener;

public class LobbyManager extends Module {

    public LobbyManager(Core plugin){
        super(plugin, "Lobby Manager", false);

        registerListener(
                new UserLoadListener(),
                new ServerListener()
        );
    }

    @Override
    public void reload(String s) {

    }
}
