package usa.jusjus.zurvive.hub.lobby.gui;

import org.bukkit.Material;
import org.bukkit.event.block.Action;
import org.bukkit.inventory.ItemStack;
import usa.devrocoding.synergy.assets.Synergy;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.gui.Gui;
import usa.devrocoding.synergy.spigot.gui.object.GuiInteract;
import usa.devrocoding.synergy.spigot.gui.object.GuiInteractElement;
import usa.devrocoding.synergy.spigot.gui.object.GuiSize;
import usa.devrocoding.synergy.spigot.user.object.SynergyUser;
import usa.devrocoding.synergy.spigot.utilities.ItemBuilder;
import usa.devrocoding.synergy.spigot.utilities.SkullItemBuilder;
import usa.jusjus.zurvive.cosmetics.outfit.gui.OutfitGUI;
import usa.jusjus.zurvive.hub.ZurviveHub;

public class LobbyGUI extends GuiInteract {

    public LobbyGUI(Core plugin){
        super(plugin);
    }

    @Override
    public void setup() {
        addElement(new GuiInteractElement() {
            @Override
            public ItemStack getIcon(SynergyUser synergyUser) {
                return new SkullItemBuilder(synergyUser.getPlayer())
                        .setName("§b§lProfile & Settings")
                        .addLore("§e§lRIGHT CLICK §7to see your profile and more....")
                        .build();
            }

            @Override
            public void click(SynergyUser synergyUser, Action action) {
                if (action == Action.RIGHT_CLICK_BLOCK || action == Action.RIGHT_CLICK_AIR){

                }
            }
        }, 2);

        addElement(new GuiInteractElement() {
            @Override
            public ItemStack getIcon(SynergyUser synergyUser) {
                return new ItemBuilder(Material.COMPASS)
                        .setName("§9§lJoin a game")
                        .addLore("§e§lRIGHT CLICK §7to get started....")
                        .build();
            }

            @Override
            public void click(SynergyUser synergyUser, Action action) {
                if (action == Action.RIGHT_CLICK_BLOCK || action == Action.RIGHT_CLICK_AIR){

                }
            }
        }, 4);
        addElement(new GuiInteractElement() {
            @Override
            public ItemStack getIcon(SynergyUser synergyUser) {
                return new ItemBuilder(Material.ENDER_CHEST)
                        .setName("§c§lCosmetics & Shop")
                        .addLore(
                                "§e§lRIGHT CLICK §7to see the cosmetics",
                                "These cosmetics will also be visable when fighting"
                        )
                        .build();
            }

            @Override
            public void click(SynergyUser synergyUser, Action action) {
                if (action == Action.RIGHT_CLICK_BLOCK || action == Action.RIGHT_CLICK_AIR){
                    new OutfitGUI(getPlugin(), ZurviveHub.getInstance().getCosmeticManager()).open(synergyUser.getPlayer());
                }
            }
        }, 6);
    }
}
