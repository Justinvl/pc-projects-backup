package usa.jusjus.zurvive.hub.lobby.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.*;

public class ServerListener implements Listener {

    @EventHandler
    public void onServerEvent(BlockBreakEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onServerEvent(BlockPlaceEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onServerEvent(PlayerPickupItemEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onServerEvent(PlayerExpChangeEvent e){
        e.setAmount(0);
    }

    @EventHandler
    public void onServerEvent(PlayerDropItemEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onServerEvent(FoodLevelChangeEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onServerEvent(PlayerSwapHandItemsEvent e){
        e.setCancelled(true);
    }

}
