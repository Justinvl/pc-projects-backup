package usa.jusjus.zurvive.hub;

import lombok.Getter;
import usa.devrocoding.synergy.spigot.Core;
import usa.devrocoding.synergy.spigot.api.SynergyPlugin;
import usa.jusjus.zurvive.cosmetics.CosmeticManager;
import usa.jusjus.zurvive.hub.lobby.LobbyManager;
import usa.jusjus.zurvive.hub.lobby.scoreboard.LobbyBoard;

public class ZurviveHub extends SynergyPlugin {

    @Getter
    public static ZurviveHub instance;
    @Getter
    public CosmeticManager cosmeticManager;
    @Getter
    public LobbyManager lobbyManager;

    @Override
    public String name() {
        return "Zurvive Hub";
    }

    @Override
    public void init() {
        instance = this;
        Core synergy = Core.getPlugin();

        this.cosmeticManager = new CosmeticManager(synergy);
        this.lobbyManager = new LobbyManager(synergy);

        Core.getPlugin().getScoreboardManager().setScoreboardPolicy(new LobbyBoard());
    }

    @Override
    public void deinit() {

    }
}
