package usa.jusjus.embot.command;

import lombok.Getter;
import usa.jusjus.embot.DiscordModule;
import usa.jusjus.embot.command.object.DiscordCommand;

import java.util.ArrayList;
import java.util.List;

public class CommandManager extends DiscordModule {

    @Getter
    private final List<DiscordCommand> commands = new ArrayList<>();

    public CommandManager(){
        super("Command Manager");

        registerEventListeners(
                new CommandListener()
        );
    }

    public void addCommand(DiscordCommand discordCommand){
        if (!this.commands.contains(discordCommand)) {
            this.commands.add(discordCommand);
        }
    }

}
