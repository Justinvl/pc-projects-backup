package usa.jusjus.embot.update;

import com.google.api.client.util.DateTime;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import usa.jusjus.embot.Core;
import usa.jusjus.embot.DiscordModule;
import usa.jusjus.embot.assets.spreadsheet.GoogleSpreadsheet;
import usa.jusjus.embot.update.command.CommandUpdate;
import usa.jusjus.embot.utilities.JSONFile;
import usa.jusjus.embot.utilities.LinuxColorCodes;
import usa.jusjus.embot.utilities.MessageBuilder;

import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.List;

public class UpdateManager extends DiscordModule {

    private JSONFile jsonFile;
    private List<Integer> notifyTime = new ArrayList<Integer>(){{
        add(-8);
        add(-5);
        add(-3);
        add(-2);
        add(-1);
        add(0);
    }};

    public UpdateManager(){
        super("Update Manager");

        registerCommands(
                new CommandUpdate()
        );
    }

    public void setupConfiguration(){
        JsonObject main = new JsonObject();

        JsonArray array = new JsonArray();
        JsonObject arrayObject = new JsonObject();
        arrayObject.addProperty("D", "E");
        arrayObject.addProperty("G", "H");
        arrayObject.addProperty("J", "L");

        arrayObject.addProperty("N", "P");
        arrayObject.addProperty("R", "T");
        arrayObject.addProperty("V", "X");
        arrayObject.addProperty("Z", "AB");

        array.add(arrayObject);

        main.add("values", array);

        this.jsonFile = new JSONFile("configuration");
        this.jsonFile.write(
                main
        ).finish();
    }

    public Map<String, String> getRanges(){
        Map<String, String> map = new HashMap<>();

        JsonElement main = this.jsonFile.read("values");
        if (main.isJsonArray()){
            JsonArray jsonArray = main.getAsJsonArray();
            JsonObject element = jsonArray.get(0).getAsJsonObject();
            element.entrySet().iterator().forEachRemaining(stringJsonElementEntry -> {
                String key = stringJsonElementEntry.getKey();
                String value = stringJsonElementEntry.getValue().getAsString();
                map.put(key+"5:"+key, value+"5:"+value);
                System.out.println("Element -> "+key+"5:"+key+" - "+value+"5:"+value);
            });

        }
        return map;
    }

    public void check(){
        System.out.println(LinuxColorCodes.ANSI_YELLOW+"Checking for deadlines...."+LinuxColorCodes.ANSI_RESET);
        final Map<String, String> ranges = getRanges();
        final Map<String, String> users = Core.getUsers();
        final GoogleSpreadsheet spreadsheet = Core.getGoogleSpreadsheet();
        // Username    Project     Deadline
        Map<String, Map<String, List<Date>>> data = new HashMap<>();

        for(String keyRange : ranges.keySet()){
            String projectRange = (keyRange.substring(0, keyRange.length() - 1))+"3";
            String project = (String) spreadsheet.getRange(projectRange).get(0).get(0);

            List<List<Object>> keyRangeList = spreadsheet.getRange(keyRange);
            List<List<Object>> deadlineRangeList = spreadsheet.getRange(ranges.get(keyRange));
            //  User    Deadline
            List<String> cache = new ArrayList<>();
            System.out.println(LinuxColorCodes.ANSI_BLUE+"Retrieving Project '"+project+"'"+LinuxColorCodes.ANSI_RESET);
            if (keyRangeList != null) {
                for (List<Object> object : keyRangeList) {
                    String user = users.get((String) object.get(0));;
                    if (user != null) {
                        cache.add(user);
                    }else{
                        user = (String) object.get(0);
                        cache.add(user);
                    }
                    data.putIfAbsent(user, new HashMap<>());
                }
            }
            if (deadlineRangeList != null){
                List<Date> dates = new ArrayList<>();
                for (List<Object> object : deadlineRangeList) {
                    try{
                        Date deadline = null;
                        if (object.size()>0){
                            deadline = new SimpleDateFormat("MM/dd/yyyy").parse((String)object.get(0));

                            Days days = Days.daysBetween(new LocalDate(deadline), new LocalDate());
                            System.out.println(deadline + " - "+days.getDays());
                            if (!this.notifyTime.contains(days.getDays())){
                                deadline = null;
                            }
                        }
                        dates.add(deadline);
                    }catch (ParseException e){
                        System.out.println(LinuxColorCodes.ANSI_RED+e.getMessage()+LinuxColorCodes.ANSI_RESET);
                    }
                }
                for(int i = 0;i<dates.size();i++){
                    String user = cache.get(i);
                    List<Date> dateList = data.get(user).getOrDefault(project, new ArrayList<>());
                    dateList.add(dates.get(i));

                    Map<String, List<Date>> origin = data.getOrDefault(user, new HashMap<>());
                    origin.put(project, dateList);
                    data.put(user, origin);
                }
            }
        }
        System.out.println(LinuxColorCodes.ANSI_YELLOW+"Sending Notifications...."+LinuxColorCodes.ANSI_RESET);
        notifyUsers(data);
        System.out.println(LinuxColorCodes.ANSI_YELLOW+"Notified Players Success..."+LinuxColorCodes.ANSI_RESET);
    }

    private void notifyUsers(Map<String, Map<String, List<Date>>> data){
        List<Guild> guilds = Core.getJda().getGuilds();
        if (guilds.size() > 0){
            Guild guild = guilds.get(1);

            for(String user : data.keySet()){
                try{
                    Member member = guild.getMemberByTag(user);
//                    System.out.println(user);
//                    System.out.println(member);
                    if (member != null) {
                        Map<String, List<Date>> info = data.get(user);
//                        System.out.println(info);
                        StringBuilder builder = new StringBuilder(
                                "Check the spreadsheet sheets to\n" +
                                "see your upcoming deadlines and status. "+member.getAsMention()+"\n\n"
                        );

                        boolean shouldSend = false;

                        for(String project : info.keySet()){
                            List<Date> dates = info.get(project);
                            if (dates.size() > 0) {
                                int i = 0;

                                for (Date date : dates) {
                                    if (date != null) {
                                        if (i==0) {
                                            builder.append("__***" + project + "***__\n");
                                        }
                                        builder.append(new SimpleDateFormat("EEEE MMMM dd yyyy").format(date) + "\n");
                                        if (!shouldSend){
                                            shouldSend = true;
                                        }
                                        i++;
                                    }
                                }
                                builder.append("\n");
                            }
                        }

                        if (shouldSend) {
                            guild.getTextChannelById("644634589020815381").sendMessage(
                                    new MessageBuilder("Upcoming deadlines for "+member.getUser().getName(), builder.toString())
                                            .overwriteColor(Color.RED)
                                            .setThumbnail(member.getUser().getAvatarUrl())
                                            .setFooter("Bot made by JusJus | © EnchantedMob, Inc. 2019", "https://www.google.com/")
                                            .build()
                            ).queue();
                        }
                    }
                }catch (Exception e){
                    System.out.println(LinuxColorCodes.ANSI_RED+e.getMessage()+" Discord Profile is empty for '"+user+"' or not online!"+LinuxColorCodes.ANSI_RESET);
                }
            }

        }
    }

}
