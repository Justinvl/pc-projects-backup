package usa.jusjus.embot;

import lombok.Getter;
import usa.jusjus.embot.command.object.DiscordCommand;

public abstract class DiscordModule {

    @Getter
    private String name;
    private Object[] listeners;

    public DiscordModule(String name){
        this.name = name;
    }

    public void registerEventListeners(Object... listeners){
        Core.jda.addEventListener(listeners);
    }

    public void registerCommands(DiscordCommand... discordCommands){
        for (DiscordCommand command : discordCommands){
            Core.getCommandManager().addCommand(command);
        }
    }

}
