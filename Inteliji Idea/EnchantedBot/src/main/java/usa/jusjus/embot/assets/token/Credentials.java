package usa.jusjus.embot.assets.token;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.SheetsScopes;

import java.io.*;
import java.util.Collections;
import java.util.List;

public class Credentials {

    private static final List<String> scopes = Collections.singletonList(SheetsScopes.SPREADSHEETS_READONLY);
    private static final String path = "/credentials.json";

    public static Credential get(final NetHttpTransport httpTransport){
        final JacksonFactory jacksonFactory = JacksonFactory.getDefaultInstance();
        try{
            InputStream inputStream = Credentials.class.getResourceAsStream(path);
            if (inputStream != null){
                GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(jacksonFactory, new InputStreamReader(inputStream));
                GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                        httpTransport, jacksonFactory, clientSecrets, scopes
                ).setDataStoreFactory(new FileDataStoreFactory(new File("tokens"))).setAccessType("offline").build();
                LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
                return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

}
