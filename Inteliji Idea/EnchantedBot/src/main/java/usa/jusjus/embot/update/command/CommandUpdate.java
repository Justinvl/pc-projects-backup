package usa.jusjus.embot.update.command;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import usa.jusjus.embot.Core;
import usa.jusjus.embot.command.object.DiscordCommand;
import usa.jusjus.embot.utilities.MessageBuilder;

import java.awt.*;

public class CommandUpdate extends DiscordCommand {

    public CommandUpdate(){
        super("", "!", "updateusers");
    }

    @Override
    public void execute(Member member, MessageChannel channel, Message message) {
        Core.users.clear();
        Core.users.putAll(Core.getGoogleSpreadsheet().getUsers());

        channel.sendMessage(
                new MessageBuilder("Successful updates", "The internal user database has been successful updated.")
                        .overwriteColor(Color.GREEN)
                        .build()
        ).queue();
    }
}
