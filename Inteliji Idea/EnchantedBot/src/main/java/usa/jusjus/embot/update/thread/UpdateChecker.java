package usa.jusjus.embot.update.thread;

import usa.jusjus.embot.Core;

import java.util.concurrent.TimeUnit;

public class UpdateChecker extends Thread{

    @Override
    public void run() {
        Core.getUpdateManager().check();
        try{
            Thread.sleep(TimeUnit.MINUTES.toMillis(1));
            run();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
