package usa.jusjus.embot;

import lombok.Getter;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import usa.jusjus.embot.assets.spreadsheet.GoogleSpreadsheet;
import usa.jusjus.embot.command.CommandManager;
import usa.jusjus.embot.update.UpdateManager;
import usa.jusjus.embot.update.thread.UpdateChecker;

import javax.security.auth.login.LoginException;
import java.util.HashMap;
import java.util.Map;

public class Core {

    @Getter // Yeah I know, I'm just lazy to make another class for the user object and stuff
    public static Map<String, String> users = new HashMap<>();

    @Getter
    public static JDA jda;
    @Getter
    private static CommandManager commandManager;
    @Getter
    private static GoogleSpreadsheet googleSpreadsheet;
    @Getter
    private static UpdateManager updateManager;

    public static void main(String[] args) {
        try{
            jda = new JDABuilder(AccountType.BOT)
                    .setToken("NjQ0MTY5MjQwNDIwODEwNzYy.XcwHxg.ieMHAmX8izwl2GZ-SjLKuWXv6XA")
                    .setAutoReconnect(true)
                    .setEnableShutdownHook(true)
                    .build();

            jda.getPresence().setActivity(Activity.watching("over everyone"));
        }catch (LoginException e){
            e.printStackTrace();
        }

        commandManager = new CommandManager();
        googleSpreadsheet = new GoogleSpreadsheet();
        updateManager = new UpdateManager();

        // Connect with the spreadsheet
        googleSpreadsheet.authorize();

        // Update the users with their discord names
        users.putAll(googleSpreadsheet.getUsers());

        // Initiate and create the configuration file
        updateManager.setupConfiguration();

//        updateManager.check();

        // Start the update sequence
        new UpdateChecker().start();
    }

}
