package usa.jusjus.embot.assets.spreadsheet;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.sun.javafx.collections.MappingChange;
import usa.jusjus.embot.assets.token.Credentials;
import usa.jusjus.embot.utilities.LinuxColorCodes;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GoogleSpreadsheet {

    private String sheetID = "1CWyOvMtuLn7aYFV5isAAGlo0Supv4YUuE7kSTxUM7cw";
//    private String sheetID = "1ITMYotSHoSTkW1fhnxc7REaGTLwQgPGuddN_nxz54OQ";
    private Sheets sheets;

    public void authorize(){
        try{
            NetHttpTransport netHttpTransport = GoogleNetHttpTransport.newTrustedTransport();
            this.sheets = new Sheets.Builder(
                    netHttpTransport,
                    JacksonFactory.getDefaultInstance(),
                    Credentials.get(netHttpTransport)
            ).setApplicationName("EnchantedBot Access").build();
        }catch (IOException| GeneralSecurityException e){
            System.out.println(LinuxColorCodes.ANSI_RED+e.getMessage()+LinuxColorCodes.ANSI_RESET);
        }
    }

    public List<List<Object>> getRange(String range){
        try{
            ValueRange response = this.sheets.spreadsheets().values().get(this.sheetID, range).execute();
            return response.getValues();
        }catch (IOException e){
            System.out.println(LinuxColorCodes.ANSI_RED+e.getMessage()+LinuxColorCodes.ANSI_RESET);
        }
        return new ArrayList<>();
    }

    public void idk(){
//        try{
//            System.out.println(getRange("Schedule"));
//        }catch (Exception e){
//            e.printStackTrace();
//        }
    }

    public Map<String, String> getUsers(){
        final String users = "Discord Profiles!A5:B";
        Map<String, String> map = new HashMap<>();

        for(List row : getRange(users)){
            map.put(String.valueOf(row.get(0)), String.valueOf(row.get(1)));
        }
        return map;
    }

    private boolean hasAccess(){
        return sheets != null;
    }

}
