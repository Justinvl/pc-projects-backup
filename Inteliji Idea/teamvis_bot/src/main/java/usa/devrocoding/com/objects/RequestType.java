package usa.devrocoding.com.objects;

import lombok.Getter;

import java.awt.*;

public enum RequestType {

    DESIGN("Design", Color.YELLOW),
    BUILD("Build", Color.CYAN),
    PRIVATE("Private", Color.BLACK),
    MECHANIC("Mechanic",Color.LIGHT_GRAY);

    @Getter
    private String name;
    @Getter
    private Color color;

    RequestType(String name, Color color){
        this.name = name;
        this.color = color;
    }

}
