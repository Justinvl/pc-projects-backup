package usa.devrocoding.com.utilities;

import lombok.Getter;

public enum ChatFilter {

    SWEAR(

            "kanker","kkr","kut","fuck","fck","cancer","cncer","ass","asshole","bitch","motherfucker",
            "nigger","nigahh","nigge","shit","shitt","bitch","b*tch","cunt","nigga","b1tch","fuc|<","fuc|<ing","hooker rat",
            "neuker","пизда","pussy"
    ),
    LOW_SWEAR(
            "ffs", "f*ck"
    );

    @Getter
    private String[] words;

    ChatFilter(String... words){
        this.words = words;
    }

    public boolean hasBadWords(String message){
        for(String bw : this.words){
            if (message.contains(bw)){
                return true;
            }
        }
        return false;
    }

    public String removeBadWords(String message){
        String msg = message;
        for(String bw : this.words){
            msg.replace(bw, "****");
        }
        return msg;
    }

}
