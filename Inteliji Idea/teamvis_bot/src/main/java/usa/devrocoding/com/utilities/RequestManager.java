package usa.devrocoding.com.utilities;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.requests.Route;
import net.dv8tion.jda.core.utils.MiscUtil;
import usa.devrocoding.com.Core;
import usa.devrocoding.com.objects.RequestType;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RequestManager {

    public static void sendOverviewRequest(RequestType type, User sender, String project, TextChannel channel) {
        Core.getJDA().getTextChannelById(Core.getChannel_overview()).sendMessage(
                new MessageBuilder("New Request", "New request from "+sender.getAsMention())
                        .addField(
                                "Additional Information",
                                "You can find Additional Information from the user in the channel "+
                                        channel.getAsMention(),
                                true
                        )
                        .addField("Request Type", "`"+type.getName()+"` for project `"+project+"`", true)
                        .addField("Mark it as done when complete!", "Click on the "+Core.getJDA().getEmoteById(Core.getEmote()).getAsMention()+
                                " to mark it as done and notify the user that it is completed.", true)
                        .setAuthor(sender)
//                        .setFooter(
//                                sender.getName(),
//                                sender.getAvatarUrl())
                        .setThumbnail(sender.getAvatarUrl())
                        .overwriteColor(type.getColor())
                        .build()
        ).queue(message -> Core.getJDA().getTextChannelById(Core.getChannel_overview()).addReactionById(
                message.getId(),
                Core.getJDA().getEmoteById(Core.getEmote())
        ).queue());

//        TrelloManager trelloManager = new TrelloManager();
//        trelloManager.printLabelNames();
//        trelloManager.createCard(project, type, sender.getName(), channel.getName());

    }

    public static void createTextChannel(MessageChannel receiveChannel, RequestType type, Member creator, String project){
        List<Permission> a = new ArrayList<>();
        a.add(Permission.MESSAGE_READ);
        a.add(Permission.MESSAGE_WRITE);
        a.add(Permission.MESSAGE_HISTORY);

        String channel_name = type.getName()+"_"+project+"_REQUEST";

        if (Core.getJDA().getTextChannelsByName(channel_name, true).size() > 0){
            receiveChannel.sendMessage(
                    new MessageBuilder("Cannot preform your request!", "Channel already exists with your requested map!")
                            .overwriteColor(Color.RED)
                            .build()
            ).queue();
            return;
        }

        switch (type){
            case MECHANIC:
                Role cb_role = Core.getJDA().getGuildById(Core.getGuild_id()).getRoleById(Core.getEngineer_id());

                Core.getJDA().getCategoryById(Core.getCategory_channels()).createTextChannel(channel_name)
                        .addPermissionOverride(creator, a, null)
                        .addPermissionOverride(cb_role, a, null)
                        .queue(channel -> next(receiveChannel, channel, type, creator, project));
                break;
            case DESIGN:
                Role design_role = Core.getJDA().getGuildById(Core.getGuild_id()).getRoleById(Core.getDesigner_id());

                Core.getJDA().getCategoryById(Core.getCategory_channels()).createTextChannel(channel_name)
                        .addPermissionOverride(creator, a, null)
                        .addPermissionOverride(design_role, a, null)
                        .queue(channel -> next(receiveChannel, channel, type, creator, project));
                break;
            default:
                Core.getJDA().getCategoryById(Core.getCategory_channels()).createTextChannel(channel_name)
                        .addPermissionOverride(creator, a, null)
                        .queue(channel -> next(receiveChannel, channel, type, creator, project));
                break;
        }

    }

    public static void next(MessageChannel receiveChannel, Channel channel, RequestType type, Member creator, String project){
        if (channel.getType() == ChannelType.TEXT) {

            TextChannel textChannel = (TextChannel) channel;

            receiveChannel.sendMessage(
                    new MessageBuilder("Created your request",
                            "We have created your channel "+
                                    textChannel.getAsMention()+
                                    ". You can use that channel for your needs.")
//                            .addField("ALSO!!!", "We added your request to trello, so it will be handles much faster!", false)
                            .build()
            ).queue();

            switch (type){
                case MECHANIC:
                    Role cb_role = Core.getJDA().getGuildById(Core.getGuild_id()).getRoleById(Core.getEngineer_id());

                    textChannel.sendMessage(creator.getAsMention()).queue();
                    textChannel.sendMessage(cb_role.getAsMention()).queue();
                    textChannel.sendMessage(
                            new MessageBuilder("Welcome to your private channel!",
                                    "The people with the role " +
                                            Core.getJDA().getRoleById(Core.getOperator_one()).getAsMention() +
                                            " and "+Core.getJDA().getRoleById(Core.getOperator_two()).getAsMention()+" have received a message and they will work the best they can to complete it. " +
                                            "You will get a message when your request is completed."
                            )
                                    .addField("Give us some extra information!", "If you have any extra information, " +
                                            "please post it in here. We can always ask for additional information in here.", false)
                                    .addField("How do I know when it is completed?",
                                            "As long as this channel exists, your request is still open and is being worked on.", false)
                                    .addField("I accidentally send the wrong request!", "No worries brother, just "+
                                            "ask if they can remove this request.", false)
                                    .build()
                    ).queue();
                    break;
                case DESIGN:
                    Role design_role = Core.getJDA().getGuildById(Core.getGuild_id()).getRoleById(Core.getDesigner_id());

                    textChannel.sendMessage(creator.getAsMention()).queue();
                    textChannel.sendMessage(design_role.getAsMention()).queue();
                    textChannel.sendMessage(
                            new MessageBuilder("Welcome to your private channel!",
                                    "The people with the role " +
                                            design_role.getAsMention() +
                                            " have received a message and they will work the best they can to complete it. " +
                                            "You will get a message when your request is completed."
                            )
                                    .addField("Give us some extra information!", "If you have any extra information, " +
                                            "please post it in here. We can always ask for additional information in here.", false)
                                    .addField("How do I know when it is completed?",
                                            "As long as this channel exists, your request is still open and is being worked on.", false)
                                    .addField("I accidentally send the wrong request!", "No worries brother, just "+
                                            "ask if they can remove this request.", false)
                                    .build()
                    ).queue();
                    break;
                case PRIVATE:
                    textChannel.sendMessage(creator.getAsMention()).queue();
                    textChannel.sendMessage(
                            new MessageBuilder(
                                "Welcome to your private channel!",
                                "Commands to use: `-add <user>` `-remove <user>` `-rename <name>...` `-archive` `-delete`. " +
                                        "Only YOU as the creator of this channel is allow to use these commands."
                            )
                            .build()
                    ).queue();
                    break;
                default:
                    textChannel.sendMessage(creator.getAsMention()).queue();
                    textChannel.sendMessage(
                            new MessageBuilder("Welcome to your private channel!",
                                    "The people with the role " +
                                            Core.getJDA().getRoleById(Core.getOperator_one()).getAsMention() +
                                            " and "+Core.getJDA().getRoleById(Core.getOperator_two()).getAsMention()+" have received a message and they will work the best they can to complete it. " +
                                            "You will get a message when your request is completed."
                            )
                                    .addField("Give us some extra information!", "If you have any extra information, " +
                                            "please post it in here. We can always ask for additional information in here.", false)
                                    .addField("How do I know when it is completed?",
                                            "As long as this channel exists, your request is still open and is being worked on.", false)
                                    .addField("I accidentally send the wrong request!", "No worries brother, just "+
                                            "ask if they can remove this request.", false)
                                    .build()
                    ).queue();
                    RequestManager.sendOverviewRequest(type, creator.getUser(), project, textChannel);
                    break;
            }
        }
    }

    private static void outPrintHistory(Channel channel_to_delete, Channel channel, java.util.List<Message> messageList){
        String author = "";
        for(Message message : messageList){
            if (message.getContentRaw().isEmpty() || message.getContentRaw() == null || message.getContentRaw() == "")
                continue;

//            if (!message.getAuthor().getName().equals(author)) {
            Core.getJDA().getTextChannelById(channel.getId()).sendMessage("__**Message from: `" + message.getAuthor().getName() + "`**__").queue();
//            }
            Core.getJDA().getTextChannelById(channel.getId()).sendMessage("```"+message.getContentRaw()+"```").queue();
            author = message.getAuthor().getName();
        }
    }

    public static void archive(Channel channel){
        TextChannel textChannel = (TextChannel) channel;

        List<Permission> a = new ArrayList<>();
        a.add(Permission.MESSAGE_READ);
        a.add(Permission.MESSAGE_WRITE);
        a.add(Permission.MESSAGE_HISTORY);

        channel.createCopy()
                .setParent(Core.getJDA().getCategoryById(Core.getCategory_history()))
                .addPermissionOverride(channel.getMemberPermissionOverrides().get(0).getMember(), null, a)

                .setName(channel.getName().replace("request", "cached"))
                .queue(
                        channel1 -> textChannel.getHistoryBefore(textChannel.getLatestMessageId(), 100)
                                .queue(messageHistory -> outPrintHistory(channel, channel1,
                                        messageHistory.getRetrievedHistory()
                                        )
                                )
                );

        channel.delete().queueAfter(500, TimeUnit.MILLISECONDS);

        String[] project_details = channel.getName().split("_");
    }

    public static void finish(MessageChannel channel){
        for (Channel cc : Core.getJDA().getGuildById(Core.getGuild_id()).getChannels()) {
            if (!cc.getName().contains("_REQUEST")) {
                continue;
            }

//            Core.getJDA().getTextChannelById(Core.getChannel_history()).sendMessage(
//                    new MessageBuilder("Completed Request",
//                            "INSERT MESSAGE HISTORY")
//                            .overwriteColor(Color.RED)
//                            .build()
//            ).queue();
            //TODO: Copy channel to history
            cc.delete().queue();
            return;
        }
        channel.sendMessage(
                new MessageBuilder("Cannot preform your request!",
                        "Action not allowed in this channel!")
                        .overwriteColor(Color.RED)
                        .build()
        ).queue();
    }

}
