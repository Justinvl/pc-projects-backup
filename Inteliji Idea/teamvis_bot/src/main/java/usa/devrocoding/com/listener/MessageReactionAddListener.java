package usa.devrocoding.com.listener;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import usa.devrocoding.com.Core;
import usa.devrocoding.com.utilities.MessageBuilder;

import java.awt.*;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MessageReactionAddListener extends ListenerAdapter {

    /*
        Bedrock World Generation | IGN/TeamName | (Project name) {size of needed world} [Biome]
        - -newrequest {requesttype}
     */

//    public void onMessageReactionAdd(MessageReactionAddEvent e) {
//
//        if (!e.getMember().getUser().isBot()) {
//            if (e.getChannel().getId().equals(Core.getChannel_overview())) {
//                if (e.getReactionEmote().getId().equals(Core.getEmote())) {
//                    if (e.getChannel().getMessageById(e.getMessageId()).complete().getReactions().size() < 2) {
//
//                        OffsetDateTime t = e.getReactionEmote().getCreationTime();
//                        String format = t.getDayOfMonth() + "-" + t.getMonthValue() + "-" + t.getYear() +
//                                " " + t.getHour() + ":" + t.getMinute();
//
//                        MessageEmbed ms = e.getChannel().getMessageById(e.getMessageId()).complete().getEmbeds().get(0);
//                        User mem = Core.getJDA().getUsersByName(ms.getAuthor().getName(), true).get(0);
//
//                        for (MessageEmbed.Field field : ms.getFields()) {
//                            if (field.getName().contains("Additional Information")) {
//                                String[] splitted = field.getValue().split("#");
//
//                                if (!(splitted.length > 0)) {
//                                    break;
//                                }
//
//                                String channel_id = splitted[1].replace(">", "");
//
//                                for (Channel channel : Core.getJDA().getGuildById(Core.getGuild_id()).getChannels()) {
//                                    if (channel.getId().equals(channel_id)) {
//                                        TextChannel textChannel = (TextChannel) channel;
//
//                                        List<Permission> a = new ArrayList<>();
//                                        a.add(Permission.MESSAGE_READ);
//                                        a.add(Permission.MESSAGE_WRITE);
//                                        a.add(Permission.MESSAGE_HISTORY);
//
//                                        channel.createCopy()
//                                                .setParent(Core.getJDA().getCategoryById(Core.getCategory_history()))
//                                                .addPermissionOverride(channel.getMemberPermissionOverrides().get(0).getMember(), null, a)
//
//                                                .setName(channel.getName().replace("request", "cached"))
//                                                .queue(
//                                                        channel1 -> textChannel.getHistoryBefore(textChannel.getLatestMessageId(), 100)
//                                                                .queue(messageHistory -> outPrintHistory(channel, channel1,
//                                                                        messageHistory.getRetrievedHistory()
//                                                                )
//                                                )
//                                        );
//
//                                        channel.delete().queueAfter(500, TimeUnit.MILLISECONDS);
//
//                                        String[] project_details = channel.getName().split("_");
//
//                                        Core.getJDA().getTextChannelById(Core.getChannel_member_updates()).sendMessage(mem == null ? "User: " + ms.getAuthor().getName() : mem.getAsMention()).queue(
//                                                message -> Core.getJDA().getTextChannelById(Core.getChannel_member_updates()).sendMessage(
//                                                        new MessageBuilder("Your request has been finished", "Today you request has been finished by " +
//                                                                e.getMember().getUser().getAsMention())
//                                                                .addField(
//                                                                        "Your request has been completed!",
//                                                                        "`"+project_details[0]+"` request for your project `"+project_details[1]+"`",
//                                                                        false)
//                                                                .build()
//                                                ).queue()
//                                        );
//                                        break;
//                                    }
//                                }
//                            }
//                        }
//
//                        e.getChannel().getMessageById(e.getMessageId()).complete().editMessage(
//                                new MessageBuilder("Finished Request", "Finished by " + e.getMember().getUser().getAsMention())
//                                        .addField(
//                                                "Additional Information",
//                                                "Finished on " + format, false)
//                                        .addField("Request has been finished", "Their channel is deleted and moved to the history.", false)
//                                        .setFooter("Finished Request", "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Checkmark_green.svg/1180px-Checkmark_green.svg.png")
//                                        .overwriteColor(Color.RED)
//                                        .build()
//                        ).queue();
//                    }
//                }
//            }
//        }
//    }

}