package usa.devrocoding.com.utilities;

import net.dv8tion.jda.core.entities.MessageChannel;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import usa.devrocoding.com.Core;
import usa.devrocoding.com.objects.StatusCode;

import java.awt.*;

public class VerifyManager {

    public String verifyUser(String name, MessageChannel channel){
        try {
            HttpClient httpclient = HttpClients.createDefault();
            HttpPost httppost = new HttpPost("https://teamvisionary.net/marketplace/members/verify-discord/");

            String json = "{ \"discord_username\" : \"" + name + "\", \"secret_key\" : \"zKsVaJalKjmUlEcfuAOKRWpd2BiYIerC\" }";
            StringEntity entity = new StringEntity(json, HTTP.UTF_8);

            httppost.setEntity(entity);

            HttpResponse response = httpclient.execute(httppost);
            System.out.println(response.getStatusLine() + " - "+name);

            for(StatusCode status : StatusCode.values()){
                if (response.getStatusLine().getStatusCode() == 200){
                    channel.sendMessage(
                            new MessageBuilder("Verified your account", status.getNice_name())
                                    .overwriteColor(Color.GREEN)
                                    .build()
                    ).queue();
                    break;
                }else if (response.getStatusLine().getStatusCode() == status.getCode()){
                    if (status.getCode() == 500){
                        channel.sendMessage(Core.getJDA().getUserById("266547090765774850").getAsMention()).queue();
                    }
                    channel.sendMessage(
                            new MessageBuilder("Cannot verify your account!", status.getNice_name())
                                    .addField("StatusCode: " + status.getCode(), status.toString().toUpperCase(), true)
                                    .overwriteColor(Color.RED)
                                    .build()
                    ).queue();
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public void two(){

    }

}
