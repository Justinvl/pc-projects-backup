package usa.devrocoding.com.listener;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.dv8tion.jda.core.managers.ChannelManager;
import usa.devrocoding.com.Core;
import usa.devrocoding.com.objects.RequestType;
import usa.devrocoding.com.utilities.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatListener extends ListenerAdapter {

    /*
        Bedrock World Generation | IGN/TeamName | (Project name) {size of needed world} [Biome]
        - -newrequest {requesttype}
     */

    private List<Member> muted = new ArrayList<>();
    private Map<Member, Integer> warnings = new HashMap<>();

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getAuthor().isBot()) return;

        Message message = event.getMessage();
        String content = message.getContentRaw();
        MessageChannel channel = event.getChannel();
        Member member = event.getMember();

//        System.out.print(message);
//        System.out.print("-----");
//        System.out.print(content.length());
//        System.out.print("-----");
//        System.out.print(channel);

        if (event.getChannelType() != ChannelType.PRIVATE) {

//            if (muted.contains(member)){
//                message.delete().queue();
//                return;
//            }
//
//            //Chat filter
//            if (ChatFilter.SWEAR.hasBadWords(content)){
//
//                if (warnings.containsKey(member)){
//                    warnings.put(member, warnings.get(member)+1);
//                }else{
//                    warnings.put(member, 1);
//                }
//
//                if (warnings.get(member) >= 3){
//                    muted.add(member);
//                    new java.util.Timer().schedule(
//                            new java.util.TimerTask() {
//                                @Override
//                                public void run() {
//                                    muted.remove(member);
//                                    channel.sendMessage(member.getUser().getAsMention()).queue();
//                                    channel.sendMessage(
//                                            new MessageBuilder("You are able to talk again!",
//                                                    "Watch out this time "+member.getUser().getName()+".......")
//                                                    .overwriteColor(Color.RED)
//                                                    .build()
//                                    ).queue();
//                                }
//                            },
//                            10000 * 60 * 20
//                    );
//                }
//
//                message.delete().queue();
//                channel.sendMessage(
//                        new MessageBuilder("HOLLAA.. Watch your language "+member.getUser().getName()+"!",
//                                "This message was blocked because a bad word was found. " +
//                                        "If you believe this word should not be blocked, please message us.")
//                                .addField("You were givin 1 warning!", "You currently have `"+warnings.get(member)+"` warning(s)! (Upon reaching 3 you will get muted)", true)
//                                .overwriteColor(Color.RED)
//                                .build()
//                ).queue();
//            }

            if (content.contains("-new")) {
                if (!channel.getId().equals("525062411485446144"))
                    return;
                String[] mc = content.split(" ");
                if (mc.length > 2){
                    try{
                        RequestType type = RequestType.valueOf(mc[1].toUpperCase());
                        String project = mc[2];
                        RequestManager.createTextChannel(channel, type, member, project);
                    }catch (Exception e){
                        e.printStackTrace();
                        error(channel);
                    }
                }else {
                    error(channel);
                }
            }else if (content.contains("-help")) {
                channel.sendMessage(
                        new MessageBuilder("So you are stuck?",
                                "Here you have a list of commands you can use.")
                                .addField("Make a new request", "-new {requestType} {project}", false)
                                .addField("List of available request types", getNiceTypes(), false)
                                .setThumbnail("https://atmanco.com/wp-content/uploads/2015/12/questions-to-ask.png")
                                .overwriteColor(Color.RED)
                                .build()
                ).queue();
            }else if (content.toLowerCase().contains("-verify")){
                //TODO: Verify account

                if (member.getRoles().size() > 0){
                    VerifyManager verifyManager = new VerifyManager();
                    verifyManager.verifyUser(member.getUser().getName()+"#"+member.getUser().getDiscriminator(), channel);
                }else{
                    channel.sendMessage(
                            new MessageBuilder("Cannot verify your account!", "According to your role. You are not a visionary member.")
                                    .overwriteColor(Color.RED)
                                    .build()
                    ).queue();
                }
            }else if (content.toLowerCase().contains("-delete")){
                if (isRequestChannel(channel)) {
                    try{
                        for(Channel channel1 : Core.getJDA().getGuildById(Core.getGuild_id()).getChannels()){
//                            System.out.println(channel.getId() + " - "+channel1.getId());
                            if (channel1.getId().equals(channel.getId())){
                                channel1.delete().queue();
                                break;
                            }
                        }
                    }catch (Exception e){
                        channel.sendMessage(
                                new MessageBuilder("Cannot preform your request!", e.getMessage())
                                        .overwriteColor(Color.RED)
                                        .build()
                        ).queue();
                    }
                }
            }else if (content.toLowerCase().contains("-archive")){
                if (isRequestChannel(channel)) {
                    try{
                        for(Channel channel1 : Core.getJDA().getGuildById(Core.getGuild_id()).getChannels()){
//                            System.out.println(channel.getId() + " - "+channel1.getId());
                            if (channel1.getId().equals(channel.getId())){
                                RequestManager.archive(channel1);
                                break;
                            }
                        }
                    }catch (Exception e){
                        channel.sendMessage(
                                new MessageBuilder("Cannot preform your request!", e.getMessage())
                                        .overwriteColor(Color.RED)
                                        .build()
                        ).queue();
                    }
                }
            }else if (content.toLowerCase().contains("-add")){
                if (isRequestChannel(channel)){
                    String[] args = content.split(" ");
                    if (args.length > 1 && args.length < 3){
                        try{
                            String id = args[1].replaceAll("<@", "").replaceAll(">", "").replaceAll("!", "");
                            Member t = Core.getJDA().getGuildById(Core.getGuild_id()).getMemberById(id);
                            List<Permission> a = new ArrayList<>();
                            a.add(Permission.MESSAGE_READ);
                            a.add(Permission.MESSAGE_WRITE);
                            a.add(Permission.MESSAGE_HISTORY);

                            Core.getJDA().getGuildById(Core.getGuild_id()).getTextChannelById(channel.getId()).getManager().putPermissionOverride(t, a, null).queue();
                            channel.sendMessage(
                                    new MessageBuilder("Successfull added "+t.getEffectiveName(), null)
                                            .overwriteColor(Color.GREEN)
                                            .build()
                            ).queue();
                        }catch (Exception e){
                            channel.sendMessage(
                                new MessageBuilder("Cannot preform your request!", e.getMessage())
                                        .overwriteColor(Color.RED)
                                        .build()
                            ).queue();
                        }
                    }
                }
            }else if (content.toLowerCase().contains("-remove")){
                if (isRequestChannel(channel)){
                    String[] args = content.split(" ");
                    if (args.length > 1 && args.length < 3){
                        try{
                            String id = args[1].replaceAll("<@", "").replaceAll(">", "").replaceAll("!", "");
                            Member t = Core.getJDA().getGuildById(Core.getGuild_id()).getMemberById(id);
                            List<Permission> a = new ArrayList<>();
                            a.add(Permission.MESSAGE_READ);
                            a.add(Permission.MESSAGE_WRITE);
                            a.add(Permission.MESSAGE_HISTORY);

                            Core.getJDA().getGuildById(Core.getGuild_id()).getTextChannelById(channel.getId()).getManager().putPermissionOverride(t, null, a).queue();
                            channel.sendMessage(
                                    new MessageBuilder("Successfull removed "+t.getEffectiveName(), null)
                                            .overwriteColor(Color.GREEN)
                                            .build()
                            ).queue();
                        }catch (Exception e){
                            channel.sendMessage(
                                    new MessageBuilder("Cannot preform your request!", e.getMessage())
                                            .overwriteColor(Color.RED)
                                            .build()
                            ).queue();
                        }
                    }
                }
            }else if (content.toLowerCase().contains("-rename")){
                if (isRequestChannel(channel)) {
                    String[] args = content.split(" ");
                    if (args.length > 1) {
                        try{
                            StringBuilder builder = new StringBuilder();
                            for(int i=1;i<args.length;i++){
                                if (i!=1){
                                    builder.append("-");
                                }
                                builder.append(args[i]);
                            }
                            builder.append("-request");
                            Core.getJDA().getGuildById(Core.getGuild_id()).getTextChannelById(channel.getId()).getManager().setName(builder.toString()).queue();
                            channel.sendMessage(
                                    new MessageBuilder("Successfull renamed the channel!", "New name: \""+builder.toString().toLowerCase()+"\"")
                                            .overwriteColor(Color.GREEN)
                                            .build()
                            ).queue();
                        }catch (Exception e){
                            channel.sendMessage(
                                    new MessageBuilder("Cannot preform your request!", e.getMessage())
                                            .overwriteColor(Color.RED)
                                            .build()
                            ).queue();
                        }
                    }
                }
            }else if (content.toLowerCase().contains("-trello")){
//                if (member.hasPermission(Permission.ADMINISTRATOR)) {
//                    String[] mc = channel.getName().split("_");
//                    if (mc.length > 1) {
//                        try {
////                            RequestType type = RequestType.valueOf(mc[0].toUpperCase().replace("-", "_"));
////                            String project = mc[1];
////                            String user = content.split(" ")[1];
////                            TrelloManager trelloManager = new TrelloManager();
////                            trelloManager.printLabelNames();
////                            trelloManager.createCard(project, type, user, channel.getName());
//
//                        } catch (Exception e) {
////                            e.printStackTrace();
////                            error(channel);
//                        }
//                    } else {
////                        error(channel);
//                    }
//                }
            }
        }
    }

    private boolean isRequestChannel(MessageChannel channel){
        return channel.getName().contains("request");
    }

    private String getNiceTypes(){
        StringBuilder builder = new StringBuilder();
        int i = 0;
        for (RequestType type : RequestType.values()) {
            if (i > 0) {
                builder.append(", ");
            }
            builder.append("`" + type.toString() + "`");
            i++;
        }
        return builder.toString();
    }

    private void error(MessageChannel channel){
        channel.sendMessage(
                new MessageBuilder("Cannot preform your request!", "Please specify a request type from this list: " + getNiceTypes())
                        .addField("How to type your command?", "-newrequest {requestType} {project}", true)
                        .overwriteColor(Color.RED)
                        .build()
        ).queue();
    }


}
