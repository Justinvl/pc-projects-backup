package usa.devrocoding.com.utilities;

import net.dv8tion.jda.core.entities.MessageChannel;

public class UtilMessage {

    public static void error(MessageChannel channel, String title, String description){
        channel.sendMessage(
                new MessageBuilder(title, description).build()
        ).queue();
    }

}
