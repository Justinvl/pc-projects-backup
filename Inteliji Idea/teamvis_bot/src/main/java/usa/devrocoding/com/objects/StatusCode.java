package usa.devrocoding.com.objects;

import lombok.Getter;

public enum StatusCode {

    SUCCESS(
            200, "Successfully verified your discord account!"
    ),
    ILLEGAL_ACTION(
            401, "Illegal Action!"
    ),
    USER_NOT_FOUND(
            404, "You didn't fill in your discord name on the website, or you have mistyped your discord name. Fill it in like this: \"User#0000\""
    ),
    ALREADY_VERIFIED(
            206, "You are already verified. You don't have to do anything anymore."
    ),
    INTERNAL_SERVER_ERROR(
            500, "Internal Server Error. Pinged the devs for you."
    );;

    @Getter
    private int code;
    @Getter
    private String nice_name;

    StatusCode(int code, String nice_name){
        this.code = code;
        this.nice_name = nice_name;
    }

}
