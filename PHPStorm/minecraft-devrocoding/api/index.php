<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

$auth = array("jusjus112" => "112jusjus");

if (isset($_GET)){
    if (isset($_GET['username'])&&isset($_GET['password'])){
        $username = $_GET['username'];
        $password = $_GET['password'];

        foreach ($auth as $key => $value) {
            if ($username == $key && $password == $value){
                http_response_code(200);
                echo json_encode(array("response"=>"200","message" => "Account information matches!"));
                return;
            }
        }
        http_response_code(404);
        echo json_encode(array("response"=>"404","error" => "Account doesn't exists!"));
        return;
    }
}
http_response_code(400);
echo json_encode(array("response"=>"400","error" => "No content specified. Check your request content!"));
return;