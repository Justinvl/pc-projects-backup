<!DOCTYPE html>
<html>
<head>
    <?php include_once 'imports.php'; ?>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="assets/images/logo-no-txt-1000x1000.png" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Want a custom minecraft plugin? Made by Devro Coding? We offer BungeeCord, PaperSpigot, Discord Bots or Website's. Feel free to look on our website's or maybe do you wanne join our team?">
    <title>Devro Coding | Minecraft</title>

    <meta property="og:title" content="Devro Coding | Minecraft" />
    <meta property="og:description" content="Want a custom minecraft plugin? Made by Devro Coding? We offer BungeeCord, PaperSpigot, Discord Bots or Website's. Feel free to look on our website's or maybe do you wanne join our team?" />
    <meta property="og:image" content="https://www.minecraft.devrocoding.com/assets/images/Logo-devro-Coding-REAL-400x400.png" />
    <meta property="og:url" content="https://www.minecraft.devrocoding.com"/>

</head>
<body>
<?php include_once 'navbar.php'; ?>
<section class="cid-qBeTVXtSsB mbr-fullscreen mbr-parallax-background" id="header2-f" data-rv-view="569">

    <div class="mbr-overlay" style="opacity: 0; background-color: rgb(247, 15, 15);"></div>

    <div class="container align-center">
        <div class="row justify-content-md-center">
            <div class="mbr-white col-md-10">
                <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1" style="text-transform: uppercase">
                    Always wanted to have your own plugin?
                </h1>

                <p class="mbr-text pb-3 mbr-fonts-style display-5">Welcome to Devro Coding where we not only focus on quality but also on satisfaction and conviviality.</p>
                <a class="mbr-section-btn"><a class="btn btn-md btn-secondary display-4" href="#pricing-tables2-j">Services</a>
<!--                    <a class="mbr-section-btn"><a class="btn btn-md btn-secondary display-4" href="./team">Expertise</a>-->
            </div>
        </div>
    </div>
    <div class="mbr-arrow hidden-sm-down" aria-hidden="true">
        <a href="#next">
            <i class="mbri-down mbr-iconfont"></i>
        </a>
    </div>
</section>

<section class="features7 cid-qBlcchqTel" id="features7-t" data-rv-view="572">
    
    <div class="container">
        <div class="row justify-content-center">

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="media">
                    <div class="card-img pr-2">
                        <span class="mbr-iconfont mbri-protect" media-simple="true"></span>
                    </div>
                    <div class="card-box media-body">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">1 Month free Updates</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                           We will fix your plugin and update the plugin for 1 Free Month... Yes.. 1 Free Month.</p>
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="media"> 
                    <div class="card-img pr-2">
                        <span class="mbr-iconfont mbri-rocket" media-simple="true"></span>
                    </div>
                    <div class="card-box media-body">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">
                            No Lagg Plugins</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                           All our plugins are like rockets. Super fast and running with zero lagg!</p>
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-4">
                <div class="media">
                    <div class="card-img pr-2">
                        <span class="mbr-iconfont mbri-magic-stick" media-simple="true"></span>
                    </div>
                    <div class="media-body card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">
                            From idea to reality</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                            We will make your amazing idea reality. It is a plugin made for your own project.
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="clients cid-qL9ybnCd4g" id="clients-2c" data-rv-view="575">

        <div class="container mb-5">
            <div class="media-container-row">
                <div class="col-12 align-center">
                    <h2 class="mbr-section-title pb-3 mbr-fonts-style display-2">
                        Our Clients
                    </h2>
                    
                </div>
            </div>
        </div>

    <div class="container">
        <div class="carousel slide" data-ride="carousel" role="listbox">
            <div class="carousel-inner" data-visible="5">

            <div class="carousel-item ">
                    <div class="media-container-row">
                        <div class="col-md-12">
                            <div class="wrap-img ">
                                <img src="assets/images/download-225x225.jpg" class="img-responsive clients-img" alt="" title="" media-simple="true">
                            </div>
                        </div>
                    </div>
                </div><div class="carousel-item ">
                    <div class="media-container-row">
                        <div class="col-md-12">
                            <div class="wrap-img ">
                                <a href="http://enchantedmc.net" target="_blank"><img src="assets/images/image-792x676.png" class="img-responsive clients-img" alt="" title="" media-simple="true"></a>
                            </div>
                        </div>
                    </div>
                </div><div class="carousel-item ">
                    <div class="media-container-row">
                        <div class="col-md-12">
                            <div class="wrap-img ">
                                <a href="https://shapescape.co/" target="_blank"><img src="assets/images/ovv4cpmi-400x400-400x400.jpg" class="img-responsive clients-img" alt="" title="" media-simple="true"></a>
                            </div>
                        </div>
                    </div>
                </div><div class="carousel-item ">
                    <div class="media-container-row">
                        <div class="col-md-12">
                            <div class="wrap-img ">
                                <a href="http://aliacraft.net" target="_blank"><img src="assets/images/logo-background-300x300.png" class="img-responsive clients-img" alt="" title="" media-simple="true"></a>
                            </div>
                        </div>
                    </div>
                </div><div class="carousel-item ">
                    <div class="media-container-row">
                        <div class="col-md-12">
                            <div class="wrap-img ">
                                <a href="http://teamvisionary.net" target="_blank"><img src="assets/images/favicon256-256x256.png" class="img-responsive clients-img" alt="" title="" media-simple="true"></a>
                            </div>
                        </div>
                    </div>
                </div></div>
            <div class="carousel-controls">
                <a data-app-prevent-settings="" class="carousel-control carousel-control-prev" role="button" data-slide="prev">
                    <span aria-hidden="true" class="mbri-left mbr-iconfont"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a data-app-prevent-settings="" class="carousel-control carousel-control-next" role="button" data-slide="next">
                    <span aria-hidden="true" class="mbri-right mbr-iconfont"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="cid-qBfaKZKsXC" id="pricing-tables2-j" data-rv-view="589">

    <div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(255, 255, 255);">
    </div>

    <div class="container">
        <div class="media-container-row">

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>SMALL PLUGIN</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">Starting from&nbsp;</span>
                        <span class="price-figure mbr-fonts-style display-1">$25</span>
                        <small class="price-term mbr-fonts-style display-7"></small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">Good for plugins like &nbsp;Double Jump's or Server Selector.. Just an small plugin.</li><li class="list-group-item"><strong>24/7 Support</strong><br></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center favorite col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>
                            MEDIUM PLUGIN</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">Starting from </span>
                        <span class="price-figure mbr-fonts-style display-1">$50</span>
                        <small class="price-term mbr-fonts-style display-7"></small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">If you want more then just a small plugin. Advanced Server Selector or Server Selector with Database Support?</li><li class="list-group-item"><strong>24/7 Support</strong></li><li class="list-group-item"><br></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>
                            BIG PLUGIN</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">Starting from&nbsp;</span>
                        <span class="price-figure mbr-fonts-style display-1">$120</span>
                        <small class="price-term mbr-fonts-style display-7"></small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">The bigger the better they say. If you have a big idea or want to have your own Pet System?</li>
                            <li class="list-group-item"><strong>24/7 Support</strong><br></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>MINIGAME OR BACKEND</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">Price in consultation</span>
                        <!--                        <span class="price-figure mbr-fonts-style display-1"></span>-->
                        <small class="price-term mbr-fonts-style display-7"></small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">If you need a cool minigame or a huge backend system for your server!</li>
                            <li class="list-group-item"><strong>24/7 Support</strong><br></li>
                        </ul>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>

<section class="mbr-section info1 cid-qFFUJWy8Pr" id="info1-19" data-rv-view="592">

    <div class="container">
        <div class="row justify-content-center content-row">
            <div class="media-container-column title col-12 col-lg-7 col-md-6">
                <h3 class="mbr-section-subtitle align-left mbr-light pb-3 mbr-fonts-style display-5">Do you want more than just a plugin?<br>Maybe a Discord Bot or Something else?</h3>
                <h2 class="align-left mbr-bold mbr-fonts-style display-2">Check our products</h2>
            </div>
            <div class="media-container-column col-12 col-lg-3 col-md-4">
                <div class="mbr-section-btn align-right py-4"><a class="btn btn-primary display-4" href="./pricing">SEE ALL OUR PRODUCTS</a></div>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section content5 cid-qBlbpeVFO6 mbr-parallax-background" id="content5-m" data-rv-view="595">

    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                <h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
                    The Magic 4 Skills</h2>
            </div>
        </div>
    </div>
</section>

<section class="features9 cid-qFFSgkrePd" id="features9-17" data-rv-view="598">

    <div class="container">
        <div class="row justify-content-center">
            <div class="card p-3 col-12 col-md-6">
                <div class="media-container-row">
                    <div class="card-img pr-2">
                        <span class="mbr-iconfont mbri-magic-stick" media-simple="true"></span>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">Allround Experience</h4>
                        <p class="mbr-text mbr-fonts-style display-7">Our expertise is jus growing and growing. We will expand your skills and experience every day.</p>
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6">
                <div class="media-container-row">
                    <div class="card-img pr-2">
                        <span class="mbr-iconfont mbri-cash" style="font-size: 73px;" media-simple="true"></span>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7"><div>Low Pricing High Quality</div></h4>
                        <p class="mbr-text mbr-fonts-style display-7">Low Pricing for High Quality means we only give you high quailty, the price isn't go higher for good quality. It's already included ;)</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="features9 cid-qFFSgMBTGI" id="features9-18" data-rv-view="601">

    <div class="container">
        <div class="row justify-content-center">
            <div class="card p-3 col-12 col-md-6">
                <div class="media-container-row">
                    <div class="card-img pr-2">
                        <span class="mbr-iconfont mbri-help" media-simple="true"></span>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">24/7 Customer Support</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                           We offer professional Support that u can acces 24/7. Small or big problem we will help every customer and solve the problem.</p>
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6">
                <div class="media-container-row">
                    <div class="card-img pr-2">
                        <span class="mbr-iconfont mbri-rocket" media-simple="true"></span>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7">
                            As fast as a Rocket.... Woosshhh</h4>
                        <p class="mbr-text mbr-fonts-style display-7">
                           All our plugins are like rockets. Super fast and running with zero lagg! That's because our custom algorithm and our crazy performance setup.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="carousel slide testimonials-slider cid-qFHzjQBlOB" id="testimonials-slider1-1x" data-rv-view="604">

    <div class="container text-center">
        <h2 class="pb-5 mbr-fonts-style display-2">
            WHAT OUR FANTASTIC USERS SAY
        </h2>

        <div class="carousel slide" data-ride="carousel" role="listbox">
            <div class="carousel-inner">

            <div class="carousel-item">
                    <div class="user col-md-8">
                        <div class="user_image">
                            <img src="assets/images/favicon256-256x256.png" alt="" title="" media-simple="true">
                        </div>
                        <div class="user_text pb-3">
                            <p class="mbr-fonts-style display-7">
                                Justin is a reliable dev, with a can do attitude.  He has worked with me on many of my projects, and is easy to communicate complex topics to.  He is a real team player.</p>
                        </div>
                        <div class="user_name mbr-bold pb-2 mbr-fonts-style display-7">
                            Gizmo&nbsp;</div>
                        
                    </div>
                </div></div>

            <div class="carousel-controls">
                <a class="carousel-control-prev" role="button" data-slide="prev">
                  <span aria-hidden="true" class="mbri-arrow-prev mbr-iconfont"></span>
                  <span class="sr-only">Previous</span>
                </a>
                
                <a class="carousel-control-next" role="button" data-slide="next">
                  <span aria-hidden="true" class="mbri-arrow-next mbr-iconfont"></span>
                  <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section article content10 cid-qBlcA0HnjF" id="content10-w" data-rv-view="612">

    <div class="container">
        <div class="inner-container" style="width: 66%;">
            <hr class="line" style="width: 25%;">
            <div class="section-text align-center mbr-white mbr-fonts-style display-1"><strong>
                    QUESTIONS?</strong></div>
            <hr class="line" style="width: 25%;">
        </div>
    </div>
</section>

<section class="mbr-section form1 cid-qBlbyC2Y9W" id="form1-n" data-rv-view="614">

    <div class="container">
        <div class="row justify-content-center">
            <div class="title col-12 col-lg-8">
                
                <h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5">Feel free to contact us when u want!<br></h3>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="media-container-column col-lg-8" data-form-type="formoid">
                    <div data-form-alert="" hidden="">Thanks for filling out the form! Your message has arrived safely and is being read immediately.</div>
            
<!--                    <form class="mbr-form" action="mailto:justin@devrocoding.com" method="post"><input type="hidden" data-form-email="true" value="O5Iw0X193t6/6ZJ1WWpYU6zex/NRqj3/ROvQKZ2/y7RpgZGVvhPU2FjYGYEjP192TMFMBQIYYySfbIlSxAgpQXA3zD/hx7MWtAvyxW0RKhMdKZzqNXpgDgYWe/qbd5es">-->
<!--                        <div class="row row-sm-offset">-->
<!--                            <div class="col-md-4 multi-horizontal" data-for="name">-->
<!--                                <div class="form-group">-->
<!--                                    <label class="form-control-label mbr-fonts-style display-7" for="name-form1-n">Name</label>-->
<!--                                    <input type="text" class="form-control" name="name" data-form-field="Name" required="" placeholder="Name" id="name-form1-n">-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-md-4 multi-horizontal" data-for="email">-->
<!--                                <div class="form-group">-->
<!--                                    <label class="form-control-label mbr-fonts-style display-7" for="email-form1-n">Email</label>-->
<!--                                    <input type="email" class="form-control" name="email" data-form-field="Email" required="" placeholder="example@outlook.com" id="email-form1-n">-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-md-4 multi-horizontal" data-for="phone">-->
<!--                                <div class="form-group">-->
<!--                                    <label class="form-control-label mbr-fonts-style display-7" for="phone-form1-n">Phone</label>-->
<!--                                    <input type="tel" class="form-control" name="phone" data-form-field="Phone" placeholder="+31612345678" id="phone-form1-n">-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="form-group" data-for="message">-->
<!--                            <label class="form-control-label mbr-fonts-style display-7" for="message-form1-n">Message</label>-->
<!--                            <textarea type="text" class="form-control" name="message" rows="7" data-form-field="Message" placeholder="Dear Devro Coding...." id="message-form1-n"></textarea>-->
<!--                        </div>-->
<!--            -->
<!--                        <span class="input-group-btn"><button href="" type="submit" class="btn btn-primary btn-form display-4">SEND FORM</button></span>-->
<!--                    </form>-->
                <div class="row justify-content-center">
                    <div class="title col-xs-6">
                        <span class="input-group-btn"><a href="mailto:justin@devrocoding.com" type="submit" class="btn btn-primary btn-form display-4">EMAIL US</a></span>
                    </div>
                    <div class="title col-xs-6">
                        <span class="input-group-btn"><a href="https://discord.gg/7DAhv5b" target="_blank" type="submit" class="btn btn-primary btn-form display-4">OR JOIN OUR DISCORD</a></span>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<?php include_once 'footer.php'; ?>

  <script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/smooth-scroll.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/viewport-checker/jquery.viewportchecker.js"></script>
  <script src="assets/jarallax/jarallax.min.js"></script>
  <script src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="assets/touch-swipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/formoid/formoid.min.js"></script>

 <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbri-down mbr-iconfont"></i></a></div>
    <input name="animation" type="hidden">
  </body>
</html>