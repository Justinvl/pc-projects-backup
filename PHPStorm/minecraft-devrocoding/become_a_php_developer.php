<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/logo-no-txt-1000x1000.png" type="image/x-icon">
    <meta name="description" content="Become part of the team! Apply here!">
    <title>Become a Developer</title>

    <meta property="og:title" content="Devro Coding | Website Developer" />
    <meta property="og:description" content="Do you wanne be part of our team? Just fill in the application on this page and become a member :D" />
    <meta property="og:image" content="https://www.minecraft.devrocoding.com/assets/images/Logo-devro-Coding-REAL-400x400.png" />
    <meta property="og:url" content="https://www.minecraft.devrocoding.com/become_a_php_developer"/>

    <?php include_once 'imports.php'; ?>

</head>
<body>
<?php include_once 'navbar.php'; ?>

<section class="header9 cid-qL9wajb4FL mbr-parallax-background" id="header9-28" data-rv-view="666">

    <div class="mbr-overlay" style="opacity: 0.3; background-color: rgb(228, 210, 149);">
    </div>

    <div class="container">
        <div class="media-container-column mbr-white col-md-8">
            <h1 class="mbr-section-title align-left mbr-bold pb-3 mbr-fonts-style display-1">BECOME A PHP / Website DEVELOPER</h1>

            <p class="mbr-text align-left pb-3 mbr-fonts-style display-5">
                On this page you can become a developer for us, do you want to join our team? Feel free to apply and maybe till later??
            </p>

        </div>
    </div>

    <div class="mbr-arrow hidden-sm-down" aria-hidden="true">
        <a href="#next">
            <i class="mbri-down mbr-iconfont"></i>
        </a>
    </div>
</section>


<iframe id="JotFormIFrame-80982070300347" onload="window.parent.scrollTo(0,0)" allowtransparency="true" allowfullscreen="true" allow="geolocation; microphone; camera"
        src="https://form.jotformeu.com/80982070300347" frameborder="0" style="width: 1px; min-width: 100%; height:539px; border:none;" scrolling="no" >
</iframe>

<script type="text/javascript">

    var ifr = document.getElementById("JotFormIFrame-80982070300347");
    if(window.location.href && window.location.href.indexOf("?") > -1) {
        var get = window.location.href.substr(window.location.href.indexOf("?") + 1);
        if(ifr && get.length > 0) {
            var src = ifr.src;
            src = src.indexOf("?") > -1 ? src + "&" + get : src + "?" + get;
            ifr.src = src;
        }
    }
    window.handleIFrameMessage = function(e) {
        var args = e.data.split(":");
        if (args.length > 2) {
            iframe = document.getElementById("JotFormIFrame-" + args[(args.length - 1)]);
        } else {
            iframe = document.getElementById("JotFormIFrame");
        } if (!iframe) {
            return;
        } switch (args[0]) {
            case "scrollIntoView": iframe.scrollIntoView();
                break;
            case "setHeight": iframe.style.height = args[1] + "px";
                break;
            case "collapseErrorPage":
                if (iframe.clientHeight > window.innerHeight) {
                    iframe.style.height = window.innerHeight + "px";
                }
                break;
            case "reloadPage": window.location.reload();
            break; case "loadScript": var src = args[1]; if (args.length > 3) { src = args[1] + ':' + args[2]; } var script = document.createElement('script'); script.src = src; script.type = 'text/javascript'; document.body.appendChild(script); break; case "exitFullscreen": if (window.document.exitFullscreen) window.document.exitFullscreen(); else if (window.document.mozCancelFullScreen) window.document.mozCancelFullScreen(); else if (window.document.mozCancelFullscreen) window.document.mozCancelFullScreen(); else if (window.document.webkitExitFullscreen) window.document.webkitExitFullscreen(); else if (window.document.msExitFullscreen) window.document.msExitFullscreen(); break; } var isJotForm = (e.origin.indexOf("jotform") > -1) ? true : false; if(isJotForm && "contentWindow" in iframe && "postMessage" in iframe.contentWindow) { var urls = {"docurl":encodeURIComponent(document.URL),"referrer":encodeURIComponent(document.referrer)}; iframe.contentWindow.postMessage(JSON.stringify({"type":"urls","value":urls}), "*"); } }; if (window.addEventListener) { window.addEventListener("message", handleIFrameMessage, false); } else if (window.attachEvent) { window.attachEvent("onmessage", handleIFrameMessage); } </script>

<section class="cid-qL9utJeNmH" id="social-buttons3-25" data-rv-view="674">

    <div class="container">
        <div class="media-container-row">
            <div class="col-md-8 align-center">
                <h2 class="pb-3 mbr-section-title mbr-fonts-style display-2">
                    SHARE THIS PAGE!
                </h2>
                <div>
                    <div class="mbr-social-likes">
                        <span class="btn btn-social socicon-bg-facebook facebook mx-2" title="Share link on Facebook">
                            <i class="socicon socicon-facebook"></i>
                        </span>
                        <span class="btn btn-social twitter socicon-bg-twitter mx-2" title="Share link on Twitter">
                            <i class="socicon socicon-twitter"></i>
                        </span>
                        <span class="btn btn-social plusone socicon-bg-googleplus mx-2" title="Share link on Google+">
                            <i class="socicon socicon-googleplus"></i>
                        </span>
                        <span class="btn btn-social vkontakte socicon-bg-vkontakte mx-2" title="Share link on VKontakte">
                            <i class="socicon socicon-vkontakte"></i>
                        </span>
                        <span class="btn btn-social odnoklassniki socicon-bg-odnoklassniki mx-2" title="Share link on Odnoklassniki">
                            <i class="socicon socicon-odnoklassniki"></i>
                        </span>
                        <span class="btn btn-social pinterest socicon-bg-pinterest mx-2" title="Share link on Pinterest">
                            <i class="socicon socicon-pinterest"></i>
                        </span>
                        <span class="btn btn-social mailru socicon-bg-mail mx-2" title="Share link on Mailru">
                            <i class="socicon socicon-mail"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include_once 'footer.php'; ?>

<script src="assets/web/assets/jquery/jquery.min.js"></script>
<script src="assets/popper/popper.min.js"></script>
<script src="assets/tether/tether.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/smooth-scroll/smooth-scroll.js"></script>
<script src="assets/jarallax/jarallax.min.js"></script>
<script src="assets/dropdown/js/script.min.js"></script>
<script src="assets/touch-swipe/jquery.touch-swipe.min.js"></script>
<script src="assets/social-likes/social-likes.js"></script>
<script src="assets/viewport-checker/jquery.viewportchecker.js"></script>
<script src="assets/theme/js/script.js"></script>
<script src="assets/formoid/formoid.min.js"></script>


<div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbri-down mbr-iconfont"></i></a></div>
<input name="animation" type="hidden">
</body>
</html>