<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 2-3-2018
 * Time: 20:02
 */
?>

<section class="cid-qFHz4OpJ3B" id="footer1-1v" data-rv-view="617">

    <div class="container">
        <div class="media-container-row content text-white">
            <div class="col-12 col-md-3">
                <div class="media-wrap">
                    <a href="#">
                        <img src="assets/images/logo-no-txt-1000x1000.png" title="" media-simple="true">
                        <br>
                        <img src="assets/images/Logo_MC.png" style="max-height: 40px" title="" media-simple="true">
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3"><strong>
                        Information</strong></h5>
                <p class="mbr-text">
<!--                    <strong>KVK </strong>-&nbsp;70082456<br>-->
<!--                    <strong>BTW </strong>-&nbsp; NL230967061B01<br><br>-->
<!--                    <strong>Account Number</strong>: (<em>Dutch</em>)<br>NL93 RABO 0325 1285 10<br>-->
                    We are a company that creates plugins for the game Minecraft. We started in 2011 and we are researching new programming languages every day.
                </p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3"><strong>
                        Contact</strong></h5>
                <p class="mbr-text">
                    Email: justin@devrocoding.com<br>Discord: @JusJus#0112</p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3"><strong>
                        Links
                    </strong></h5>
                <p class="mbr-text">
                    <a href="https://www.minecraft.devrocoding.com">Minecraft Plugins&nbsp;Pricing</a>
                    <br>
                    <a href="https://www.school.devrocoding.com" target="_blank">School Projects</a>
                    <br>
                    <a href="./become_a_developer" target="_blank">Become a developer</a>
                    <br>
                </p>
            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row mbr-white">
                <div class="col-sm-6 copyright">
                    <p class="mbr-text mbr-fonts-style display-7" style="visibility: visible !important;">
                        © Copyright 2019 Devro Coding</p>
                </div>
                <div class="col-md-6">
                    <div class="social-list align-right">
                        <div class="soc-item">
                            <a href="https://www.linkedin.com/company/devro-coding" target="_blank" style="visibility: visible !important;">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-linkedin socicon" media-simple="true"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://twitter.com/devrocoding" target="_blank" style="visibility: visible !important;">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-twitter socicon" media-simple="true"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://www.facebook.com/DevroCoding/" target="_blank" style="visibility: visible !important;">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-facebook socicon" media-simple="true"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://stackexchange.com/users/12401410/devro-coding" target="_blank" style="visibility: visible !important;">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-stackoverflow socicon" media-simple="true"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://www.linkedin.com/company/devro-coding" target="_blank" style="visibility: visible !important;">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-linkedin socicon" media-simple="true"></span>
                            </a>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
