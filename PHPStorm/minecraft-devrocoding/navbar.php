<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 2-3-2018
 * Time: 20:01
 */
?>

<section class="menu cid-qBeTVW41AL" once="menu" id="menu1-e" data-rv-view="567">


    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="https://www.minecraft.devrocoding.com">
                         <img src="assets/images/logo-long-3000x1000.png" title="" media-simple="true" style="height: 4.5rem;">
                    </a>
                </span>
<!--                <span class="navbar-caption-wrap"><a class="navbar-caption text-secondary display-5" href="https://mobirise.com"></a></span>-->
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true"><li class="nav-item dropdown">
                    <a class="nav-link link text-white display-4" aria-expanded="false">
                    </a>
                </li>
                <li class="nav-item"><a class="nav-link link text-white display-4" href="./pricing"><span class="mbri-shopping-cart mbr-iconfont mbr-iconfont-btn"></span>
                        Products</a></li><li class="nav-item">
<!--                    <a class="nav-link link text-white display-4" href="./team"><span class="mbri-users mbr-iconfont mbr-iconfont-btn"></span>Our Team</a>-->
                </li></ul>

        </div>
    </nav>
</section>
