<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="assets/images/logo-no-txt-1000x1000.png" type="image/x-icon">
  <meta name="description" content="Devro Coding Team Members! Join our team? Check here!">
    <title>Devro Coding MC | Team</title>

    <meta property="og:title" content="Devro Coding | Minecraft" />
    <meta property="og:description" content="Want a custom minecraft plugin? We offer BungeeCord, PaperSpigot, Discord Bots or Website's" />
    <meta property="og:image" content="https://www.minecraft.devrocoding.com/assets/images/Logo-devro-Coding-REAL-400x400.png" />
    <meta property="og:url" content="https://www.minecraft.devrocoding.com/team"/>

    <?php include_once 'imports.php'; ?>
  
</head>
<body>

<?php include_once 'navbar.php'; ?>

<section class="header9 cid-qL9CnMdijh mbr-parallax-background" id="header9-2d" data-rv-view="682">

    <div class="mbr-overlay" style="opacity: 0.3; background-color: rgb(20, 157, 204);">
    </div>

    <div class="container">
        <div class="media-container-column mbr-white col-md-8">
            <h1 class="mbr-section-title align-left mbr-bold pb-3 mbr-fonts-style display-1">OUR TEAM</h1>
            
            <p class="mbr-text align-left pb-3 mbr-fonts-style display-5">
                On this page you can see al of our hard workers! Everyone who is involved in our team!
            </p>
            
        </div>
    </div>

    <div class="mbr-arrow hidden-sm-down" aria-hidden="true">
        <a href="#next">
            <i class="mbri-down mbr-iconfont"></i>
        </a>
    </div>
</section>

<section class="features16 cid-qLagFXJmlr" id="features16-3b" data-rv-view="755">



    <div class="container align-center">
        <div class="row media-row">

            <div class="team-item col-lg-3 col-md-6">
                <div class="item-image">
                    <img src="assets/images/14316808-1281730845184809-8257726387418963301-n-960x956.jpg" media-simple="true">
                </div>
                <div class="item-caption py-3">
                    <div class="item-name px-2">
                        <p class="mbr-fonts-style display-5">
                            Justin (JusJus)
                        </p>
                    </div>
                    <div class="item-role px-2">
                        <p>CEO - Founder</p>
                    </div>
<!--                    <div class="item-social pt-2">-->
<!--                        <a href="https://twitter.com/mobirise" target="_blank">-->
<!--                            <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                            <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                            <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                            <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                        </a>-->
<!--                    </div>-->
                </div>
            </div><div class="team-item col-lg-3 col-md-6">
                <div class="item-image">
                    <img src="assets/images/laurenslogo-556x556.png" media-simple="true">
                </div>
                <div class="item-caption py-3">
                    <div class="item-name px-2">
                        <p class="mbr-fonts-style display-5">
                            Jonas (Jopep)
                        </p>
                    </div>
                    <div class="item-role px-2">
                        <p>PHP | Java Developer</p>
                    </div>
<!--                    <div class="item-social pt-2">-->
<!--                        <a href="https://twitter.com/mobirise" target="_blank">-->
<!--                            <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                            <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                            <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                            <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                        </a>-->
<!--                    </div>-->
                </div>
            </div><div class="team-item col-lg-3 col-md-6">
                <div class="item-image">
                    <img src="assets/images/user-2517433-960-720-720x720-720x720.png" media-simple="true">
                </div>
                <div class="item-caption py-3">
                    <div class="item-name px-2">
                        <p class="mbr-fonts-style display-5">
                            Stephan (Mist69)
                        </p>
                    </div>
                    <div class="item-role px-2">
                        <p>Java Developer</p>
                    </div>
<!--                    <div class="item-social pt-2">-->
<!--                        <a href="https://twitter.com/mobirise" target="_blank">-->
<!--                            <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                            <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                            <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                            <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                        </a>-->
<!--                    </div>-->
                </div>
            </div><div class="team-item col-lg-3 col-md-6">
                <div class="item-image">
                    <img src="assets/images/user-2517433-960-720-720x720-720x720.png" media-simple="true">
                </div>
                <div class="item-caption py-3">
                    <div class="item-name px-2">
                        <p class="mbr-fonts-style display-5">
                            Andy (Andy)
                        </p>
                    </div>
                    <div class="item-role px-2">
                        <p>Java Developer</p>
                    </div>
<!--                    <div class="item-social pt-2">-->
<!--                        <a href="https://twitter.com/mobirise" target="_blank">-->
<!--                            <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                            <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                            <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                            <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
<!--                        </a>-->
<!--                    </div>-->
                </div>
            </div><div class="team-item col-lg-3 col-md-6">
                <div class="item-image">
                    <img src="assets/images/user-2517433-960-720-720x720-720x720.png" media-simple="true">
                </div>
                <div class="item-caption py-3">
                    <div class="item-name px-2">
                        <p class="mbr-fonts-style display-5">
                            Gavin (Mr DrkAngel)
                        </p>
                    </div>
                    <div class="item-role px-2">
                        <p>Java Developer</p>
                    </div>
                    <!--                    <div class="item-social pt-2">-->
                    <!--                        <a href="https://twitter.com/mobirise" target="_blank">-->
                    <!--                            <span class="p-1 socicon-twitter socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
                    <!--                            <span class="p-1 socicon-facebook socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
                    <!--                            <span class="p-1 socicon-googleplus socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
                    <!--                            <span class="p-1 socicon-linkedin socicon mbr-iconfont mbr-iconfont-social" media-simple="true"></span>-->
                    <!--                        </a>-->
                    <!--                    </div>-->
                </div>
            </div></div>
    </div>
</section>

<section class="mbr-section info1 cid-qL9Tye4Yw3" id="info1-2f" data-rv-view="704">

    

    
    <div class="container">
        <div class="row justify-content-center content-row">
            <div class="media-container-column title col-12 col-lg-7 col-md-6">
                <h3 class="mbr-section-subtitle align-left mbr-light pb-3 mbr-fonts-style display-5">
                    Interested in joining our team?</h3>
                <h2 class="align-left mbr-bold mbr-fonts-style display-2">Click the button</h2>
            </div>
            <div class="media-container-column col-12 col-lg-3 col-md-4">
                <div class="mbr-section-btn align-right py-4"><a class="btn btn-danger-outline display-4" href="./become_a_developer">Join Our Team</a></div>
            </div>
        </div>
    </div>
</section>

<section class="cid-qL9y1Qtdbt" id="social-buttons3-2b" data-rv-view="707">
    
    

    

    <div class="container">
        <div class="media-container-row">
            <div class="col-md-8 align-center">
                <h2 class="pb-3 mbr-section-title mbr-fonts-style display-2">
                    SHARE THIS PAGE!
                </h2>
                <div>
                    <div class="mbr-social-likes">
                        <span class="btn btn-social socicon-bg-facebook facebook mx-2" title="Share link on Facebook">
                            <i class="socicon socicon-facebook"></i>
                        </span>
                        <span class="btn btn-social twitter socicon-bg-twitter mx-2" title="Share link on Twitter">
                            <i class="socicon socicon-twitter"></i>
                        </span>
                        <span class="btn btn-social plusone socicon-bg-googleplus mx-2" title="Share link on Google+">
                            <i class="socicon socicon-googleplus"></i>
                        </span>
                        <span class="btn btn-social vkontakte socicon-bg-vkontakte mx-2" title="Share link on VKontakte">
                            <i class="socicon socicon-vkontakte"></i>
                        </span>
                        <span class="btn btn-social odnoklassniki socicon-bg-odnoklassniki mx-2" title="Share link on Odnoklassniki">
                            <i class="socicon socicon-odnoklassniki"></i>
                        </span>
                        <span class="btn btn-social pinterest socicon-bg-pinterest mx-2" title="Share link on Pinterest">
                            <i class="socicon socicon-pinterest"></i>
                        </span>
                        <span class="btn btn-social mailru socicon-bg-mail mx-2" title="Share link on Mailru">
                            <i class="socicon socicon-mail"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include_once 'footer.php'; ?>

  <script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/smooth-scroll.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/touch-swipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/viewport-checker/jquery.viewportchecker.js"></script>
  <script src="assets/masonry/masonry.pkgd.min.js"></script>
  <script src="assets/imagesloaded/imagesloaded.pkgd.min.js"></script>
  <script src="assets/bootstrap-carousel-swipe/bootstrap-carousel-swipe.js"></script>
  <script src="assets/social-likes/social-likes.js"></script>
  <script src="assets/jquery-mb-vimeo_player/jquery.mb.vimeo_player.js"></script>
  <script src="assets/jarallax/jarallax.min.js"></script>
  <script src="assets/theme/js/script.js"></script>
  <script src="assets/mobirise-slider-video/script.js"></script>
  <script src="assets/mobirise-gallery/player.min.js"></script>
  <script src="assets/mobirise-gallery/script.js"></script>

 <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbri-down mbr-iconfont"></i></a></div>
    <input name="animation" type="hidden">
  </body>
</html>