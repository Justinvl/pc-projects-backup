<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="assets/images/logo-no-txt-1000x1000.png" type="image/x-icon">
  <meta name="description" content="Want a custom plugin? Check here for our pricing!">
  <title>Devro Coding MC | Products</title>

    <meta property="og:title" content="Devro Coding | Minecraft" />
    <meta property="og:description" content="Want a custom minecraft plugin? We offer BungeeCord, PaperSpigot, Discord Bots or Website's" />
    <meta property="og:image" content="https://www.minecraft.devrocoding.com/assets/images/Logo-devro-Coding-REAL-400x400.png" />
    <meta property="og:url" content="https://www.minecraft.devrocoding.com/pricing"/>
  
  <?php include_once 'imports.php'; ?>

</head>
<body>

<?php include_once 'navbar.php'; ?>

<section class="header9 cid-qFGfb4sRFQ mbr-parallax-background" id="header9-1e" data-rv-view="622">

    <div class="mbr-overlay" style="opacity: 0.3; background-color: rgb(20, 157, 204);">
    </div>

    <div class="container">
        <div class="media-container-column mbr-white col-md-8">
            <h1 class="mbr-section-title align-left mbr-bold pb-3 mbr-fonts-style display-1">DEVRO CODING PRICING</h1>
            
            <p class="mbr-text align-left pb-3 mbr-fonts-style display-5">
                On this page you can find all of our products together with the prices. All the prices are in overleg. Feel free to take a look.
            </p>
            
        </div>
    </div>

    <div class="mbr-arrow hidden-sm-down" aria-hidden="true">
        <a href="#next">
            <i class="mbri-down mbr-iconfont"></i>
        </a>
    </div>
</section>

<section class="cid-qBfaKZKsXC" id="pricing-tables2-j" data-rv-view="589">

    <div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(255, 255, 255);">
    </div>

    <div class="container">
        <div class="media-container-row">

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>SMALL PLUGIN</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">Starting from&nbsp;</span>
                        <span class="price-figure mbr-fonts-style display-1">$25</span>
                        <small class="price-term mbr-fonts-style display-7"></small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">Good for plugins like &nbsp;Double Jump's or Server Selector.. Just an small plugin.</li><li class="list-group-item"><strong>24/7 Support</strong><br></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center favorite col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>
                            MEDIUM PLUGIN</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">Starting from </span>
                        <span class="price-figure mbr-fonts-style display-1">$50</span>
                        <small class="price-term mbr-fonts-style display-7"></small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">If you want more then just a small plugin. Advanced Server Selector or Server Selector with Database Support?</li><li class="list-group-item"><strong>24/7 Support</strong></li><li class="list-group-item"><br></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>
                            BIG PLUGIN</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">Starting from&nbsp;</span>
                        <span class="price-figure mbr-fonts-style display-1">$120</span>
                        <small class="price-term mbr-fonts-style display-7"></small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">The bigger the better they say. If you have a big idea or want to have your own Pet System?</li>
                            <li class="list-group-item"><strong>24/7 Support</strong><br></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>MINIGAME OR BACKEND</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">Price in consultation</span>
<!--                        <span class="price-figure mbr-fonts-style display-1"></span>-->
                        <small class="price-term mbr-fonts-style display-7"></small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">If you need a cool minigame or a huge backend system for your server!</li>
                            <li class="list-group-item"><strong>24/7 Support</strong><br></li>
                        </ul>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>

<section class="cid-qFGf8bmGnd" id="pricing-tables1-1d" data-rv-view="628">

    <div class="container">
        <h2 class="pb-5 mbr-fonts-style display-2" style="text-align: center">
            + EXTRA OPTIONS
        </h2>
        <div class="media-container-row">

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>Plugin Update's</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">
                            + $
                        </span>
                        <span class="price-figure mbr-fonts-style display-1">
                              25,-</span>
                        <small class="price-term mbr-fonts-style display-7">
                            per month
                        </small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">When you have an existing plugin. Do you want to have it under control by us?</li>
                            <li class="list-group-item"><span style="font-size: 1rem;">&nbsp;+ Version Updates</span><br></li>
                        </ul>
                    </div>
<!--                    <div class="mbr-section-btn text-center py-4 pb-5"><a href="page3.html" class="btn btn-primary display-4">BUY NOW</a></div>-->
                </div>
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>Proxy Plugin</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">
                            + $
                        </span>
                        <span class="price-figure mbr-fonts-style display-1">10,-</span>
<!--                        <small class="price-term mbr-fonts-style display-7">-->
<!--                            per month</small>-->
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">Do you want a Proxy plugin? You can. We have experience in this!</li>
                            <li class="list-group-item">Anything You want</li>
                        </ul>
                    </div>
<!--                    <div class="mbr-section-btn text-center py-4 pb-5"><a href="page3.html" class="btn btn-primary display-4">BUY NOW</a></div>-->
                </div>
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>100% Configurable</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">+ $
                        </span>
                        <span class="price-figure mbr-fonts-style display-1">
                            30,-</span>
                        <small class="price-term mbr-fonts-style display-7"></small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">We ensure that your plugin is 100% configurable with configs!<br><br><br><br></li>
                        </ul>
                    </div>
<!--                    <div class="mbr-section-btn text-center py-4 pb-5"><a href="page3.html" class="btn btn-primary display-4">BUY NOW</a></div>-->
                </div>
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>Database Included</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">
                            + $
                        </span>
                        <span class="price-figure mbr-fonts-style display-1">20,-</span>
                        <small class="price-term mbr-fonts-style display-7"></small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">SQL, MongoDB, RedisDB. It doesn't matter wich database your prefer. We can do it.</li>
<!--                            <li class="list-group-item">Sql, MongoDB, RedisDB</li>-->
                        </ul>
                    </div>
<!--                    <div class="mbr-section-btn text-center py-4 pb-5"><a href="page3.html" class="btn btn-primary display-4">BUY NOW</a></div>-->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cid-qFGmUUIAox" id="pricing-tables1-1h" data-rv-view="631">
    <div class="container">
        <div class="media-container-row">

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-6">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>Bridge Included</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">+ $
                        </span>
                        <span class="price-figure mbr-fonts-style display-1">50,-</span>
                        <small class="price-term mbr-fonts-style display-7"></small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">We will make a bridge between your proxy and bukkit so they can talk to each other.<br><br></li>
                            <li class="list-group-item">Included a proxy plugin</li>
                        </ul>
                    </div>
<!--                    <div class="mbr-section-btn text-center py-4 pb-5"><a href="page3.html" class="btn btn-primary display-4">BUY NOW</a></div>-->
                </div>
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center col-lg-6">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>Plugin Compatibility</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">
                            + $
                        </span>
                        <span class="price-figure mbr-fonts-style display-1">5</span>
                        <small class="price-term mbr-fonts-style display-7">
                            per plugin</small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">We will make sure the plugin will be compatible with the givin plugin.<br><br><br><br></li>
                        </ul>
                    </div>
<!--                    <div class="mbr-section-btn text-center py-4 pb-5"><a href="page3.html" class="btn btn-primary display-4">BUY NOW</a></div>-->
                </div>
            </div>

        </div>
    </div>
</section>

<section class="cid-qBfaKZKsXC" id="pricing-tables2-j" data-rv-view="589">

    <div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(255, 255, 255);">
    </div>

    <div class="container">
        <div class="media-container-row">

            <div class="plan col-12 mx-2 my-2 justify-content-center favorite col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>Discord Bot</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">Starting from&nbsp;</span>
                        <span class="price-figure mbr-fonts-style display-1">$25</span>
                        <small class="price-term mbr-fonts-style display-7"></small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">Do you want your own custom bot with cool features?</li><li class="list-group-item"><strong>24/7 Support</strong><br></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center favorite col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>
                            Network / Server Setup</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">Starting from</span>
                        <span class="price-figure mbr-fonts-style display-1">$50</span>
                        <small class="price-term mbr-fonts-style display-7"></small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">We have the knowledge of setting up a server like factions, skyblock, prion e.t.c.</li><li class="list-group-item"><strong>24/7 Support</strong></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center favorite col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>
                            Proxy Setup</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5"><br></span>
                        <span class="price-figure mbr-fonts-style display-1">$100</span>
                        <small class="price-term mbr-fonts-style display-7"></small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">Are you stuck or do you not not how to setup a proxy? No problem, we can do it for you.</li>
                            <li class="list-group-item"><strong>24/7 Support</strong><br></li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="plan col-12 mx-2 my-2 justify-content-center favorite col-lg-3">
                <div class="plan-header text-center pt-5">
                    <h3 class="plan-title mbr-fonts-style display-5"><strong>Buycraft Setup</strong></h3>
                    <div class="plan-price">
                        <span class="price-value mbr-fonts-style display-5">Starting from&nbsp;</span>
                        <span class="price-figure mbr-fonts-style display-1">$200</span>
                        <small class="price-term mbr-fonts-style display-7"></small>
                    </div>
                </div>
                <div class="plan-body">
                    <div class="plan-list align-center">
                        <ul class="list-group list-group-flush mbr-fonts-style display-7">
                            <li class="list-group-item">Problems with setting up buycraft? No worries, we got your back.</li>
                            <li class="list-group-item"><strong>24/7 Support</strong><br></li>
                        </ul>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>

<section class="cid-qFGWNLvsv9" id="social-buttons3-1u" data-rv-view="634" style="margin-top: 50px">

    <div class="container">
        <div class="media-container-row">
            <div class="col-md-8 align-center">
                <h2 class="pb-3 mbr-section-title mbr-fonts-style display-2">
                    SHARE THIS PAGE!
                </h2>
                <div>
                    <div class="mbr-social-likes">
                        <span class="btn btn-social socicon-bg-facebook facebook mx-2" title="Share link on Facebook">
                            <i class="socicon socicon-facebook"></i>
                        </span>
                        <span class="btn btn-social twitter socicon-bg-twitter mx-2" title="Share link on Twitter">
                            <i class="socicon socicon-twitter"></i>
                        </span>
                        <span class="btn btn-social plusone socicon-bg-googleplus mx-2" title="Share link on Google+">
                            <i class="socicon socicon-googleplus"></i>
                        </span>
                        <span class="btn btn-social vkontakte socicon-bg-vkontakte mx-2" title="Share link on VKontakte">
                            <i class="socicon socicon-vkontakte"></i>
                        </span>
                        <span class="btn btn-social odnoklassniki socicon-bg-odnoklassniki mx-2" title="Share link on Odnoklassniki">
                            <i class="socicon socicon-odnoklassniki"></i>
                        </span>
                        <span class="btn btn-social pinterest socicon-bg-pinterest mx-2" title="Share link on Pinterest">
                            <i class="socicon socicon-pinterest"></i>
                        </span>
                        <span class="btn btn-social mailru socicon-bg-mail mx-2" title="Share link on Mailru">
                            <i class="socicon socicon-mail"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include_once 'footer.php'; ?>

  <script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/smooth-scroll/smooth-scroll.js"></script>
  <script src="assets/dropdown/js/script.min.js"></script>
  <script src="assets/touch-swipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/viewport-checker/jquery.viewportchecker.js"></script>
  <script src="assets/jarallax/jarallax.min.js"></script>
  <script src="assets/social-likes/social-likes.js"></script>
  <script src="assets/theme/js/script.js"></script>
  
  
 <div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i class="mbri-down mbr-iconfont"></i></a></div>
    <input name="animation" type="hidden">
  </body>
</html>