"""visionary_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import path, include
from django.conf import settings
from django.views.generic import TemplateView
# from django.views.generic.base import RedirectView

from .sitemaps import StaticSitemap, PortfolioProjectSitemap


sitemaps = {
    'pages': StaticSitemap,
    'projects': PortfolioProjectSitemap,
}


urlpatterns = [
    path('', include('visionary_web.apps.pages.urls')),
    # path('dashboard/', TemplateView.as_view(template_name="dashboard/dashboard.html"), name='dashboard'),
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    # path('account/change_password/', TemplateView.as_view(template_name="accounts/change_password.html")),
    # path('account/forgot_password/', TemplateView.as_view(template_name="accounts/forgot_password.html")),
    # path('account/profile/', TemplateView.as_view(template_name="shop/account_profile.html")),
    # path('account/profile/edit/', TemplateView.as_view(template_name="shop/account_profile_edit.html")),
    # path('account/orders/', TemplateView.as_view(template_name="profiles/orders.html")),
    # path('account/offers/', TemplateView.as_view(template_name="profiles/offers.html")),
    # path('account/offers/edit/', TemplateView.as_view(template_name="shop/account_offers_edit.html")),
    # path('login/', TemplateView.as_view(template_name="accounts/login.html"), name='login'),
    # path('signup/', TemplateView.as_view(template_name="accounts/signup.html"), name='signup'),
    # TODO: CHECK THIS AND FIX IT
    # path('team/', TemplateView.as_view(template_name="pages/team.html"), name='team'),
    path('portfolio/', include('visionary_web.apps.portfolio.urls')),
    path('404/', TemplateView.as_view(template_name="pages/404.html"), name='404'),
    path('500/', TemplateView.as_view(template_name="pages/500.html"), name='500'),
    path('account/', include('visionary_web.apps.profiles.urls')),
    path('marketplace/', include('visionary_web.apps.marketplace.urls')),
    path('robots.txt', TemplateView.as_view(template_name="robots.txt", content_type="text/plain"), name="robots_file"),
    path('google68765b21417bb6ac.html', TemplateView.as_view(template_name="google68765b21417bb6ac.html", content_type="text/plain"), name="google_site_verification"),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name="sitemap"),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
