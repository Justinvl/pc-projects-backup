from django.urls import path
from .views import (IndexRedirectView,
                    ProductRedirectView,
                    ProductListView,

                    ProductMemberListView,
                    ProductMemberDetailView,
                    ProductManagerDetailView,

                    MemberListView,
                    MemberCreateView,
                    MemberDeleteView,

                    # MarketplaceUserListView,
                    # MarketplaceUserCreateView,
                    # MarketplaceUserDeleteView
                    )


urlpatterns = [
    path('', IndexRedirectView.as_view(), name="marketplace"),

    path('products/<str:username>/', ProductMemberListView.as_view(), name='marketplace_product_member_list'),

    path('manager/products/', ProductListView.as_view(), name="marketplace_product_list"),
    path('manager/product/<slug:slug>/', ProductManagerDetailView.as_view(), name="marketplace_manager_product_detail"),
    # path('product/<slug:slug>/') This should show admins any project they wanna see and redirect members to the one below or
    # to index if they don't have permission
    path('product/<slug:slug>/<str:username>/', ProductMemberDetailView.as_view(), name="marketplace_product_member_detail"),
    # path('product/<slug:slug>/', ProductRedirectView.as_view(), name="marketplace_product_detail"),

    # path('product/create/', ProductCreateView.as_view(), name="marketplace_product_create"),
    # path('product/<slug:slug>/update/', ProductUpdateView.as_view(), name="marketplace_product_update"),
    # path('product/<slug:slug>/delete/', ProductDeleteView.as_view(), name="marketplace_product_delete"),

    path('members/<slug:slug>/', MemberListView.as_view(), name="marketplace_members_list"),
    path('members/<slug:slug>/add/', MemberCreateView.as_view(), name="marketplace_members_add"),
    path('members/<slug:slug>/<int:pk>/remove/', MemberDeleteView.as_view(), name="marketplace_members_remove"),


    # path('users/', MarketplaceUserListView.as_view(), name="marketplace_users_list"),
    # path('users/add/<str:username>/', MarketplaceUserCreateView.as_view(), name="marketplace_users_add"),
    # path('users/remove/<str:username>/', MarketplaceUserDeleteView.as_view(), name="marketplace_users_remove")
]
