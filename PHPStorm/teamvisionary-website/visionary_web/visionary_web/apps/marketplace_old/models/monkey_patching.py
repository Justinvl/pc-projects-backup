from django.contrib.auth.models import User
from .member import Member
from .marketplace_manager import MarketplaceManager


def has_marketplace_permission(self):
    if MarketplaceManager.objects.filter(user=self).exists():
        return True
    if Member.objects.is_member(self):
        return True
    return False


def is_marketplace_manager(self):
    if MarketplaceManager.objects.filter(user=self).exists():
        return True
    return False


User.add_to_class("has_marketplace_permission", has_marketplace_permission)
User.add_to_class("is_marketplace_manager", is_marketplace_manager)
