from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.template.defaultfilters import slugify


class Product(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(unique=True, blank=True)
    description = models.TextField(null=True, blank=True)

    UNKNOWN_CATEGORY = "UC"
    SURVIVAL_SPAWN = 'SS'
    EXPLORATION_MAP = 'EM'
    ADVENTURE_MAP = 'AM'
    MINIGAME_MAP = 'MM'
    SKIN_PACK = 'SP'
    RESOURCE_PACK = 'RP'
    MESHUP_PACK = 'MP'
    CATEGORY_CHOICES = (
        (UNKNOWN_CATEGORY, 'Unknown Category'),
        (SURVIVAL_SPAWN, 'Survival Spawn'),
        (EXPLORATION_MAP, 'Exporation Map'),
        (ADVENTURE_MAP, 'Adventure Map'),
        (MINIGAME_MAP, 'Mini-Game Map'),
        (SKIN_PACK, 'Skin Pack'),
        (RESOURCE_PACK, 'Resource Pack'),
        (MESHUP_PACK, 'Mesh-Up Pack'),
    )
    category = models.CharField(max_length=2, choices=CATEGORY_CHOICES, null=True, blank=True, default=UNKNOWN_CATEGORY)

    def get_total_earnings(self):
        return self.transactions.payments_sum()

    def get_day_earnings(self, day):
        return self.transactions.filter(date__day=day.day).payments_sum()

    def get_month_earnings(self, month, year):
        return self.transactions.filter(date__month=month, date__year=year).payments_sum()

    def get_year_earnings(self, year):
        return self.transactions.filter(date__year=year).payments_sum()

    def __str__(self):
        return self.name

    # TODO: add get_absolute_url


@receiver(pre_save, sender=Product)
def pre_save_product(sender, instance, *args, **kwargs):
    instance.slug = slugify(instance.name)
