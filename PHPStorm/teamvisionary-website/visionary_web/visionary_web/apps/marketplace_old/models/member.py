from django.contrib.auth.models import User
from django.db import models
from django.core.exceptions import ValidationError
from . import Product
from django.utils.translation import gettext_lazy


class MemberQuerySet(models.query.QuerySet):
    def is_member(self, user):
        return self.filter(user=user).exists()

    def get_earnings_percentage_sum(self, product):
        return self.filter(product=product).aggregate(models.Sum('earnings_percentage'))["earnings_percentage__sum"]


class Member(models.Model):  # Maybe I need a custom Manager too
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name="members", on_delete=models.CASCADE)
    earnings_percentage = models.DecimalField(max_digits=5, decimal_places=2)  # create a pre_save that checks if the procentages add up to 100%

    objects = MemberQuerySet.as_manager()

    def get_member_earnings(self, product_earnings):
        if product_earnings is not None:
            return (self.earnings_percentage / 100) * product_earnings
        return None

    def get_day_earnings(self, day):
        return self.get_member_earnings(self.product.get_day_earnings(day))

    def get_month_earnings(self, month, year):
        return self.get_member_earnings(self.product.get_month_earnings(month, year))

    def get_year_earnings(self, year):
        return self.get_member_earnings(self.product.get_year_earnings(year))

    def get_total_earnings(self):
        return self.get_member_earnings(self.product.get_total_earnings())

    class Meta:
        unique_together = ("user", "product")

    def __str__(self):
        return self.product.name + " -> " + self.user.username + " -> " + str(self.earnings_percentage)
