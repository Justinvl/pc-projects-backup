from django.core.management import BaseCommand
from datetime import datetime
from visionary_web.apps.marketplace.models import (Product,
                                                   ProductPayment,
                                                   ProductDayPayment,
                                                   ProductMonthPayment,
                                                   ProductTotalPayment,
                                                   ProductYearPayment)


class Command(BaseCommand):
    help = "My test 2 "

    def add_arguments(self, parser):
        parser.add_argument('month', type=int)
        parser.add_argument('year', type=int)

    def handle(self, *args, **options):
        month = options['month']
        year = options['year']

        format_str = '%Y-%m'
        month_start = datetime.strptime(str(year) + "-" + str(month), format_str)
        month_end = datetime.strptime(str(year) + "-" + str(month + 1), format_str)
        payments = ProductPayment.objects.filter(date_time__gte=month_start, date_time__lt=month_end)

        for product in Product.objects.all():
            product_payments = payments.filter(product=product)
            days = {}
            month_total_payments = 0
            for product_payment in product_payments:
                day = product_payment.date_time
                days[day] = days.get(day, 0) + product_payment.payment
                month_total_payments += product_payment.payment

            for day in days:
                try:
                    day_payment = ProductDayPayment.objects.get(product=product, day=day)
                except ProductDayPayment.DoesNotExist:
                    day_payment = None

                if day_payment is None:
                    day_payment = ProductDayPayment.objects.create(
                        product=product,
                        day=day,
                        payment=days[day]
                    )
                else:
                    day_payment.payment = days[day]
                    day_payment.save()

            try:
                month_payment = ProductMonthPayment.objects.get(product=product, month=month, year=year)
            except ProductMonthPayment.DoesNotExist:
                month_payment = None

            if month_payment is None:
                month_payment = ProductMonthPayment.objects.create(
                    product=product,
                    month=month,
                    year=year,
                    payment=month_total_payments
                )
            else:
                month_payment.payment = month_total_payments
                month_payment.save()

            try:
                product_year_payment = ProductYearPayment.objects.get(product=product, year=year)
            except ProductYearPayment.DoesNotExist:
                product_year_payment = None

            if product_year_payment is None:
                product_year_payment = ProductYearPayment.objects.create(
                    product=product,
                    year=year,
                    payment=month_total_payments
                )
            else:
                if product_year_payment.year == year:
                    product_year_payment.payment += month_total_payments
                    product_year_payment.save()

            try:
                product_total_payment = ProductTotalPayment.objects.get(product=product)
            except ProductTotalPayment.DoesNotExist:
                product_total_payment = None

            if product_total_payment is None:
                product_total_payment = ProductTotalPayment.objects.create(
                    product=product,
                    payment=month_total_payments
                )
            else:
                product_total_payment.payment += month_total_payments
                product_total_payment.save()

        print("Marketplace Update DB: DONE!")
