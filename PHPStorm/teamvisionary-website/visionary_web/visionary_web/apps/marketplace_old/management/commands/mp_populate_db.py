from django.core.management import BaseCommand

import csv
import logging
from datetime import datetime
from decimal import Decimal, InvalidOperation

from visionary_web.apps.marketplace.models import Product, Transaction


logging.basicConfig(filename='mp_populate_db.log', level=logging.DEBUG)


def get_datetime_from_string(transaction_date):
    transaction_date = transaction_date.strip()

    parts = transaction_date.split(' ')

    if len(parts) != 2 and len(parts) != 3:
        return None

    if len(parts) == 2:
        date_format = '%m/%d/%Y %I:%M:%S'
    elif len(parts) == 3:
        date_format = '%m/%d/%Y %I:%M:%S %p'

    date_parts = parts[0].split('/')
    time_parts = parts[1].split(':')

    if len(date_parts) != 3 or len(time_parts) != 3:
        return None

    return datetime.strptime(transaction_date, date_format)


def get_product_name_from_string(product_name):
    product_name = product_name.strip()

    if product_name.isspace():
        return None

    return product_name


def get_decimal_payment_from_string(payment):
    try:
        decimal_payment = Decimal(payment)
        if decimal_payment > 1000000000:
            decimal_payment /= 1000000000
    except InvalidOperation:
        return None
    return decimal_payment


class Command(BaseCommand):
    help = "My test command"

    def add_arguments(self, parser):
        parser.add_argument('csv_file', type=str)

    def handle(self, *args, **options):
        with open(options['csv_file']) as csvfile:
            reader = csv.reader(csvfile, delimiter=',')

            for index, row in enumerate(reader):

                # Check if the row is valid, must have 26 columns
                # TODO: Does this work or is it redundant?
                if len(row) != 26:
                    logging.warning("The row at index {0} doesn't have 26 columns.".format(index))
                    continue

                # Get the transaction date and check if it's valid
                entry_transaction_date = row[3]
                transaction_date = get_datetime_from_string(entry_transaction_date)

                if transaction_date is None:
                    logging.error("Couldn't get the datetime for transaction date on row {0}.".format(index))
                    continue

                # Get the product name and check if it's valid
                entry_product_name = row[7]
                product_name = get_product_name_from_string(entry_product_name)

                if product_name is None:
                    logging.error("Couldn't get the Product Name on row {0}".format(index))
                    continue

                # Get the payment and check if it's valid
                entry_payment = row[21]
                payment = get_decimal_payment_from_string(entry_payment)

                if payment is None:
                    logging.error("Couldn't convert the Payment to Decimal on row {0}".format(index))
                    continue

                # Get the Product object
                product, created = Product.objects.get_or_create(
                    name=product_name
                )

                # Create the ProductPayment object
                Transaction.objects.create(
                    product=product,
                    date=transaction_date,
                    payment=payment
                )

                # From here on I will use the product_payment object to get the transaction_date and payment.\

                # Update the ProductDayPayment if already exists. Otherwise, create it.
                # try:
                #     product_day_payment = ProductDayPayment.objects.get(product=product, day=product_payment.date_time)
                #     product_day_payment.payment += product_payment.payment
                #     product_day_payment.save()
                # except ProductDayPayment.DoesNotExist:
                #     product_day_payment = ProductDayPayment.objects.create(
                #         product=product,
                #         day=product_payment.date_time,
                #         payment=product_payment.payment
                #     )

                # # Update the ProductMonthPayment if already exists. Otherwise, create it.
                # try:
                #     product_month_payment = ProductMonthPayment.objects.get(product=product,
                #                                                             month=product_payment.date_time.month,
                #                                                             year=product_payment.date_time.year)
                #     product_month_payment.payment += product_payment.payment
                #     product_month_payment.save()
                # except ProductMonthPayment.DoesNotExist:
                #     product_month_payment = ProductMonthPayment.objects.create(
                #         product=product,
                #         month=product_payment.date_time.month,
                #         year=product_payment.date_time.year,
                #         payment=product_payment.payment
                #     )

                # # Update the ProductYearPayment if already exists. Otherwise, create it.
                # try:
                #     product_year_payment = ProductYearPayment.objects.get(product=product,
                #                                                           year=product_payment.date_time.year)
                #     product_year_payment.payment += product_payment.payment
                #     product_year_payment.save()
                # except ProductYearPayment.DoesNotExist:
                #     product_year_payment = ProductYearPayment.objects.create(
                #         product=product,
                #         year=product_payment.date_time.year,
                #         payment=product_payment.payment
                #     )

                # # Update the ProductTotalPayment if already exists. Otherwise, create it.
                # try:
                #     product_total_payment = ProductTotalPayment.objects.get(product=product)
                #     product_total_payment.payment += product_payment.payment
                #     product_total_payment.save()
                # except ProductTotalPayment.DoesNotExist:
                #     product_total_payment = ProductTotalPayment.objects.create(
                #         product=product,
                #         payment=product_payment.payment
                #     )

                # TODO: Create the other models here too.

                # logging.info("Added {0}, {1}, {2}, {3}, {4}".format(product_payment,
                #                                                     product_day_payment,
                #                                                     product_month_payment,
                #                                                     product_year_payment,
                #                                                     product_total_payment
                #                                                     ))

                # print("Processed {0}".format(product_payment))
