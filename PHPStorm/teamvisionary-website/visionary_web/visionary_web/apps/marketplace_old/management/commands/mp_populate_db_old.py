from django.core.management import BaseCommand

import csv
from datetime import datetime
from decimal import Decimal, InvalidOperation
from visionary_web.apps.marketplace.models import Product, ProductPayment


class Command(BaseCommand):
    help = "My test command"

    def add_arguments(self, parser):
        parser.add_argument('csv_file', type=str)

    def handle(self, *args, **options):
        with open(options['csv_file']) as csvfile:
            reader = csv.reader(csvfile, delimiter=',')

            for index, row in enumerate(reader):

                # Check if the row is valid, must have at least 22 elements
                if len(row) >= 22:

                    # DATE - ROW INDEX: 3
                    row_parts = row[3].split(' ')
                    if len(row_parts) >= 3:
                        date_parts = row_parts[0].split('/')
                        time_parts = row_parts[1].split(':')
                        if len(date_parts) == 3 and len(time_parts) == 3:
                            date_str = row[3]
                            format_str = '%m/%d/%Y %I:%M:%S %p'
                            datetime_obj = datetime.strptime(date_str, format_str)

                            # PRODUCT NAME - ROW INDEX: 7
                            product_name = row[7].strip()

                            # PAYMENT - ROW INDEX: 21
                            try:
                                payment = Decimal(row[21])
                            except InvalidOperation:
                                print("Invalid Payment! Row: " + str(index))

                            product, product_created = Product.objects.get_or_create(
                                name=product_name
                            )

                            ProductPayment.objects.create(
                                product=product,
                                date_time=datetime_obj,
                                payment=payment
                            )

                            # try:
                            #     content_payment = ContentPayment.objects.get(
                            #         content=content,
                            #         transaction_datetime=datetime_obj,
                            #     )
                            # except ContentPayment.DoesNotExist:
                            #     content_payment = None
                            #  THEY DON'T HAVE TO BE UNIQUE TOGETHER, JUST STORE THE DATA AS IN THE CSV FILE. YOUR DAY/MONTH REVENUE SHOULD
                            #  BE UNIQUE, NOT THIS.
