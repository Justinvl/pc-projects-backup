from ..models import MarketplaceManager, Product, Member
from django.core.exceptions import PermissionDenied


def user_is_marketplace_manager(user):
    if MarketplaceManager.objects.filter(user=user).exists():
        return True
    return False


class MarketplacePermissionMixin:
    def dispatch(self, request, *args, **kwargs):
        if self.user_has_permissions(request):
            return super(MarketplacePermissionMixin, self).dispatch(
                request, *args, **kwargs)
        else:
            raise PermissionDenied

    def user_has_permissions(self, request):

        if user_is_marketplace_manager(self.request.user):
            return True

        if Member.objects.is_member(self.request.user):
            return True

        return False


class MarketplaceManagerPermissionMixin:
    def dispatch(self, request, *args, **kwargs):
        if self.user_has_permissions(request):
            return super(MarketplaceManagerPermissionMixin, self).dispatch(
                request, *args, **kwargs)
        else:
            raise PermissionDenied

    def user_has_permissions(self, request):
        return user_is_marketplace_manager(self.request.user)


class ProductMemberFilterMixin:
    def get_queryset(self):
        return Product.objects.filter(members__user=self.request.user)


class ProductMemberPermissionMixin:
    def dispatch(self, request, *args, **kwargs):
        if self.user_has_permissions(request):
            return super(ProductMemberPermissionMixin, self).dispatch(
                request, *args, **kwargs)
        else:
            raise PermissionDenied

    def user_has_permissions(self, request):
        if self.object.members.filter(user=self.request.user).exists():
            return True
        return False

# class ProductListViewPermissionMixin:
#     def dispatch(self, request, *args, **kwargs):
#         if self.user_has_permissions(request):
#             return super(ProductListViewPermissionMixin, self).dispatch(
#                 request, *args, **kwargs)
#         else:
#             raise PermissionDenied

#     def user_has_permissions(self, request):
#         if self.request.user.has_perm('marketplace_view_all_projects'):
#             return True
#         return False


# class ProductMemberViewPermissionMixin:
#     def dispatch(self, request, *args, **kwargs):
#         if self.user_has_permissions(request):
#             return super(ProductMemberViewPermissionMixin, self).dispatch(
#                 request, *args, **kwargs)
#         else:
#             raise PermissionDenied

#     def user_has_permissions(self, request):
#         try:
#             slug = self.kwargs['slug']
#             product = Product.objects.get(slug=slug)
#         except Product.DoesNotExist:
#             product = None

#         if product is None:
#             return False

#         if product.members.filter(user=request.user).count() == 0:
#             return False

#         return True
