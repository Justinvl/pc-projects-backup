
def get_year_months(year, starting_month):
    months = year * 12 + starting_month - 1
    return [((months - i) // 12, (months - i) % 12 + 1) for i in range(6)]
