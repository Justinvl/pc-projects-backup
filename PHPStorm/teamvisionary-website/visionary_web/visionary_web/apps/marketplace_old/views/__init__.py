from .product import (IndexRedirectView, 
                      ProductRedirectView, 
                      ProductListView,
                      ProductMemberListView,
                      ProductMemberDetailView,
                      ProductManagerDetailView)
from .member import (MemberCreateView,
                     MemberDeleteView,
                     MemberListView)
# from .marketplace_user import (MarketplaceUserListView,
#                                MarketplaceUserCreateView,
#                                MarketplaceUserDeleteView)