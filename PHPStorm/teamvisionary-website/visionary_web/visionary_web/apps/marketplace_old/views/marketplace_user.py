from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.views import View
from django.views.generic.list import ListView
from django.views.generic.edit import DeleteView
from django.shortcuts import get_object_or_404

from django.contrib.auth.models import User

from .mixins import MarketplaceManagerPermissionMixin


# class MarketplaceUserListView(LoginRequiredMixin, MarketplaceManagerPermissionMixin, SuccessMessageMixin, ListView):
#     template_name = "marketplace/marketplace_user_list.html"
#     model = MarketplaceUser


# class MarketplaceUserCreateView(LoginRequiredMixin, MarketplaceManagerPermissionMixin, SuccessMessageMixin, View):
#     success_message = "Marketplace rights were granted to the user successfully!"

#     def post(self, request, *args, **kwargs):
#         user = get_object_or_404(User, username=self.kwargs['username'])
#         MarketplaceUser(user=user).save()
#         messages.success(self.request, self.success_message)
#         return HttpResponseRedirect('/profiles/public/' + self.kwargs['username'])


# class MarketplaceUserDeleteView(LoginRequiredMixin, MarketplaceManagerPermissionMixin, SuccessMessageMixin, DeleteView):
#     template_name = "marketplace/marketplace_user_delete.html"
#     model = MarketplaceUser
#     success_message = "Marketplace rights were removed for the user successfully!"

#     def get_success_url(self):
#         return '/profiles/public/' + self.object.user.username

#     def delete(self, request, *args, **kwargs):
#         messages.success(self.request, self.success_message)
#         return super(MarketplaceUserDeleteView, self).delete(request, *args, **kwargs)

#     def get_object(self):
#         return get_object_or_404(MarketplaceUser, user__username=self.kwargs['username'])
