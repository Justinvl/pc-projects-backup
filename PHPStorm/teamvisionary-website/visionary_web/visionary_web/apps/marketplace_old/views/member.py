from django.core.exceptions import ValidationError
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db import IntegrityError
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView

from ..models import Member, Product
from .mixins import MarketplaceManagerPermissionMixin


class ProjectSlugMixin:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["project_slug"] = self.kwargs['slug']
        return context


class MemberListView(LoginRequiredMixin, MarketplaceManagerPermissionMixin, ProjectSlugMixin, ListView):
    template_name = 'marketplace/member_list.html'
    model = Member

    def get_queryset(self):
        return Member.objects.filter(product__slug=self.kwargs['slug'])


class MemberCreateView(LoginRequiredMixin, MarketplaceManagerPermissionMixin, SuccessMessageMixin, ProjectSlugMixin, CreateView):
    template_name = 'marketplace/member_add.html'
    model = Member
    fields = ['user', 'earnings_percentage']
    success_url = '/marketplace/'
    success_message = "The member was added to the project successfully!"

    def form_valid(self, form):
        product = Product.objects.get(slug=self.kwargs['slug'])
        earnings_percentage = form.cleaned_data.get('earnings_percentage')

        user = form.cleaned_data.get('user')
        if product.members.filter(user=user).exists():
            form.add_error('user', ValidationError('User is already a member!'))
            return super().form_invalid(form)

        if earnings_percentage > 100:
            form.add_error('earnings_percentage', ValidationError('Earnings percentage cannot exceed 100%!'))
            return super().form_invalid(form)

        earnings_percentage_sum = Member.objects.get_earnings_percentage_sum(product)
        if earnings_percentage_sum is not None:
            if earnings_percentage + earnings_percentage_sum > 100:
                form.add_error('earnings_percentage', ValidationError('Cannot add Member because earnings percentage sum exceeds 100%!'))
                return super().form_invalid(form)

        self.object = form.save(commit=False)
        self.object.product = product
        self.object.save()

        return super().form_valid(form)


class MemberDeleteView(LoginRequiredMixin, MarketplaceManagerPermissionMixin, SuccessMessageMixin, ProjectSlugMixin, DeleteView):
    template_name = 'marketplace/member_remove.html'
    model = Member
    success_url = '/marketplace/'
    success_message = "The member was removed from the project successfully!"

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(MemberDeleteView, self).delete(request, *args, **kwargs)
