from django.core.exceptions import PermissionDenied
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.views.generic.base import RedirectView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from ..models import Product, Member
from .mixins import (MarketplacePermissionMixin,
                     MarketplaceManagerPermissionMixin,
                     ProductMemberFilterMixin,
                     ProductMemberPermissionMixin)
from .utils import get_year_months

import calendar
import json
from decimal import Decimal


class IndexRedirectView(LoginRequiredMixin, MarketplacePermissionMixin, RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        return '/marketplace/products/' + self.request.user.username + '/'


class ProductRedirectView(LoginRequiredMixin, MarketplacePermissionMixin, RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        return '/marketplace/product/' + self.kwargs['slug'] + '/' + self.request.user.username + '/'


class ProductListView(LoginRequiredMixin, MarketplaceManagerPermissionMixin, ListView):
    template_name = 'marketplace/product_list.html'
    model = Product

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        product_list = context["object_list"]
        for product in product_list:
            total_earnings = product.get_total_earnings()
            product.total_earnings = "{:.2f}".format(total_earnings)

        return context


class ProductMemberListView(LoginRequiredMixin, MarketplacePermissionMixin, ProductMemberFilterMixin, ListView):
    template_name = 'marketplace/product_member_list.html'
    model = Product

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        product_list = context["product_list"]

        if product_list.count() == 0:
            return context

        monthly_earnings = dict()
        now = timezone.now()
        total_earnings = 0
        average_percentage = 0

        total_earnings = 0
        member_earnings_percentage = 0

        for product in product_list:
            member = get_object_or_404(Member, user=self.request.user, product=product)
            member_total_earnings = member.get_total_earnings()
            total_earnings += member_total_earnings
            product.member_total_earnings = "{:.2f}".format(member_total_earnings)
            product.member_earnings_percentage = "{:.2f}".format(member.earnings_percentage)
            average_percentage += member.earnings_percentage

        average_percentage = "{:.2f}".format(average_percentage / product_list.count())

        year_months = get_year_months(now.year, now.month)

        for year_month in year_months:
            total_month_earnings = 0
            for product in product_list:
                member = get_object_or_404(Member, user=self.request.user, product=product)
                member_month_earnings = member.get_month_earnings(year_month[1], year_month[0])
                if member_month_earnings is not None:
                    total_month_earnings += member_month_earnings
            monthly_earnings[calendar.month_name[year_month[1]]] = "{:.2f}".format(total_month_earnings)

        context["monthly_earnings"] = json.dumps(monthly_earnings)
        context["total_earnings"] = "{:.2f}".format(total_earnings)
        context["member_earnings_percentage"] = member_earnings_percentage
        context["average_percentage"] = average_percentage

        return context


class ProductStatisticsMixin():

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        product_monthly_earnings = dict()
        now = timezone.now()

        product = context["product"]
        context["product_total_earnings"] = "{:.2f}".format(product.get_total_earnings())

        year_months = get_year_months(now.year, now.month)

        for year_month in year_months:
            month_earnings = product.get_month_earnings(month=year_month[1], year=year_month[0])
            month_name = calendar.month_name[year_month[1]]
            if month_earnings is not None:
                product_monthly_earnings[month_name] = "{:.2f}".format(month_earnings)
            else:
                product_monthly_earnings[month_name] = 0

        product.monthly_earnings = json.dumps(product_monthly_earnings)
        product.total_earnings = "{:.2f}".format(product.get_total_earnings())

        context['members'] = product.members

        return context


def get_member_statistics(context, user):
    product = context["product"]

    member_monthly_earnings = dict()
    member = get_object_or_404(Member, user=user, product=product)

    now = timezone.now()
    year_months = get_year_months(now.year, now.month)

    for year_month in year_months:
        month_earnings = product.get_month_earnings(month=year_month[1], year=year_month[0])
        month_name = calendar.month_name[year_month[1]]
        if month_earnings is not None:
            member_monthly_earnings[month_name] = "{:.2f}".format((member.earnings_percentage / 100) * month_earnings)
        else:
            member_monthly_earnings[month_name] = 0

    context["member_monthly_earnings"] = json.dumps(member_monthly_earnings)
    context["member_earnings_percentage"] = "{:.2f}".format(member.earnings_percentage)
    context["member_total_earnings"] = "{:.2f}".format(member.get_total_earnings())

    last_date = product.transactions.order_by('date').last().date
    month = last_date.month
    year = last_date.year
    context["last_month_earnings"] = "{:.2f}".format(Decimal(member.get_month_earnings(month, year)))


class ProductManagerDetailView(LoginRequiredMixin, MarketplaceManagerPermissionMixin, ProductStatisticsMixin, DetailView):
    template_name = 'marketplace/product_detail.html'
    model = Product

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        product = context["product"]
        context["user_is_member"] = False
        if product.members.filter(user=self.request.user).exists():
            context["user_is_member"] = True
            get_member_statistics(context, self.request.user)
        return context


class ProductMemberDetailView(LoginRequiredMixin, MarketplacePermissionMixin, ProductMemberPermissionMixin, ProductStatisticsMixin, DetailView):
    template_name = 'marketplace/product_detail.html'
    model = Product

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        get_member_statistics(context, self.request.user)
        context["user_is_member"] = True

        return context
