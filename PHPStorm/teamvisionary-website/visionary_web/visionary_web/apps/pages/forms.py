from django import forms


class ContactForm(forms.Form):
    name = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    message = forms.CharField(widget=forms.Textarea, required=True)


class ApplicationForm(forms.Form):
    email = forms.CharField(required=True)
    age = forms.CharField(required=True)
    minecraft_username = forms.CharField(required=True)
    experience_years = forms.CharField(required=True)
    country = forms.CharField(required=True)
    available_hours = forms.CharField(required=True)
    other_build_teams = forms.CharField(widget=forms.Textarea, required=True)
    comfortable_build_styles = forms.CharField(widget=forms.Textarea, required=True)
    uncomfortable_build_styles = forms.CharField(widget=forms.Textarea, required=True)
    build_plugins_experience = forms.CharField(widget=forms.Textarea, required=True)
    why_join_team = forms.CharField(widget=forms.Textarea, required=True)
    where_found_team = forms.CharField(required=True)
    special_skills = forms.CharField(widget=forms.Textarea, required=True)
    skype_discord = forms.CharField(required=True)
    portfolio_work = forms.CharField(widget=forms.Textarea, required=True)
