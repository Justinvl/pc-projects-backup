from django.urls import path
from .views import ContactFormView, AccountView, HomepageView, ManagementView, ApplicationFormView, DocumenationListView, DashboardView, ProductListView, ProductDetailView, ProductIntervalEarningsView
from django.views.generic import TemplateView


urlpatterns = [
    path('', HomepageView.as_view(), name='index'),
    # path('dashboard/', DashboardView.as_view(), name='dashboard'),
    # path('dashboard/projects/', ProductListView.as_view()),
    # path('dashboard/projects/project/', ProductDetailView.as_view()),
    # path('dashboard/projects/project/earnings/', ProductIntervalEarningsView.as_view()),
    # path('management/', ManagementView.as_view()),
    # path('dashboard/account/', AccountView.as_view()),
    # path('documentation/', DocumenationListView.as_view()),
    path('contact/', ContactFormView.as_view(), name='contact'),
    path('apply/', ApplicationFormView.as_view(), name="apply"),
]
