from django.views.generic.edit import FormView
from django.views.generic.list import ListView
from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.views.generic import TemplateView

import json
import urllib

from visionary_web.apps.portfolio.models import Project
from .forms import ContactForm, ApplicationForm


class HomepageView(ListView):
    template_name = 'pages/homepage.html'
    model = Project

    def get_queryset(self):
        return Project.objects.all().order_by("-id")[:6]


class DashboardView(TemplateView):
    template_name = "dashboard/marketplace_home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["navigation_links"] = [
            {
                'label': 'Marketplace'
            }
        ]
        return context


class ProductListView(TemplateView):
    template_name = "dashboard/product_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["navigation_links"] = [
            {
                'url': '/dashboard/',
                'label': 'Marketplace'
            },
            {
                'label': 'Projects'
            }
        ]
        return context


class ProductDetailView(TemplateView):
    template_name = "dashboard/product_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["navigation_links"] = [
            {
                'url': '/dashboard/',
                'label': 'Marketplace'
            },
            {
                'url': '/dashboard/projects/',
                'label': 'Projects'
            },
            {
                'label': 'Project name'
            }
        ]
        return context


class ProductIntervalEarningsView(TemplateView):
    template_name = "dashboard/product_interval_earnings.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["navigation_links"] = [
            {
                'url': '/dashboard/',
                'label': 'Marketplace'
            },
            {
                'url': '/dashboard/projects/',
                'label': 'Projects'
            },
            {
                'url': '/dashboard/projects/project/',
                'label': 'Project name'
            },
            {
                'label': 'Earnings between date1 and date2'
            }
        ]
        return context


class DocumenationListView(TemplateView):
    template_name = "dashboard/documentation.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["navigation_links"] = [
            {
                'label': 'Documentation'
            }
        ]
        return context


class ManagementView(TemplateView):
    template_name = "dashboard/management.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["navigation_links"] = [
            {
                'label': 'Management'
            }
        ]
        return context


class AccountView(TemplateView):
    template_name = "dashboard/account.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["navigation_links"] = [
            {
                'label': 'Account'
            }
        ]
        return context


def verify_recaptcha(request):
    recaptcha_response = request.POST.get('g-recaptcha-response')
    url = 'https://www.google.com/recaptcha/api/siteverify'
    values = {
        'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
        'response': recaptcha_response
    }
    data = urllib.parse.urlencode(values).encode()
    req = urllib.request.Request(url, data=data)
    response = urllib.request.urlopen(req)
    result = json.loads(response.read().decode())
    return result


class ContactFormView(FormView):
    template_name = "pages/contact.html"
    form_class = ContactForm
    success_url = "/contact/"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_info"] = {
            'title': 'Contact'
        }
        return context

    def form_valid(self, form):
        result = verify_recaptcha(self.request)

        print(form.cleaned_data)

        if result['success']:
            email_message = render_to_string('pages/email_contact.txt', form.cleaned_data)
            send_mail("Team Visionary new message", email_message, settings.EMAIL_HOST_USER, ['c.stunt3r@gmail.com'], fail_silently=False)
            messages.success(self.request, 'Your message was sent successfully!')
        else:
            messages.error(self.request, 'Invalid reCAPTCHA. Please try again.')
            return super().form_invalid(form)

        return super().form_valid(form)


class ApplicationFormView(FormView):
    template_name = "pages/apply.html"
    form_class = ApplicationForm
    success_url = "/apply/"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_info"] = {
            'title': 'Apply'
        }
        return context

    def form_valid(self, form):
        result = verify_recaptcha(self.request)

        if result['success']:
            email_message = render_to_string('pages/email_application.txt', form.cleaned_data)
            send_mail("Team Visionary new application", email_message, settings.EMAIL_HOST_USER, ['c.stunt3r@gmail.com'], fail_silently=False)
            messages.success(self.request, 'Your application was sent successfully!')
        else:
            messages.error(self.request, 'Invalid reCAPTCHA. Please try again.')
            return super().form_invalid(form)

        return super().form_valid(form)
