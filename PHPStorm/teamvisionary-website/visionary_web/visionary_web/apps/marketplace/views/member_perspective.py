from ..models import Product, Member, Transaction
from .mixins import MarketplacePermissionMixin

from django.shortcuts import get_object_or_404

from django.contrib.auth.mixins import LoginRequiredMixin

from django.views.generic.list import ListView
from django.views.generic import TemplateView

from . import mp_utils


class DashboardView(LoginRequiredMixin, MarketplacePermissionMixin, TemplateView):
    template_name = "marketplace/dashboard.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["navigation_links"] = [
            {
                'label': 'Dashboard'
            }
        ]

        product_list = Product.objects.get_member_products(self.request.user)

        context["projects"] = product_list[:4]
        context["projects_count"] = product_list.count()
        context["projects_count_remaining"] = product_list.count() - 4

        member_total_earnings = Member.objects.get_member_total_earnings(self.request.user)
        context["member_total_earnings"] = "{:.2f}".format(member_total_earnings)
        context["member_last_30_days_earnings"] = "{:.2f}".format(Member.objects.get_member_last_30_days_earnings(self.request.user))
        highest_lowest_earning_products = Member.objects.get_member_highest_lowest_earning_products(self.request.user)
        context["member_highest_earning_product"] = highest_lowest_earning_products[0]
        context["member_lowest_earning_product"] = highest_lowest_earning_products[1]
        member_average_earnings_per_product = member_total_earnings / Member.objects.get_member_number_of_products(self.request.user)
        context["member_average_earnings_per_product"] = "{:.2f}".format(member_average_earnings_per_product)

        # Last 6 months earnings
        last_transaction_date = Transaction.objects.order_by('-date').first().date
        dates = []
        for m_delta in range(-5, 1):
            dates.append(mp_utils.monthdelta(last_transaction_date, m_delta))

        months_earnings = []
        for date in dates:
            total_month_earnings = 0
            for product in product_list:
                member = get_object_or_404(Member, user=self.request.user, product=product)
                member_month_earnings = member.get_month_earnings(month=date.month, year=date.year)
                if member_month_earnings is not None:
                    total_month_earnings += member_month_earnings
            months_earnings.append((date.strftime("%B"), "{:.2f}".format(total_month_earnings)))

        context["last_6_months_earnings"] = reversed(months_earnings)
        context["months"] = [i[0] for i in months_earnings]
        context["earnings"] = [i[1] for i in months_earnings]
        # End

        return context


class MemberProductListView(LoginRequiredMixin, MarketplacePermissionMixin, ListView):
    template_name = "marketplace/product_list.html"
    model = Product

    def get_queryset(self):
        queryset = Product.objects.get_member_products(self.request.user)

        sort_by = self.request.GET.get("sort_by")
        if sort_by == "name":
            queryset = queryset.order_by('name')
        elif sort_by == "total-earnings":
            queryset = sorted(queryset, key=lambda x: x.get_total_earnings(), reverse=True)
        elif sort_by == "your-earnings":
            for product in queryset:
                product.member_earnings = product.get_member_instance(self.request.user).get_total_earnings()
            queryset = sorted(queryset, key=lambda x: x.member_earnings, reverse=True)

        search = self.request.GET.get("search")
        if search:
            queryset = queryset.filter(name__icontains=search)

        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["navigation_links"] = [
            {
                'url': '/marketplace/dashboard/',
                'label': 'Dashboard'
            },
            {
                'label': 'Projects'
            }
        ]

        product_list = context["object_list"]

        for product in product_list:
            member = product.get_member_instance(self.request.user)
            product.member_earnings = "{:.2f}".format(member.get_total_earnings())
            product.total_earnings = "{:.2f}".format(product.get_total_earnings())

        all_products = Product.objects.get_member_products(self.request.user)
        top_5_earning_products = sorted(all_products, key=lambda x: x.get_total_earnings(), reverse=True)[:5]

        context["top_5_products"] = [p.name for p in top_5_earning_products]
        context["top_5_earnings"] = ["{:.2f}".format(p.get_total_earnings()) for p in top_5_earning_products]

        return context
