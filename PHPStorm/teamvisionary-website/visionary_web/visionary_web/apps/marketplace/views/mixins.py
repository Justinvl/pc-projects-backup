from ..models import Product
from django.core.exceptions import PermissionDenied


class MarketplacePermissionMixin:
    def dispatch(self, request, *args, **kwargs):
        if self.user_has_permissions(request):
            return super(MarketplacePermissionMixin, self).dispatch(
                request, *args, **kwargs)
        else:
            raise PermissionDenied

    def user_has_permissions(self, request):

        if self.request.user.is_marketplace_manager():
            return True

        if self.request.user.has_marketplace_permission():
            return True

        return False


class ManagerRightsMixin:
    def dispatch(self, request, *args, **kwargs):
        if self.user_has_permissions(request):
            return super(ManagerRightsMixin, self).dispatch(
                request, *args, **kwargs)
        else:
            raise PermissionDenied

    def user_has_permissions(self, request):
        return self.request.user.is_marketplace_manager()


class ProductPermissionMixin:
    def dispatch(self, request, *args, **kwargs):
        if self.user_has_permissions(request):
            return super(ProductPermissionMixin, self).dispatch(
                request, *args, **kwargs)
        else:
            raise PermissionDenied

    def user_has_permissions(self, request):
        if self.request.user.is_marketplace_manager():
            return True
        if self.object.has_member(self.request.user):
            return True
        return False


class ProductMemberFilterMixin:
    def get_queryset(self):
        return Product.objects.filter(members__user=self.request.user)
