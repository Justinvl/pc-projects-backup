from django.views.generic.detail import DetailView
from .mixins import MarketplacePermissionMixin
from django.contrib.auth.mixins import LoginRequiredMixin

from django.db.models import Sum
from django.db.models.functions import Cast
from django.db.models.fields import DateField

from ..models import Product
import datetime
from decimal import Decimal
from . import mp_utils


class ProductDetailView(LoginRequiredMixin, MarketplacePermissionMixin, DetailView):
    template_name = "marketplace/product_detail.html"
    model = Product

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        product = context['object']
        member = product.get_member_instance(self.request.user)
        if member is not None:
            context["user_is_member"] = True
        else:
            context["user_is_member"] = False

        context["navigation_links"] = [
            {
                'url': '/marketplace/dashboard/',
                'label': 'Dashboard'
            },
            {
                'url': '/marketplace/projects/',
                'label': 'Projects'
            },
            {
                'label': product.name
            }
        ]

        year = datetime.datetime.now().year

        product_total_earnings = product.get_total_earnings()
        context["product_total_earnings"] = "${:.2f}".format(product_total_earnings) if product_total_earnings != Decimal(0) else "N/A"

        product_last_30_days_earnings = product.get_last_30_days_earnings()
        context["product_last_30_days_earnings"] = "${:.2f}".format(product_last_30_days_earnings) if product_last_30_days_earnings != Decimal(0) else "N/A"

        product_last_60_days_earnings = product.get_last_60_days_earnings()
        context["product_last_60_days_earnings"] = "${:.2f}".format(product_last_60_days_earnings) if product_last_60_days_earnings != Decimal(0) else "N/A"

        product_last_90_days_earnings = product.get_last_90_days_earnings()
        context["product_last_90_days_earnings"] = "${:.2f}".format(product_last_90_days_earnings) if product_last_90_days_earnings != Decimal(0) else "N/A"

        product_last_6_months_earnings = product.get_last_6_months_earnings()
        context["product_last_6_months_earnings"] = "${:.2f}".format(product_last_6_months_earnings) if product_last_6_months_earnings != Decimal(0) else "N/A"

        product_this_year_earnings = product.get_year_earnings(year)
        context["product_this_year_earnings"] = "${:.2f}".format(product_this_year_earnings) if product_this_year_earnings != Decimal(0) else "N/A"

        product_last_year_earnings = product.get_year_earnings(year - 1)
        context["product_last_year_earnings"] = "${:.2f}".format(product_last_year_earnings) if product_last_year_earnings != Decimal(0) else "N/A"

        member_total_earnings = member.get_total_earnings()
        context["member_total_earnings"] = "${:.2f}".format(member_total_earnings) if member_total_earnings != Decimal(0) else "N/A"

        member_last_30_days_earnings = member.get_last_30_days_earnings()
        context["member_last_30_days_earnings"] = "${:.2f}".format(member_last_30_days_earnings) if member_last_30_days_earnings != Decimal(0) else "N/A"

        member_last_60_days_earnings = member.get_last_60_days_earnings()
        context["member_last_60_days_earnings"] = "${:.2f}".format(member_last_60_days_earnings) if member_last_60_days_earnings != Decimal(0) else "N/A"

        member_last_90_days_earnings = member.get_last_90_days_earnings()
        context["member_last_90_days_earnings"] = "${:.2f}".format(member_last_90_days_earnings) if member_last_90_days_earnings != Decimal(0) else "N/A"

        member_last_6_months_earnings = member.get_last_6_months_earnings()
        context["member_last_6_months_earnings"] = "${:.2f}".format(member_last_6_months_earnings) if member_last_6_months_earnings != Decimal(0) else "N/A"

        member_this_year_earnings = member.get_year_earnings(year)
        context["member_this_year_earnings"] = "${:.2f}".format(member_this_year_earnings) if member_this_year_earnings != Decimal(0) else "N/A"

        member_last_year_earnings = member.get_year_earnings(year - 1)
        context["member_last_year_earnings"] = "${:.2f}".format(member_last_year_earnings) if member_last_year_earnings != Decimal(0) else "N/A"

        # Last 6 months earnings
        last_transaction_date = product.transactions.order_by('-date').first().date
        dates = []
        for m_delta in range(-5, 1):
            dates.append(mp_utils.monthdelta(last_transaction_date, m_delta))

        product_months_earnings = []
        member_months_earnings = []

        for date in dates:
            product_months_earnings.append((date.strftime("%B"), product.get_month_earnings(month=date.month, year=date.year)))
            member_months_earnings.append((date.strftime("%B"), member.get_month_earnings(month=date.month, year=date.year)))

        context["months"] = [i[0] for i in product_months_earnings]
        context["product_earnings"] = ["{:.2f}".format(i[1]) for i in product_months_earnings]
        context["member_earnings"] = ["{:.2f}".format(i[1]) for i in member_months_earnings]
        # End

        # Last 30 days earnings
        # TODO: end_date should be TODAY, I used last_transaction_date only for testing, also everywhere I use last_transaction_date, it should be TODAY
        end_date = last_transaction_date
        start_date = end_date + datetime.timedelta(-30)

        daily_transactions = product.transactions.annotate(date_only=Cast('date', DateField())) \
                                                 .filter(date__gte=start_date, date__lte=end_date) \
                                                 .values('date_only') \
                                                 .annotate(product_day_earnings=Sum('payment')) \
                                                 .order_by('-date_only')

        for transaction in list(daily_transactions):
            if member is not None:
                transaction["member_day_earnings"] = "{:.2f}".format(member.get_member_earnings(transaction["product_day_earnings"]))
            transaction["product_day_earnings"] = "{:.2f}".format(transaction["product_day_earnings"])

        context["daily_transactions_first"] = daily_transactions[:15]
        context["daily_transactions_second"] = daily_transactions[15:]  # TODO: Try [15:-1]
        # End

        return context


class ProductIntervalEarningsView(DetailView):
    template_name = "marketplace/product_interval_earnings.html"
    model = Product

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        product = context['object']
        member = product.get_member_instance(self.request.user)
        if member is not None:
            context["user_is_member"] = True
        else:
            context["user_is_member"] = False

        context["navigation_links"] = [
            {
                'url': '/marketplace/dashboard/',
                'label': 'Dashboard'
            },
            {
                'url': '/marketplace/projects/',
                'label': 'Projects'
            },
            {
                'url': '/marketplace/projects/' + product.slug,
                'label': product.name
            }
        ]

        get_start_date = self.request.GET.get("start_date")
        get_end_date = self.request.GET.get("end_date")

        if get_start_date is None or get_end_date is None:
            context["navigation_links"].append({'label': "Invalid date interval"})
            context["invalid_dates"] = True
            return context

        try:
            start_date = datetime.datetime.strptime(self.request.GET.get("start_date"), "%d-%m-%Y")
        except ValueError:
            start_date = None
        try:
            end_date = datetime.datetime.strptime(self.request.GET.get("end_date"), "%d-%m-%Y")
        except ValueError:
            end_date = None

        if start_date is None or end_date is None or start_date > end_date:
            context["navigation_links"].append({'label': "Invalid date interval"})
            context["invalid_dates"] = True
            return context

        str_start_date = start_date.strftime("%d-%m-%Y")
        str_end_date = end_date.strftime("%d-%m-%Y")

        context["navigation_links"].append(
            {
                'label': "Earnings between {0} and {1}".format(str_start_date, str_end_date)
            }
        )

        context["start_date"] = str_start_date
        context["end_date"] = str_end_date

        end_date = end_date + datetime.timedelta(days=1)

        daily_transactions = product.transactions.annotate(date_only=Cast('date', DateField())) \
                                                 .filter(date__gte=start_date, date__lte=end_date) \
                                                 .values('date_only') \
                                                 .annotate(product_day_earnings=Sum('payment')) \
                                                 .order_by('-date_only')

        if member is not None:
            for transaction in list(daily_transactions):
                transaction["member_day_earnings"] = member.get_member_earnings(transaction["product_day_earnings"])

        context["daily_transactions"] = daily_transactions

        return context
