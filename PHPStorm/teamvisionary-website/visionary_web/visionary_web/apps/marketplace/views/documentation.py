from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from .mixins import MarketplacePermissionMixin


class DocumentationView(LoginRequiredMixin, MarketplacePermissionMixin, TemplateView):
    template_name = "marketplace/documentation.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["navigation_links"] = [
            {
                'label': 'Documentation'
            }
        ]
        return context
