from django.core.management import BaseCommand

import csv
import logging
from datetime import datetime
from decimal import Decimal, InvalidOperation

from visionary_web.apps.marketplace.models import Product, Transaction


logging.basicConfig(filename='mp_populate_db.log', level=logging.DEBUG)


def is_csv_valid(first_row):
    if first_row[1].strip().lower() != "Order ID".lower():
        return False
    if first_row[3].strip().lower() != "Transaction Date Time".lower():
        return False
    if first_row[7].strip().lower() != "Product Name".lower():
        return False
    if first_row[21].strip().lower() != "Payment".lower():
        return False
    return True


def get_datetime_from_string(transaction_date):
    transaction_date = transaction_date.strip()

    parts = transaction_date.split(' ')

    if len(parts) != 2 and len(parts) != 3:
        return None

    if len(parts) == 2:
        date_format = '%m/%d/%Y %I:%M:%S'
    elif len(parts) == 3:
        date_format = '%m/%d/%Y %I:%M:%S %p'

    date_parts = parts[0].split('/')
    time_parts = parts[1].split(':')

    if len(date_parts) != 3 or len(time_parts) != 3:
        return None

    return datetime.strptime(transaction_date, date_format)


def get_text_from_string(cell_string):
    text = cell_string.strip()

    if text.isspace():
        return None

    return text


def get_decimal_payment_from_string(payment):
    try:
        decimal_payment = Decimal(payment)
        if decimal_payment > 1000000000:
            decimal_payment /= 1000000000
    except InvalidOperation:
        return None
    return decimal_payment


class Command(BaseCommand):
    help = "Command used to populate Transaction model with a CSV file"

    def add_arguments(self, parser):
        parser.add_argument('csv_file', type=str)

    def handle(self, *args, **options):
        with open(options['csv_file']) as csvfile:
            reader = csv.reader(csvfile, delimiter=',')

            first_row = next(reader)
            if not is_csv_valid(first_row):
                logging.error("The CSV file is not valid.")
                return

            for index, row in enumerate(reader):

                # Check if the row is valid, must have 26 columns
                if len(row) <= 22:
                    logging.warning("The row at index {0} doesn't have at least 22 columns.".format(index))
                    continue

                # Get the Order ID and check if it's valid
                entry_order_id = row[1]
                order_id = get_text_from_string(entry_order_id)
                if order_id is None:
                    logging.error("Couldn't get the Order ID on row {0}".format(index))
                    continue

                # Check if the Transaction already exists in the DB
                if Transaction.objects.filter(order_id=order_id).exists():
                    logging.warning("The Transaction with Order ID: {0} already exists in the DB.".format(order_id))
                    continue

                # Get the transaction date and check if it's valid
                entry_transaction_date = row[3]
                transaction_date = get_datetime_from_string(entry_transaction_date)
                if transaction_date is None:
                    logging.error("Couldn't get the datetime for transaction date on row {0}.".format(index))
                    continue

                # Get the product name and check if it's valid
                entry_product_name = row[7]
                product_name = get_text_from_string(entry_product_name)
                if product_name is None:
                    logging.error("Couldn't get the Product Name on row {0}".format(index))
                    continue

                # Get the payment and check if it's valid
                entry_payment = row[21]
                payment = get_decimal_payment_from_string(entry_payment)
                if payment is None:
                    logging.error("Couldn't convert the Payment to Decimal on row {0}".format(index))
                    continue

                # Get the Product object
                product, created = Product.objects.get_or_create(
                    name=product_name
                )

                # Create the Transaction object
                Transaction.objects.create(
                    order_id=order_id,
                    product=product,
                    date=transaction_date,
                    payment=payment
                )

            print("The command finished running.")
            print("Total value of transactions: " + str(Transaction.objects.all().payments_sum()))
