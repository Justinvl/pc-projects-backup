from django.contrib import admin
from .models import (Product,
                     Member,
                     Transaction,
                     MarketplaceManager)


admin.site.register(Product)
admin.site.register(Member)
admin.site.register(Transaction)
admin.site.register(MarketplaceManager)
