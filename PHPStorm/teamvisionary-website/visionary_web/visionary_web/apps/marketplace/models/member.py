from django.contrib.auth.models import User
from django.db import models
from . import Product
from decimal import Decimal


class MemberQuerySet(models.query.QuerySet):
    def is_member(self, user):
        return self.filter(user=user).exists()

    def get_earnings_percentage_sum(self, product):
        return self.filter(product=product).aggregate(models.Sum('earnings_percentage'))["earnings_percentage__sum"]

    def get_member_number_of_products(self, user):
        return self.filter(user=user).count()

    def get_member_highest_lowest_earning_products(self, user):
        memberships = self.filter(user=user)
        highest_earnings = Decimal(0)
        highest_earning_product = None
        lowest_earnings = Decimal(1000000000)
        lowest_earning_product = None
        for member in memberships:
            member_total_earnings = member.get_total_earnings()
            if member_total_earnings > highest_earnings:
                highest_earnings = member_total_earnings
                highest_earning_product = member.product
            if member_total_earnings < lowest_earnings:
                lowest_earnings = member_total_earnings
                lowest_earning_product = member.product
        return (highest_earning_product, lowest_earning_product)

    def get_member_last_30_days_earnings(self, user):
        memberships = self.filter(user=user)
        last_30_days_earnings = Decimal(0)
        for member in memberships:
            last_30_days_earnings += member.get_last_30_days_earnings()
        return last_30_days_earnings

    def get_member_total_earnings(self, user):
        memberships = self.filter(user=user)
        total_earnings = Decimal(0)
        for member in memberships:
            total_earnings += member.get_total_earnings()
        return total_earnings


class Member(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name="members", on_delete=models.CASCADE)
    earnings_percentage = models.DecimalField(max_digits=5, decimal_places=2)

    objects = MemberQuerySet.as_manager()

    def get_member_earnings(self, product_earnings):
        if product_earnings is not None:
            return (self.earnings_percentage / 100) * product_earnings
        return Decimal(0)

    def get_earnings_between_dates(self, start_date, end_date):
        return self.get_member_earnings(self.product.get_earnings_between_dates(start_date, end_date))

    def get_last_n_days_earnings(self, n):
        return self.get_member_earnings(self.product.get_last_n_days_earnings(n))

    def get_last_30_days_earnings(self):
        return self.get_last_n_days_earnings(30)

    def get_last_60_days_earnings(self):
        return self.get_last_n_days_earnings(60)

    def get_last_90_days_earnings(self):
        return self.get_last_n_days_earnings(90)

    def get_last_6_months_earnings(self):
        return self.get_last_n_days_earnings(182)

    def get_day_earnings(self, day):
        return self.get_member_earnings(self.product.get_day_earnings(day))

    def get_month_earnings(self, month, year):
        return self.get_member_earnings(self.product.get_month_earnings(month, year))

    def get_year_earnings(self, year):
        return self.get_member_earnings(self.product.get_year_earnings(year))

    def get_total_earnings(self):
        return self.get_member_earnings(self.product.get_total_earnings())

    class Meta:
        unique_together = ("user", "product")

    def __str__(self):
        return self.product.name + " -> " + self.user.username + " -> " + str(self.earnings_percentage)
