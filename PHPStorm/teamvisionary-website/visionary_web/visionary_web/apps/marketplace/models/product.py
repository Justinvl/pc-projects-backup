from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.template.defaultfilters import slugify
from django.core.exceptions import ObjectDoesNotExist
import datetime
from decimal import Decimal


class ProductQuerySet(models.query.QuerySet):
    def get_member_products(self, user):
        return self.filter(members__user=user)


class Product(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(unique=True, blank=True)

    objects = ProductQuerySet.as_manager()

    def get_member_instance(self, user):
        try:
            member = self.members.get(user=user)
        except ObjectDoesNotExist:
            member = None
        return member

    def get_total_earnings(self):
        return self.transactions.payments_sum() or Decimal(0)

    def get_earnings_between_dates(self, start_date, end_date):
        return self.transactions.filter(date__range=(start_date, end_date)).payments_sum() or Decimal(0)

    def get_last_n_days_earnings(self, n):
        end_date = self.transactions.order_by('-date').first().date
        start_date = end_date + datetime.timedelta(n * -1)
        return self.get_earnings_between_dates(start_date, end_date) or Decimal(0)

    def get_last_30_days_earnings(self):
        return self.get_last_n_days_earnings(30) or Decimal(0)

    def get_last_60_days_earnings(self):
        return self.get_last_n_days_earnings(60) or Decimal(0)

    def get_last_90_days_earnings(self):
        return self.get_last_n_days_earnings(90) or Decimal(0)

    def get_last_6_months_earnings(self):
        return self.get_last_n_days_earnings(182) or Decimal(0)

    def get_day_earnings(self, day):
        return self.transactions.filter(date__day=day.day).payments_sum() or Decimal(0)

    def get_month_earnings(self, month, year):
        return self.transactions.filter(date__month=month, date__year=year).payments_sum() or Decimal(0)

    def get_year_earnings(self, year):
        return self.transactions.filter(date__year=year).payments_sum() or Decimal(0)

    def has_member(self, user):
        return self.members.filter(user=user).exists()

    def __str__(self):
        return self.name


@receiver(pre_save, sender=Product)
def pre_save_product(sender, instance, *args, **kwargs):
    instance.slug = slugify(instance.name)
