from django.db import models
from . import Product


class TransactionQuerySet(models.query.QuerySet):
    def payments_sum(self):
        return self.aggregate(models.Sum('payment'))["payment__sum"]


class Transaction(models.Model):
    order_id = models.CharField(max_length=50, unique=True)
    product = models.ForeignKey(Product, related_name="transactions", on_delete=models.CASCADE)
    date = models.DateTimeField()
    payment = models.DecimalField(max_digits=10, decimal_places=5)

    objects = TransactionQuerySet.as_manager()

    def __str__(self):
        return "Transaction[{0}, {1}, {2}]".format(self.product.name, self.payment, self.date)
