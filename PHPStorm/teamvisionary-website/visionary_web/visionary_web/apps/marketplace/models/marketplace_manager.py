from django.contrib.auth.models import User
from django.db import models


class MarketplaceManager(models.Model):
    user = models.OneToOneField(User, related_name="marketplace_manager", on_delete=models.CASCADE, unique=True)

    def __str__(self):
        return self.user.username
