from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from PIL import Image


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    paypal_address = models.EmailField(null=True, blank=True)  # TODO: Make this an E-mail field maybe?
    avatar = models.ImageField(upload_to="avatars/", null=True, blank=True)

    about = models.TextField(blank=True)
    country = models.CharField(max_length=50, blank=True)
    timezone = models.CharField(max_length=50, blank=True)

    facebook = models.URLField(blank=True)
    twitter = models.URLField(blank=True)
    youtube = models.URLField(blank=True)
    planet_minecraft = models.URLField(blank=True)

    def save(self, **kwargs):
        super(UserProfile, self).save()
        if self.avatar and hasattr(self.avatar, 'file'):
            image = Image.open(self.avatar)
            resized_avatar = image.resize((250, 250))
            resized_avatar.save(self.avatar.path)


@receiver(post_save, sender=User)
def post_save_userprofile(sender, instance, created, *args, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)
