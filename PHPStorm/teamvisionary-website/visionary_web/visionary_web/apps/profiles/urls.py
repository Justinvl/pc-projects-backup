from django.urls import path
from .views import (UserProfileView,
                    UserProfileUpdateView,
                    UserUpdateView,
                    UserProfileAvatarUpdateView,
                    PublicProfileView)


urlpatterns = [
    path('public/<str:username>/', PublicProfileView.as_view(), name="account_public_profile"),
    path('profile/', UserProfileView.as_view(), name='account_profile'),
    path('profile/edit/info/', UserProfileUpdateView.as_view(), name='account_profile_edit_info'),
    path('profile/edit/personal/', UserUpdateView.as_view(), name='account_profile_edit_personal'),
    path('profile/edit/avatar/', UserProfileAvatarUpdateView.as_view(), name='account_profile_edit_avatar')
]
