# from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
# from django.urls import reverse
# from django.views.generic.base import TemplateView
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.shortcuts import render, get_object_or_404

# from django.http import HttpResponse

# from .forms import UserProfileForm, UserForm
from .models import UserProfile

from visionary_web.apps.marketplace.views.mixins import MarketplacePermissionMixin


class UserProfileView(LoginRequiredMixin, DetailView):
    template_name = 'profiles/account.html'

    def get_object(self):
        return get_object_or_404(UserProfile, user=self.request.user)


class UserUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'profiles/profile_edit.html'
    model = User
    fields = ['first_name', 'last_name']
    success_url = '/profiles/my_profile/'
    success_message = 'Your profile was updated successfully!'

    def get_object(self):
        return self.request.user


class UserProfileUpdateView(LoginRequiredMixin, MarketplacePermissionMixin, SuccessMessageMixin, UpdateView):
    template_name = 'profiles/profile_edit.html'
    model = UserProfile
    fields = ['paypal_address']
    success_url = '/profiles/my_profile/'
    success_message = 'Your profile was updated successfully!'

    # def form_valid(self, form):
    #     print(form.instance.server)
    #     return super(UserProfileUpdateView, self).form_valid(form)

    def get_object(self):
        return UserProfile.objects.get(user=self.request.user)


class UserProfileAvatarUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'profiles/profile_avatar_update.html'
    model = UserProfile
    fields = ['avatar']
    success_url = '/account/profile/'
    success_message = 'Your avatar was updated successfully!'

    def get_object(self):
        return UserProfile.objects.get(user=self.request.user)


class PublicProfileView(DetailView):
    template_name = 'profiles/public_profile.html'
    model = UserProfile

    def get_object(self):
        return get_object_or_404(UserProfile, user__username=self.kwargs['username'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # try:
        #     marketplace_user = MarketplaceUser.objects.get(user=self.object.user)
        # except MarketplaceUser.DoesNotExist:
        #     marketplace_user = None

        # if marketplace_user is not None:
        #     context["profile_user_is_marketplace_user"] = True
        # else:
        #     context["profile_user_is_marketplace_user"] = False

        # print(context["profile_user_is_marketplace_user"])

        # try:
        #     marketplace_manager = MarketplaceManager.objects.get(user=self.request.user)
        # except MarketplaceManager.DoesNotExist:
        #     marketplace_manager = None

        # if marketplace_manager is not None:
        #     context["request_user_is_marketplace_manager"] = True
        # else:
        #     context["request_user_is_marketplace_manager"] = False

        # print(context["request_user_is_marketplace_manager"])

        return context
