# Generated by Django 2.0.6 on 2019-01-16 18:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portfolio', '0003_auto_20190116_1845'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='youtube_video',
            field=models.URLField(blank=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='short_description',
            field=models.CharField(max_length=200),
        ),
    ]
