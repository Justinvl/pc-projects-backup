from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy

from .models import Project, ImageModel
from .forms import ProjectForm


class ProjectListView(ListView):
    template_name = 'portfolio/project_list.html'
    model = Project


class ProjectDetailView(DetailView):
    template_name = 'portfolio/project_single.html'
    model = Project


class ProjectCreateView(PermissionRequiredMixin, SuccessMessageMixin, CreateView):
    template_name = 'portfolio/project_forms.html'
    form_class = ProjectForm
    permission_required = 'portfolio.manage_projects'
    success_url = '/portfolio/'
    success_message = 'Project was created successfully!'


class ProjectUpdateView(PermissionRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'portfolio/project_forms.html'
    model = Project
    form_class = ProjectForm
    permission_required = 'portfolio.manage_projects'
    success_url = '/portfolio/'
    success_message = "Project was updated successfully!"

    def get_form_kwargs(self):
        kwargs = super(ProjectUpdateView, self).get_form_kwargs()
        kwargs.update({'thumbnail_required': False})
        return kwargs


class ProjectDeleteView(PermissionRequiredMixin, SuccessMessageMixin, DeleteView):
    template_name = 'portfolio/project_delete.html'
    model = Project
    success_url = '/portfolio/'
    success_message = "Project was deleted successfully!"
    permission_required = 'portfolio.manage_projects'

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(ProjectDeleteView, self).delete(request, *args, **kwargs)


class ProjectSlugMixin:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["project_slug"] = self.kwargs['slug']
        return context


class ProjectImagesSuccessUrlMixin:
    def get_success_url(self):
        slug = self.kwargs['slug']
        return reverse_lazy('portfolio_project_images_view', kwargs={'slug': slug})


class ProjectImagesListView(PermissionRequiredMixin, ProjectSlugMixin, ListView):
    template_name = 'portfolio/project_images_view.html'
    model = ImageModel
    permission_required = 'portfolio.manage_projects'

    def get_queryset(self):
        slug = self.kwargs['slug']
        try:
            qs = ImageModel.objects.filter(project__slug=slug)
            return qs
        except ImageModel.DoesNotExist:
            return ImageModel.objects.none()


class ProjectImagesCreateView(PermissionRequiredMixin, SuccessMessageMixin, ProjectSlugMixin, ProjectImagesSuccessUrlMixin, CreateView):
    template_name = 'portfolio/project_images_add.html'
    model = ImageModel
    fields = ['image']
    permission_required = 'portfolio.manage_projects'
    success_message = 'Image was added to the Project successfully!'

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.project = Project.objects.get(slug=self.kwargs['slug'])
        self.object.save()
        return super().form_valid(form)


class ProjectImagesDeleteView(PermissionRequiredMixin, SuccessMessageMixin, ProjectSlugMixin, ProjectImagesSuccessUrlMixin, DeleteView):
    template_name = 'portfolio/project_images_delete.html'
    model = ImageModel
    success_message = "Image was removed successfully!"
    permission_required = 'portfolio.manage_projects'

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(ProjectImagesDeleteView, self).delete(request, *args, **kwargs)
