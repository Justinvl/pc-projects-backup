from django.urls import path
from .views import (ProjectListView,
                    ProjectCreateView,
                    ProjectUpdateView,
                    ProjectDeleteView,
                    ProjectDetailView,
                    ProjectImagesListView,
                    ProjectImagesCreateView,
                    ProjectImagesDeleteView)


urlpatterns = [
    path('', ProjectListView.as_view(), name='portfolio'),
    path('add/', ProjectCreateView.as_view(), name='portfolio_project_add'),
    path('<slug:slug>/', ProjectDetailView.as_view(), name='portfolio_project_view'),
    path('<slug:slug>/update/', ProjectUpdateView.as_view(), name='portfolio_project_update'),
    path('<slug:slug>/delete/', ProjectDeleteView.as_view(), name='portfolio_project_delete'),
    path('<slug:slug>/images/', ProjectImagesListView.as_view(), name='portfolio_project_images_view'),
    path('<slug:slug>/images/add/', ProjectImagesCreateView.as_view(), name='portfolio_project_images_add'),
    path('<slug:slug>/images/<int:pk>/delete/', ProjectImagesDeleteView.as_view(), name='portfolio_project_images_delete')
]
