from django import forms
from .models import Project


class ProjectForm(forms.ModelForm):
    thumbnail = forms.ImageField(widget=forms.FileInput)

    def __init__(self, *args, **kwargs):
        if 'thumbnail_required' in kwargs:
            self.thumbnail_required = kwargs.pop('thumbnail_required')
        else:
            self.thumbnail_required = True
        super(ProjectForm, self).__init__(*args, **kwargs)
        self.fields['thumbnail'].required = self.thumbnail_required

    class Meta:
        model = Project
        fields = ['title', 'author', 'short_description', 'description', 'project_download', 'sketchfab', 'thumbnail']
