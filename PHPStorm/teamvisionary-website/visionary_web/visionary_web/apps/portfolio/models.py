from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.template.defaultfilters import slugify


class Project(models.Model):
    author = models.CharField(max_length=100)
    title = models.CharField(max_length=50)
    slug = models.SlugField(unique=True, blank=True)
    short_description = models.CharField(max_length=200)
    description = models.TextField()
    project_download = models.URLField(blank=True)
    sketchfab = models.URLField(blank=True)
    youtube_video = models.URLField(blank=True)
    thumbnail = models.ImageField(upload_to="portfolio/")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return "/portfolio/%s/" % self.slug

    class Meta:
        permissions = (
            ("manage_projects", "Can manage the portfolio projects"),
        )
        unique_together = ("author", "title")


@receiver(pre_save, sender=Project)
def pre_save_product(sender, instance, *args, **kwargs):
    instance.slug = slugify(instance.title + '-' + instance.author)


class ImageModel(models.Model):
    project = models.ForeignKey(Project, related_name="images", on_delete=models.CASCADE)
    image = models.ImageField(upload_to="portfolio/")
