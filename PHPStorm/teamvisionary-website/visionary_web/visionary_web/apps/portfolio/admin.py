from django.contrib import admin
from .models import Project, ImageModel


admin.site.register(Project)
admin.site.register(ImageModel)
