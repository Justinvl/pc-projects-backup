from django.contrib.sitemaps import Sitemap
from visionary_web.apps.portfolio.models import Project
from django.urls import reverse


class StaticSitemap(Sitemap):
    changefreq = "weekly"
    
    def items(self):
       # Return list of url names for views to include in sitemap
       return ['index', 'contact', 'apply']
    
    def location(self, item):
        return reverse(item)


class PortfolioProjectSitemap(Sitemap):
    changefreq = "weekly"

    def items(self):
        return Project.objects.all()
