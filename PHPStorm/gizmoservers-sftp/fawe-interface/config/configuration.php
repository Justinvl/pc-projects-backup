<?php

class Config {
    private static $info = array();
    public static function set($key, $value) {
        self::$info[$key] = $value;
    }
    public static function get($key) {
        return self::$info[$key];
    }
    public static function getAll() {
        return self::$info;
    }
}

########################################################################################################################
$config = array();                                                                                                     #
                                                                                                                       #
//DO NOT EDIT ABOVE THIS                                                                                               #
                                                                                                                       #
# Edit below this ######################################################################################################
$config['title'] = 'FAWE Schematic Interface'                              ; # Title of the web page                    #
# ============== # Styles: 8bit, dark, default, shadow, phanaticd, orange, green, clean, grey, frame. # ============== #
$config['style'] = "8bit"                                                 ; # The style to use (see styles folder)     #
//If you like a specific style: $config['style'] = 'default'                                                           #
                                                                                                                       #
$config['navtitle'] = 'Clipboard Upload:'							      ; # The text in the navbar				   #
$config['header1'] = 'FAWE Schematic Interface'						  ; # The big text on the web page			   #
$config['serverip'] = 'build.teamvisionary.net'									  ;	# The server IP to display on the web page #
$config['home']  = 'https://teamvisionary.net' ;            # The website for the home link            #
                                                                                                                       #
# e.g. array('192.168.1.10')                                                                                           #
$config['required_ips']   = array()                                       ; # To authorize all servers use array()     #
                                                                                                                       #
$config['size']  = 1500000000                                               ; # Max file size                            #
                                                                                                                       #
$config['allow-delete']  = false                                           ; # Allow file deletion requests             #
########################################################################################################################

// DO NOT EDIT BELOW THIS
foreach($config as $var => $val) {
    Config::set($var, $val);
}
unset($config);
?>
