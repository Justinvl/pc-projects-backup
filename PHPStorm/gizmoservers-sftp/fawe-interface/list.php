<?php
require "config/configuration.php";
$ips = $schematic_settings = Config::get('required_ips');
if (count($ips) > 0 && !in_array($_SERVER['REMOTE_ADDR'], $ips)) {
    header("HTTP/1.1 406 Not Acceptable");
    exit("Bad address");
}
if (strlen($_SERVER['QUERY_STRING']) != 36 && strlen($_SERVER['QUERY_STRING']) != 32) {
  header("HTTP/1.1 413 Request Entity Too Large");
  exit("Bad ID");
}
if (preg_match('/^\{?[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}\}?$/', $_SERVER['QUERY_STRING']) == 0) {
  header("HTTP/1.1 401 Unauthorized");
  exit();
}
$dir = "saves/" . $_SERVER['QUERY_STRING'];
$return_array = array();

if(is_dir($dir)){
    if($dh = opendir($dir)){
        while(($file = readdir($dh)) != false){
            if($file != "." and $file != ".."){
                $return_array[] = $file;
            }
        }
    }
    echo json_encode($return_array, true);
}
?>