<?php

class Category
{
    private $id;
    private $collection;
    private $name;
    private $image;

    public function __construct($id, $collection, $name, $image)
    {
        $this->id = $id;
        $this->collection = $collection;
        $this->name = $name;
        $this->image = $image;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCollection()
    {
        return $this->collection;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getImage()
    {
        return ($this->image == null ? 'default.png' : $this->image);
    }

    public static function all($collection)
    {
        $database = new Database();
        $connection = $database->getConnection();
        $statement = $connection->prepare("SELECT * FROM category WHERE collection=?");
        $statement->execute(array($collection));
        $categories = [];

        while($data = $statement->fetch())
        {
            array_push($categories, new Category($data['id'], $data['collection'], $data['name'], $data['image']));
        }

        return $categories;
    }

    public static function find($id)
    {
        $database = new Database();
        $connection = $database->getConnection();
        $statement = $connection->prepare("SELECT * FROM category WHERE id=?");
        $statement->execute(array($id));

        while($data = $statement->fetch())
        {
            return new Category($data['id'], $data['collection'], $data['name'], $data['image']);
        }

        return null;
    }
}