<?php

class Faq
{
    private $id;
    private $question;
    private $answer;
    private $categoryID;

    public function __construct($id, $question, $answer, $categoryID = 0)
    {
        $this->id = $id;
        $this->question = $question;
        $this->answer = $answer;
        $this->categoryID = $categoryID;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getQuestion()
    {
        return $this->question;
    }

    public function getAnswer()
    {
        return $this->answer;
    }

    public static function getCateGoryName($categoryID){
        $database = new Database();
        $connection = $database->getConnection();
        $statement = $connection->prepare("SELECT * FROM faq_category WHERE id = " . $categoryID);
        $statement->execute();

        return $statement->fetch()['name'];
    }

    public static function getByCategory($categoryID){
        $database = new Database();
        $connection = $database->getConnection();
        $statement = $connection->prepare("SELECT * FROM faq WHERE category = " . $categoryID);
        $statement->execute();
        $faqs = [];

        while($data = $statement->fetch())
        {
            array_push($faqs, new Faq($data['id'], $data['question'], $data['answer'], $data['category']));
        }

        return $faqs;
    }

    public static function all()
    {
        $database = new Database();
        $connection = $database->getConnection();
        $statement = $connection->prepare("SELECT * FROM faq");
        $statement->execute();
        $faqs = [];

        while($data = $statement->fetch())
        {
            array_push($faqs, new Faq($data['id'], $data['question'], $data['answer']));
        }

        return $faqs;
    }
}