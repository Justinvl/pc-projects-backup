<?php

class Collection
{
    private $id;
    private $name;
    private $showInHeader;

    public function __construct($id, $name, $showInHeader)
    {
        $this->id = $id;
        $this->name = $name;
        $this->showInHeader = $showInHeader;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getShowInHeader()
    {
        return $this->showInHeader;
    }

    public function getName()
    {
        return $this->name;
    }

    public static function all()
    {
        $database = new Database();
        $connection = $database->getConnection();
        $statement = $connection->prepare("SELECT * FROM collection");
        $statement->execute();
        $collections = [];

        while($data = $statement->fetch())
        {
            array_push($collections, new Collection($data['id'], $data['name'], $data['show_in_header']));
        }

        return $collections;
    }

    public static function find($id)
    {
        $database = new Database();
        $connection = $database->getConnection();
        $statement = $connection->prepare("SELECT * FROM collection WHERE id=?");
        $statement->execute(array($id));

        while($data = $statement->fetch())
        {
            return new Collection($data['id'], $data['name'], $data['show_in_header']);
        }

        return null;
    }
}