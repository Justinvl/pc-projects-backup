<?php

class Database
{

    private $host;
    private $database;
    private $username;
    private $password;
    private $connection;

    public function __construct()
    {
        $this->host = Config::$dbHost;
        $this->database = Config::$db;
        $this->username = Config::$dbUsername;
        $this->password = Config::$dbPassword;

        try
        {
            $this->connection = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->database, $this->username, $this->password);

            $this->connection->query('CREATE TABLE IF NOT EXISTS collection (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255) NOT NULL, show_in_header BIT DEFAULT \'0\')')->execute();
            $this->connection->query('CREATE TABLE IF NOT EXISTS category (id INT AUTO_INCREMENT PRIMARY KEY, collection INT NOT NULL, name VARCHAR(255) NOT NULL, image TEXT NOT NULL)')->execute();
            $this->connection->query('CREATE TABLE IF NOT EXISTS product (id INT AUTO_INCREMENT PRIMARY KEY, category INT NOT NULL, name VARCHAR(255) NOT NULL, price VARCHAR(255) NOT NULL, ram VARCHAR(255) NOT NULL, players INT NOT NULL, image TEXT NOT NULL, link TEXT NOT NULL)')->execute();
            $this->connection->query('CREATE TABLE IF NOT EXISTS faq (id INT AUTO_INCREMENT PRIMARY KEY, question TEXT NOT NULL, answer TEXT NOT NULL)')->execute();
        }
        catch (PDOException $ex)
        {
            print "Error: " . $ex->getMessage();
            die();
        }
    }

    public function getConnection()
    {
        return $this->connection;
    }
}