<?php

class Product
{
    private $id;
    private $category;
    private $name;
    private $price;
    private $ram;
    private $players;
    private $image;
    private $link;

    public function __construct($id, $category, $name, $price, $ram, $players, $image, $link)
    {
        $this->id = $id;
        $this->category = $category;
        $this->name = $name;
        $this->price = $price;
        $this->ram = $ram;
        $this->players = $players;
        $this->image = $image;
        $this->link = $link;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getRam()
    {
        return $this->ram;
    }

    public function getPlayers()
    {
        return $this->players;
    }

    public function getImage()
    {
        return ($this->image == null ? 'cow.png' : $this->image);
    }

    public function getLink()
    {
        return $this->link;
    }

    public static function all($category)
    {
        $database = new Database();
        $connection = $database->getConnection();
        $statement = $connection->prepare("SELECT * FROM product WHERE category=?");
        $statement->execute(array($category));
        $products = [];

        while($data = $statement->fetch())
        {
            array_push($products, new Product($data['id'], $data['category'], $data['name'], $data['price'], $data['ram'], $data['players'], $data['image'], $data['link']));
        }

        return $products;
    }

    public static function find($id)
    {
        $database = new Database();
        $connection = $database->getConnection();
        $statement = $connection->prepare("SELECT * FROM product WHERE id='?'");
        $statement->execute(array($id));

        while($data = $statement->fetch())
        {
            return new Product($data['id'], $data['category'], $data['name'], $data['price'], $data['ram'], $data['players'], $data['image'], $data['link']);
        }

        return false;
    }
}