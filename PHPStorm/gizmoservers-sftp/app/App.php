<?php

class App
{

    private $controller = '';
    private $method = 'index';
    private $args = [];

    public function __construct()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = explode('/', $url);
        $url = array_splice($url, 1);
        $url = str_replace("-", "_", $url);

        if(isset($url[0]))
        {
            $this->controller = $url[0];
        }

        switch($this->controller)
        {
            default:
                require('controller/controllers/HomeController.php');
                $this->controller = new HomeController();
        }

        $name = $url[0];

        if(isset($url[0]) && $url[0] != "")
        {
            if ($url[0] == 'index.php'){
                $name = 'index';
            }

            if(method_exists($this->controller, $name))
            {
                $this->method = $name;
            }else{
                $this->method = 'four_o_four';
            }
        }

        $can_url = ROOT_URL.$this->method;

        if ($this->method == 'index'){
            $can_url = ROOT_URL;
        }

        define("CAN_URL", $can_url);

        $url = array_splice($url, 2);

        if(isset($url[0]))
        {
            $this->args = $url;
        }

        $this->controller->{$this->method}($this->args);
    }

}