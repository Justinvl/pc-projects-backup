<?php

class HomeController extends Controller
{

    public function index($args = [])
    {
        $view = new View('index',
            "Cheap Minecraft Server Hosting Available Here – Gizmoservers ",
            "Rent Minecraft server, Minecraft Server Hosting, Cheap MCPE Server Hosting, Best Minecraft Pocket Edition Server Hosting, Best Minecraft Server Host, Minecraft Server, Modded Minecraft server host",
            "We provide the best minecraft server hosting. It does not matter if you want a Spigot, Bukkit or Modpack server. We got it. Don’t waste more time, Begin your own minecraft Server at Gizmoservers .");
        $view->render();
    }

    public function about($args = [])
    {
        $view = new View('about',
            "About us – Gizmoservers ",
            "Rent Minecraft server, Minecraft Server Hosting, Cheap MCPE Server Hosting, Best Minecraft Pocket Edition Server Hosting, Best Minecraft Server Host, Minecraft Server, Modded Minecraft server host",
            "We provide the best minecraft server hosting. It does not matter if you want a Spigot, Bukkit or Modpack server. We got it. Don’t waste more time, Begin your own minecraft Server at Gizmoservers .");
        $view->render();
    }

    public function four_o_four($args = [])
    {
        $view = new View('404', "Can't find what you're looking for", "Can't find what you looking for. Try something else instead. Make sure you typed the wrong URL!");
        $view->render();
    }

    public function frequently_asked_questions($args = [])
    {
        $view = new View('frequently-asked-questions',
            "Frequently Asked Questions From The Experts – Gizmoservers ",
            "Minecraft Servers Specialist, Minecraft Servers Experts, experts of minecraft servers",
            "If you have questions about certain subjects, then you are at the right place. You can find here the most frequently asked questions.");
        $view->render();
    }

    public function hire_a_minecraft_server_specialist($args = [])
    {
        $view = new View('hire-a-minecraft-server-specialist',
            "Hire Minecraft Servers Specialist Today – Gizmoservers ",
            "Hire Minecraft Servers Specialist, Minecraft Servers Specialist, entire server specialist",
            "Select your package according to your budget and additionally we will create an entire server for you but don’t forget these additionally services depend on your package.");
        $view->render();
    }

    public function become_a_partner($args = [])
    {
        $view = new View('become-a-partner',
            "Become A Partner Of Gizmoservers ",
            "Partner of Gizmoservers, partners of minecraft servers, MCPE host partners",
        "It’s Time Become a Partner of Gizmoservers just read the following details and become partners. Just come and take a look."
        );
        $view->render();
    }

    public function terms($args = [])
    {
        $view = new View('terms', "Terms of service");
        $view->render();
    }

//    public function collection($args = [])
//    {
//        $view = new View('collection', $args);
//        $view->render();
//    }

    public function jobs($args = [])
    {
        $view = new View('jobs',
            "Golden Job Opportunities – Gizmoservers",
            "web developer jobs, sales and marketing associate jobs",
            "Gizmoservers is always ready to help you, now we introduce the golden career opportunities. If you have skills you must apply without hesitation.", $args);
        $view->render();
    }

    public function minecraft_ssd_servers($args = [])
    {
        $view = new View('minecraft-ssd-servers',
            "Best Quality & High Performance Minecraft SSD Server – Gizmoservers",
            "Minecraft Servers, Minecraft SSD Servers, High Performance Minecraft SSD Server.",
            "Large collections of Minecraft SSD Servers available at Gizmoservers Just visit our website and choose the best minecraft server according to your need.");
        $view->render();
    }
}