<?php
/**
 * Created by PhpStorm.
 * User: justi
 * Date: 5-11-2018
 * Time: 11:33
 */

/*
 * - Full Name
 * - Business Email
 * - Youtube URL if they have it
 * - Twitch URL
 * - Other URL's
 * - Reason why partnership
 * - What can you offer us
 *
 *
 */

?>

<section id="sub-header">
    <div class="container relative">
        <div class="row">
            <div class="col-xs-12 center-xs">
                <div class="breadcrumb">
                    <div class="breadcrumb-item">
                        GizmoServers
                    </div>
                    <div class="breadcrumb-item">
                        Partnership
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="sub-header-title">
                    <h1>Become a partner</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="partnership">
    <div id="topper">
        <div class="container">
            <div class="row specialist-faq">
                <div class="col-xs-12">
                    <button class='accordion-open'>
                        Who are we looking for?
                    </button>
                    <div class='panel-open'>
                        <p>
                            Do you have a stable community, but looking to grow? Are you a Youtuber with a fair amount of subscribers,
                            looking for a server to host your projects on? Are you a plugin developer looking for test servers?
                            Are you a twitch streamer with a large community, looking for a server to have fun on? We got you!
                        </p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <button class='accordion-open'>
                        What do we get in return as a partner?
                    </button>
                    <div class='panel-open'>
                        <p>
                            GizmoServers can offer you a wide, stable base for your servers.
                            With our expertise we can ensure a stable future for you and your community, catering our solutions towards you, not only limited to Minecraft hosting.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <button class='accordion-open'>
                        What do we expect from you?
                    </button>
                    <div class='panel-open'>
                        <p>
                            We are not going to demand you splatter your channel or our site with our logo.
                            Simple links and friendly reminders of our help towards your community is all we need. We'll supply you with the right material.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="partners-slider">

</section>
<section id="partner-apply">
    <div class="container">
        <form id="partner-apply-form">
            <div class="row">
                <div class="col-xs-6">
                    <div class="input-group">
                        <label for="partner-name">Full Name</label>
                        <input type="text" class="form-input partner-name apply-required" name="name" placeholder="John Smitt">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="input-group">
                        <label for="partner-email">Business Email</label>
                        <input type="email" class="form-input partner-email apply-required" name="name" placeholder="info@example.com">
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="input-group">
                        <label>What are you?</label>
                        <select id="partner-job-select">
                            <option class="select-item" disabled selected>Choose what best describes you...</option>
                            <option class="select-item" value="influencer">Influencer (Youtube)</option>
                            <option class="select-item" value="streamer">Streamer (Youtube & Twitch)</option>
                            <option class="select-item" value="streamer_twitch">Twitch Streamer</option>
                            <option class="select-item" value="plugin_developer">Plugin Developer</option>
                            <option class="select-item" value="server_owner">Server Owner</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div id="slide_streamer" class="slide_item">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="input-group">
                                    <label>Full Name</label>
                                    <input type="text" class="form-input" name="name" placeholder="John Smitt">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="input-group">
                                    <label>Business Email</label>
                                    <input type="email" class="form-input" name="name" placeholder="info@example.com">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <label>What are you?</label>
                                    <select id="select">
                                        <option class="select-item" disabled>Choose what best describes you...</option>
                                        <option class="select-item" value="influencer">Influencer (Youtube)</option>
                                        <option class="select-item" value="streamer">Streamer (Youtube & Twitch)</option>
                                        <option class="select-item" value="streamer_twitch">Twitch Streamer</option>
                                        <option class="select-item" value="plugin_developer">Plugin Developer</option>
                                        <option class="select-item" value="server_owner">Server Owner</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div id="slide_influencer" class="slide_item">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="input-group">
                                    <label>Full Name</label>
                                    <input type="text" class="form-input" name="name" placeholder="John Smitt">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="input-group">
                                    <label>Business Email</label>
                                    <input type="email" class="form-input" name="name" placeholder="info@example.com">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="input-group">
                                    <label>What are you?</label>
                                    <select id="job-select">
                                        <option class="select-item" disabled>Choose what best describes you...</option>
                                        <option class="select-item" value="influencer">Influencer (Youtube)</option>
                                        <option class="select-item" value="streamer">Streamer (Youtube & Twitch)</option>
                                        <option class="select-item" value="streamer_twitch">Twitch Streamer</option>
                                        <option class="select-item" value="plugin_developer">Plugin Developer</option>
                                        <option class="select-item" value="server_owner">Server Owner</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <a href="#" target="_blank">
                        <button id="object-btn" class="fullw partner-form-submit disabled">Send Application</button>
                    </a>
                </div>
            </div>
        </form>
    </div>
</section>
