<?php
/**
 * Created by PhpStorm.
 * User: justi
 * Date: 7-11-2018
 * Time: 15:40
 */

$no_message = '
<div class="">
    <div class="card">
        <div class="error-card">
            <!--<div class="error-card-title">-->

            <!--</div>-->
            <div class="error-card-content">
                <div class="container">
                    <div class="row" style="margin-right: 0">
                        <div class="col-xs-4">
                            <i class="fas fa-exclamation fa-5x"></i>
                        </div>
                        <div class="col-xs-8">
                            Currently there are no vacancies.<br>You can send your CV and motivation to
                            <a href="mailto:support@gizmoservers.com">support@gizmoservers.com</a>.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
';
?>

<section id="sub-header">
    <div class="container relative">
        <div class="row">
            <div class="col-xs-12 center-xs">
                <div class="breadcrumb">
                    <div class="breadcrumb-item">
                        GizmoServers
                    </div>
                    <div class="breadcrumb-item">
                        Jobs
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="sub-header-title">
                    <h1>Golden Job Opportunities</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="jobs">
    <div id="topper">

<!--        <div class="row" style="margin-right: 0">-->
<!--            <div class="col-xs-12 center-xs">-->
<!--                <h1>Gizmoservers Jobs</h1>-->
<!--                <p>Gizmoservers is one of the cheapest Minecraft Server Hosting companies out there. <br>-->
<!--                    We're a team of people driven by creativity and the love for making minecraft servers.<br>-->
<!--                    We're open to new talent and are always interested to look at your resume.<br>-->
<!--                    If there are open positions, do not hesitate to apply.-->
<!--                </p>-->
<!--            </div>-->
<!--        </div>-->
        <div class="row content" style="margin-right: 0">
            <div class="col-lg-6 col-xs-12">
                <h2 style="font-size: 24px;">Full Stack Developer</h2>
                <?= $no_message; ?>
            </div>
            <div class="col-lg-6 col-xs-12">
                <h2 style="font-size: 24px;">Sales / Marketing Associate</h2>
                <?= $no_message; ?>
            </div>
        </div>



    </div>
</section>
