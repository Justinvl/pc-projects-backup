<?php
//$args = self::getArgs();

//if(isset($args[0]))
//{
//    $category = $args[0];
//var_dump(self::getFile);
//    $category = Category::find(self::getFile);
//var_dump($category);
//    if($category == null)
//    {
//        header("Location: " . ROOT . "?error=category_not_found");
//    }
//}
//else
//{
//    header("Location: " . ROOT . "home/index");
//}
?>
<section id="sub-header">
    <div class="container relative">
        <div class="row">
            <div class="col-xs-12 center-xs">
                <div class="breadcrumb">
                    <div class="breadcrumb-item">
                        GizmoServers
                    </div>
                    <div class="breadcrumb-item">
                        Minecraft SSD Servers
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="sub-header-title">
                    <h1>Minecraft SSD Servers</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="category">
    <div class="container">
<!--        <div id="topper">-->
<!--            <h1 id="title">-->
<!--                --><?php //echo $category->getName();?>
<!--            </h1>-->
<!--            <p id="subtitle">-->
<!--                You can almost get started with your server.<br>-->
<!--                Choose your package below-->
<!--            </p>-->
<!--            <div class="object-btn-grp">-->
<!--                <p class="choose-slider">Or choose our slider to have more control</p>-->
<!--                <a href="https://billing.gizmoservers.com/cart.php" target="_blank"><button id="object-btn" class="slider">GO TO SLIDER</button></a>-->
<!--            </div>-->
<!--        </div>-->
        <div id="objects">
            <div class="row">
                <?php
                    $products = Product::all($category->getId());
                    foreach($products as $product)
                    {
                        $id = $product->getId();
                        $name = $product->getName();
                        $price = $product->getPrice();
                        $players = $product->getPlayers();
                        $ram = $product->getRam();
                        $image = $product->getImage();
                        $link = $product->getLink();
                    ?>
    <!--                <a href='$link'>-->
                        <div class='object animated zoomIn center-xs col-lg-3'>
                            <img src='<?php echo ROOT ?>images/products/<?php echo $image?>'>
                            <h3 class='product-title'>
                                <?php echo $name; ?>
                            </h3>
                            <p><b><?php echo $players; ?></b> Players</p>
                            <p><b><?php echo $ram; ?></b>  RAM</p>
                            <p><b>Automatic</b>  Backups</p>
                            <p><b>Free</b> 24/7 Support</p>
                            <div class="object-btn-grp">
                                <h4><?php echo $price; ?></h4>
                                <h5>Per Month</h5>
                                <a href="<?php echo $link; ?>" target="_blank"><button id="object-btn">CHOOSE</button></a>
                            </div>
                        </div>
    <!--                </a>-->
                    <?php }
                ?>
            </div>
        </div>
        <div class="row category-faq">
            <div class="col-xs-12">
                <div class="accordion-item">
                    <button class='accordion'>
                        I want more RAM on my server. How can we make that happen?
                    </button>
                    <div class='panel'>
                        <p>
                            Great question. The point with those packages, is to have already pre-installed servers up and running.<br>
                            If you want more RAM than our highest package? Just hit us up with an email or open a ticket so we can arrange something.<br>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="accordion-item">
                    <button class='accordion'>
                        Are those servers SSD or HHD?
                    </button>
                    <div class='panel'>
                        <p>
                            Great question. All packages listed here are SSD. We currently do not hire HHD packages.. YET!<br>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 center-xs">
                <a target="_blank"  href="<?= ROOT_URL ?>faq">
                    <button id="view-more-faq" class="blue rounded">
                        <h2>View more "Frequently Asked Questions"</h2>
                    </button>
                </a>
            </div>
        </div>
    </div>
</section>