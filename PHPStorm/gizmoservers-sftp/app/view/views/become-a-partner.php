<?php
/**
 * Created by PhpStorm.
 * User: justi
 * Date: 5-11-2018
 * Time: 11:33
 */

/*
 * - Full Name
 * - Business Email
 * - Youtube URL if they have it
 * - Twitch URL
 * - Other URL's
 * - Reason why partnership
 * - What can you offer us
 *
 *
 */

?>

<section id="sub-header">
    <div class="container relative">
        <div class="row">
            <div class="col-xs-12 center-xs">
                <div class="breadcrumb">
                    <div class="breadcrumb-item">
                        Gizmoservers
                    </div>
                    <div class="breadcrumb-item">
                        Partnership
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="sub-header-title">
                    <h1>Become a partner</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="partnership">
    <div id="topper">
        <div class="container">
            <div class="row specialist-faq">
                <div class="col-xs-12">
                    <button class='accordion-open'>
                        Who are we looking for?
                    </button>
                    <div class='panel-open'>
                        <p>
                            Do you have a stable community, but looking to grow? Are you a Youtuber with a fair amount of subscribers,
                            looking for a server to host your projects on? Are you a plugin developer looking for test servers?
                            Are you a twitch streamer with a large community, looking for a server to have fun on?
                        </p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <button class='accordion-open'>
                        Can I earn money?
                    </button>
                    <div class='panel-open'>
                        <p>
                            Gizmoservers can offer you a wide, stable base for your servers.
                            With our expertise we can ensure a stable future for you and your community, catering our solutions towards you, not only limited to Minecraft hosting.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12">
                    <button class='accordion-open'>
                        What do we expect from you?
                    </button>
                    <div class='panel-open'>
                        <p>
                            We are not going to demand you splatter your channel or site with out logo.
                            Simple links and friendly reminders of our help towards your community are all we need. We'll supply you with the right material.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--<section id="partner-requirements">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-xs-12 center-xs">-->
<!--                <table class="partner-requirement-table">-->
<!--                    <tr>-->
<!--                        <th></th>-->
<!--                        <th>-->
<!--                            <img src="--><?//= ROOT; ?><!--images/tier1.png">-->
<!--                            <ul>-->
<!--                                <li>10.000 Subscribers</li>-->
<!--                                <li>5.000 Twitch Followers</li>-->
<!--                            </ul>-->
<!--                        </th>-->
<!--                        <th>-->
<!--                            <img src="--><?//= ROOT; ?><!--images/tier2.png">-->
<!--                        </th>-->
<!--                        <th>-->
<!--                            <img src="--><?//= ROOT; ?><!--images/tier3.png">-->
<!--                        </th>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>Minecraft Server</td>-->
<!--                        <td>1x - 1GB</td>-->
<!--                        <td>1x - 2GB</td>-->
<!--                        <td>2x - 2GB</td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>Social Media DSSD</td>-->
<!--                        <td><i class="fas fa-check"></i></td>-->
<!--                        <td><i class="fas fa-check"></i></td>-->
<!--                        <td><i class="fas fa-check"></i></td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>Social Media DSSD</td>-->
<!--                        <td><i class="fas fa-check"></i></td>-->
<!--                        <td><i class="fas fa-check"></i></td>-->
<!--                        <td><i class="fas fa-check"></i></td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>Social Media DSSD</td>-->
<!--                        <td><i class="fas fa-check"></i></td>-->
<!--                        <td><i class="fas fa-check"></i></td>-->
<!--                        <td><i class="fas fa-check"></i></td>-->
<!--                    </tr>-->
<!--                    <tr>-->
<!--                        <td>Social Media DSSD</td>-->
<!--                        <td><i class="fas fa-check"></i></td>-->
<!--                        <td><i class="fas fa-check"></i></td>-->
<!--                        <td><i class="fas fa-check"></i></td>-->
<!--                    </tr>-->
<!--                </table>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->
<section id="partner-apply-form">
    <div class="container">
        <form id="apply-form">
            <div class="row">
                <div class="col-xs-6">
                    <div class="gizmo-group">
                        <label>Full Name</label>
                        <input type="text" class="form-input partner-name" name="name" placeholder="John Smitt">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="gizmo-group">
                        <label>Business Email</label>
                        <input type="email" class="form-input partner-email" name="name" placeholder="info@example.com">
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="gizmo-group">
                        <label>What are you?</label>
                        <select id="partner-job-select">
                            <option class="select-item" disabled selected>Choose what best describes you...</option>
                            <option class="select-item" value="influencer">Influencer (Youtube)</option>
                            <option class="select-item" value="streamer">Streamer (Youtube & Twitch)</option>
                            <option class="select-item" value="streamer_twitch">Twitch Streamer</option>
                            <option class="select-item" value="plugin_developer">Plugin Developer</option>
                            <option class="select-item" value="server_owner">Server Owner</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div id="slide_streamer" class="slide_item">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="gizmo-group">
                                    <label>Full Name</label>
                                    <input type="text" class="form-input" name="name" placeholder="John Smitt">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="gizmo-group">
                                    <label>Business Email</label>
                                    <input type="email" class="form-input" name="name" placeholder="info@example.com">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="gizmo-group">
                                    <label>What are you?</label>
                                    <input type="email" class="form-input" name="name" placeholder="info@example.com">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div id="slide_influencer" class="slide_item">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="gizmo-group">
                                    <label>Youtube URL</label>
                                    <input type="url" class="form-input" name="youtube" placeholder="https://www.youtube.com/channel/gizmoservers">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="gizmo-group">
                                    <label>Business Email</label>
                                    <input type="email" class="form-input" name="name" placeholder="info@example.com">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="gizmo-group">
                                    <label>What are you?</label>
                                    <input type="email" class="form-input" name="name" placeholder="info@example.com">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="gizmo-group">
                        <label>Why do you want a partnership?</label>
                        <textarea rows="4" class="form-input"></textarea>
                    </div>
                </div>
                <div class="col-xs-12">
                    <a href="#" target="_blank">
                        <button id="partner-apply-submit" class="fullw disabled white">Send Application</button>
                    </a>
                </div>
            </div>
        </form>
    </div>
</section>
