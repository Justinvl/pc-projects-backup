<section id="sub-header">
    <div class="container relative">
        <div class="row">
            <div class="col-xs-12 center-xs">
                <div class="breadcrumb">
                    <div class="breadcrumb-item">
                        GizmoServers
                    </div>
                    <div class="breadcrumb-item">
                        Frequently Asked Questions
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="sub-header-title">
                    <h1>Frequently Asked Questions</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<!--TOTAL CATEGORIES HARDCODED-->
<?php $total_categories = 2; ?>

<section id="faq">
    <div class="container" style="padding-bottom: 30px;">
<!--        <div id="topper" class="faq-topper no-height">-->
            <div class="row">
                <div class="col-lg-7 col-xs-12">
<!--                    <h2>Quick Navigation</h2>-->
                    <div class="row">
                        <?php for($i=1;$i<=$total_categories;$i++): ?>
                            <?php $name = Faq::getCateGoryName($i); ?>
                            <div class="col-lg-2 col-xs-6">
                                <a href="javascript:void(0)" sub="<?= $name; ?>">
                                    <div class="faq-card">
                                        <i class="fas fa-fingerprint"></i>
                                        <?= $name; ?>
                                    </div>
                                </a>
                            </div>
                        <?php endfor; ?>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="faq-steve">
                        <img src="<?= ROOT; ?>images/3d/5.png">
                    </div>
                </div>
            </div>
<!--        </div>-->
        <?php for($i=1;$i<=$total_categories;$i++): ?>
            <?php $name = Faq::getCateGoryName($i); ?>
            <div id="faq_<?= $name; ?>">
                <h2 class="faq-title"><?= $name; ?></h2>
                <?php $faqs = Faq::getByCategory($i); ?>

                <?php foreach($faqs as $faq): ?>
                    <?php $question = $faq->getQuestion(); ?>
                    <?php $answer = $faq->getAnswer(); ?>

                    <button class='accordion'>
                        <?= $question ?>
                    </button>
                    <div class='panel'>
                        <p><?= $answer; ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endfor; ?>
    </div>
</section>