<?php
/**
 * Created by PhpStorm.
 * User: justi
 * Date: 3-11-2018
 * Time: 23:15
 */

/*
 *
 * Also need to add somewhere Iron Gold and Diamond Packages for fully maintained servers
 * which we will charge 1000 / month for a dedicated server with 1 basic server setup and maintainence, $2500 / month
 * for 3 advanced server setups, website, buycraft, discord, and social media setup and maintainence,
 * and Diamond Tier which is $5000 / month which includes 6 Professional level server setups, website, discord,
 * buycraft, social media, email account, dedicated IP, 6 Professional level builds up to 200x200 is the max size
 * for the builds each, staff management, chargeback mangement, etc.
 *
 * Iron Tier ($100)
 *  - 1 Dedicated Server
 *  - Basic Server Setup
 *  - Server Maintenance
 *
 * Gold Tier ($2500)
 *  - 1 Dedicated Server
 *  - 3 Advanced Server Setup
 *  - Custom Website
 *  - Custom Themed Premium Buycraft
 *  - Discord Setup / Maintenance
 *  - Social Media Setup (Twitter, Facebook, Instagram) / Maintenance
 *  -
 *
 * Diamond Tier ($5000)
 *  - 1 Dedicated Server
 *  - 6 Advanced Server Setup
 *  - 6 Professional level builds (200 x 200)
 *  - Social Media Setup (Twitter, Facebook, Instagram) / Maintenance
 *  - Custom Themed Premium Buycraft
 *  - Custom Website
 *  - Discord Setup / Maintenance
 *  - Staff Managament
 *  - chargeback mangement
 */

?>

<section id="sub-header">
    <div class="container relative">
        <div class="row">
            <div class="col-xs-12 center-xs">
                <div class="breadcrumb">
                    <div class="breadcrumb-item">
                        GizmoServers
                    </div>
                    <div class="breadcrumb-item">
                        Hire A Specialist
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="sub-header-title">
                    <h1>Hire A Specialist</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="hire_a_specialist">
    <div class="container" style="padding-bottom: 30px;">
<!--        <div id="topper">-->
<!--            <div class="row">-->
<!--                <div class="col-lg-4">-->
<!--                    <img class="specialist-steve" src="--><?php //echo ROOT ?><!--images/3d/6.png">-->
<!--                </div>-->
<!--                <div class="col-lg-8">-->
<!--                    <h1>Always wanted to let a team work for you?</h1>-->
<!--                    <p>GizmoServers can offer different variations of packages.-->
<!--                        With those packages we will offer you as a customer to get more out of your service.<br>-->
<!--                        We will create an entire server for you (Depends on the package) and we will make sure it's successful.-->
<!--                    </p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="tiers">-->
            <div class="row">
                <div class="col-lg-4 center-lg">
                    <div class='object animated zoomIn'>
                            <img src="<?php echo ROOT; ?>images/tiers/iron_apple.png" width="150" height="150">
                            <h2 class='product-title'>
                                <span class="iron-tier">Iron</span><br>Specialist
                            </h2>
                            <p><b>Dedicated Server</b> + Setup</p>
                            <p><b>One Advanced</b>  Server Setup</p>
                            <p>
                                <span class="tooltip tooltip-big"><b>Custom Website</b> + Maintenance
                                    <span class="tooltiptext tooltip-right">
                                        We offer a complete website for your server.<br>
                                        We offer the following:<br><Br>
                                        - Xenforo Forum + custom template<br>
                                        - Website with backend and frontend<br>
                                        - Wordpress Website<br>
                                        Ask for more ....<br><br>
                                        We employ various developers who can offer this.<br>
                                        If you want to know more, you can always contact us.
                                    </span>
                                </span>
                            </p>
                            <p>Custom themed <b>Premium Buycraft</b> + Maintenance</p>
                            <p>Two <b>Custom Made Plugins</b></p>
                            <p>
                                <span class="tooltip tooltop-medium"><b>Social Media</b> Setup & Maintenance
                                    <span class="tooltiptext tooltip-right">
                                        This will include the following
                                        social media websites.<br><br>
                                        - Twitter<br>
                                        - Facebook<br>
                                        - Instagram
                                    </span>
                                </span>
                            </p>
                            <div class="object-btn-grp">
                                <h2><?php echo '$1000'; ?></h2>
                                <h3>Per Month</h3>
                                <a href="mailto:support@gizmoservers.com" target="_blank">
                                    <button id="object-btn">I am Interested</button>
                                </a>
                            </div>
                        </div>
                </div>
                <div class="col-lg-4 center-lg">
                    <div class='object animated zoomIn'>
                        <img src="<?php echo ROOT; ?>images/tiers/gold_apple.png" width="120" height="150">
                        <h2 class='product-title'>
                            <span class="gold-tier">Gold</span><br>Specialist
                        </h2>
                        <p><b>Dedicated Server</b> + Setup</p>
                        <p><b>Three Advanced</b>  Server Setup</p>
                        <p>Four <b>Custom Made Plugins</b></p>
                        <p>
                                <span class="tooltip tooltip-big"><b>Custom Website</b> + Maintenance
                                    <span class="tooltiptext tooltip-right">
                                        We offer a complete website for your server.<br>
                                        We offer the following:<br><Br>
                                        - Xenforo Forum + custom template<br>
                                        - Website with backend and frontend<br>
                                        - Wordpress Website<br>
                                        Ask for more ....<br><br>
                                        We employ various developers who can offer this.<br>
                                        If you want to know more, you can always contact us.
                                    </span>
                                </span>
                        </p>
                        <p>Custom themed <b>Premium Buycraft</b> + Maintenance</p>
                        <p>Discord <b>setup & maintenance</b></p>
                        <p>
                                <span class="tooltip tooltop-medium"><b>Social Media</b> Setup & Maintenance
                                    <span class="tooltiptext tooltip-right">
                                        This will include the following
                                        social media websites.<br><br>
                                        - Twitter<br>
                                        - Facebook<br>
                                        - Instagram
                                    </span>
                                </span>
                        </p>
                        <p>Two <b>Professional Builds</b> (100 x 100)</p>
                        <div class="object-btn-grp">
                            <h2><?php echo '$2500'; ?></h2>
                            <h3>Per Month</h3>
                            <a href="mailto:support@gizmoservers.com" target="_blank">
                                <button id="object-btn">I am Interested</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 center-lg">
                    <div class='object animated zoomIn'>
                        <img src="<?php echo ROOT; ?>images/tiers/diamond_apple.png" width="150" height="150">
                        <h2 class='product-title'>
                            <span class="diamond-tier">Diamond</span><br>Specialist
                        </h2>
                        <p><b>Dedicated Server</b> + Setup</p>
                        <p><b>6+ Advanced</b>  Server Setup</p>
                        <p>Six <b>Custom Made Plugins</b></p>
                        <p>
                                <span class="tooltip tooltip-big"><b>Custom Website</b> + Maintenance
                                    <span class="tooltiptext tooltip-right">
                                        We offer a complete website for your server.<br>
                                        We offer the following:<br><Br>
                                        - Xenforo Forum + custom template<br>
                                        - Website with backend and frontend<br>
                                        - Wordpress Website<br>
                                        Ask for more ....<br><br>
                                        We employ various developers who can offer this.<br>
                                        If you want to know more, you can always contact us.
                                    </span>
                                </span>
                        </p>
                        <p>Custom themed <b>Premium Buycraft</b> + Maintenance</p>
                        <p>Discord <b>setup & maintenance</b></p>
                        <p><b>Staff Management</b></p>
                        <p><b>Chargeback Management</b></p>
                        <p>
                                <span class="tooltip tooltop-medium"><b>Social Media</b> Setup & Maintenance
                                    <span class="tooltiptext tooltip-right">
                                        This will include the following
                                        social media websites.<br><br>
                                        - Twitter<br>
                                        - Facebook<br>
                                        - Instagram
                                    </span>
                                </span>
                        </p>
                        <p>Six <b>Professional Builds</b> (200 x 200)</p>
                        <div class="object-btn-grp">
                            <h2><?php echo '$5000'; ?></h2>
                            <h3>Per Month</h3>
                            <a href="mailto:support@gizmoservers.com" target="_blank">
                                <button id="object-btn">I am Interested</button>
                            </a>
                        </div>
                    </div>
                </div>
<!--            </div>-->
        </div>
        <div class="last-text">
            <h2>Do you want <span>more</span> or a more <span>professional</span> package.</h2>
            <p>Feel free to chat with us or open a ticket</p>
            <a target="_blank" href="/product/minecraft-ssd"><button class="blue rounded">
                    Open a ticket
                </button>
            </a>
        </div>


        <div class="row specialist-faq">
            <div class="col-xs-12">
                <div class="accordion-item">
                    <button class='accordion'>
                        If I already have a team, what should I do?
                    </button>
                    <div class='panel'>
                        <p>
                            Great question. The point with those packages, is to let you hire our people to make an epic server.<br>
                            But if you already got a big or small team under you and want them to work on the technical parts then we can arrange something.<br>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="accordion-item">
                    <button class='accordion'>
                        Do you got more information on those packages?
                    </button>
                    <div class='panel'>
                        <p>
                            Yes and no, we will be trying our best to list every detail on those packages on this page.<br>
                            But for further questions we will ask you to hit us up with an email or open a ticket. Because we can't list everything in here unfortunately
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 center-xs">
                <a target="_blank"  href="<?= ROOT_URL ?>faq">
                    <button id="view-more_faq" class="blue rounded">
                        View more "Frequently Asked Questions"
                    </button>
                </a>
            </div>
        </div>
    </div>
</section>