<!doctype html>
<html>
    <head>
        <!-- Google Tag Manager -->
<!--        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':-->
<!--                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],-->
<!--                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=-->
<!--                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);-->
<!--            })(window,document,'script','dataLayer','GTM-MQVHNVM');</script>-->
        <!-- End Google Tag Manager -->
        <title>
            <?= TITLE; ?>
        </title>

        <link rel="canonical" href="<?= CAN_URL; ?>">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all">

        <meta name="description" content="<?= DESCRIPTION; ?>">
        <meta name="keywords" content="<?= KEYWORDS; ?>">
        <meta name="author" content="Gizmoservers">
        <meta name="Language" content="EN">

        <meta name="theme-color" content="#34495e">
        <meta name="msapplication-TileColor" content="#34495e">

        <meta property="og:site_name" content="Gizmoservers LLC | Minecraft Server Hosting"/>
        <meta property="og:url"
              content="<?= PAGE_URL; ?>" />
        <meta property="og:type"
              content="website" />
        <meta property="og:title"
              content="<?= TITLE; ?>" />
        <meta property="og:description"
              content="<?= DESCRIPTION; ?>" />
        <meta property="og:image"
              content="<?php echo ROOT; ?>images/gs_white_button_background.jpg" />

        <link rel="shortcut icon" type="image/png" sizes="32x32" href="<?php echo ROOT; ?>images/gs_white_button-500x500.png">

<!--        <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">-->
        <!--===============================================================================================-->
<!--        <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">-->

<!--        <link href="vendor/select2/select2.min.css" rel="stylesheet" />-->
<!--        <link href="--><?php //echo ROOT; ?><!--css/main.css" type="text/css" rel="stylesheet">-->
        <link rel="stylesheet" href="<?php echo ROOT; ?>includes/flexboxgrid-6.3.1/css/flexboxgrid.min.css" type="text/css">
<!--        <link href="--><?php //echo ROOT; ?><!--css/animate.css" type="text/css" rel="stylesheet">-->
        <link href="<?php echo ROOT; ?>css/style.min.css" type="text/css" rel="stylesheet">
<!--        <link href="https://cdnjs.cloudflare.com/ajax/libs/formjs/1.1.1/formjs.min.css" type="text/css" rel="stylesheet">-->
        <link href="<?php echo ROOT; ?>css/responsive.min.css" type="text/css" rel="stylesheet">
        <link href="<?php echo ROOT; ?>css/form.min.css" type="text/css" rel="stylesheet">
<!--        <link href="--><?php //echo ROOT; ?><!--lib/jquery-ui-1.12.1.custom/jquery-ui.min.css" type="text/css" rel="stylesheet">-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

        <script
                src="https://code.jquery.com/jquery-3.3.0.min.js"
                integrity="sha256-RTQy8VOmNlT6b2PIRur37p6JEBZUE7o8wPgMvu18MC4="
                crossorigin="anonymous">
        </script>
<!--        <script src="vendor/select2/select2.min.js"></script>-->
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-130644782-1');
        </script>

        <script type="text/javascript">
            var ROOT = '<?php echo ROOT; ?>';

            window.addEventListener("load", function(){
                window.cookieconsent.initialise({
                    "palette": {
                        "popup": {
                            "background": "#252e39"
                        },
                        "button": {
                            "background": "#14a7d0"
                        }
                    },
                    "theme": "classic",
                    "position": "bottom-right"
                })
            });

            (function(w,d,v3){
                w.chaportConfig = { appId : '5bccc77178e52a7dc1eb8868', appearance: { windowColor: "blue", teamName: "Gizmoservers Support" }};
                if(w.chaport)return;v3=w.chaport={};v3._q=[];v3._l={};v3.q=function(){v3._q.push(arguments)};v3.on=function(e,fn){if(!v3._l[e])v3._l[e]=[];v3._l[e].push(fn)};var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://app.chaport.com/javascripts/insert.js';var ss=d.getElementsByTagName('script')[0];ss.parentNode.insertBefore(s,ss)})(window, document);

            // (function(h,o,t,j,a,r){
            //     h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            //     h._hjSettings={hjid:1073468,hjsv:6};
            //     a=o.getElementsByTagName('head')[0];
            //     r=o.createElement('script');r.async=1;
            //     r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            //     a.appendChild(r);
            // })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');

            function myFunction() {
                var x = document.getElementById("myLinks");
                if (x.style.display === "block") {
                    x.style.display = "none";
                } else {
                    x.style.display = "block";
                }
            }
        </script>

        <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "BreadcrumbList",
          "itemListElement": [{
            "@type": "ListItem",
            "position": 1,
            "name": "Gizmoservers",
            "item": "https://gizmoservers.com"
          }
          <?php if (SHORT_NAME != 'Index'): ?>
          ,{
            "@type": "ListItem",
            "position": 2,
            "name": "<?= SHORT_NAME; ?>",
            "item": "<?= PAGE_URL; ?>"
          }]
          <?php else: ?>
          <?= "]" ?>
          <?php endif; ?>
        }
        </script>

    </head>
    <style>

    </style>
    <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MQVHNVM"
                      height="0" width="0" style="display:none;visibility:hidden">
        </iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- NORMAL NAVIGATION-->
    <header id="site-header">
        <div class="topnav-links">
            <div id="myLinks">
                <div class="row">
                    <div class="col-xs-12" style="padding-right: 0;">
                        <a title="Minecraft SSD Servers" href="<?= ROOT_URL ?>minecraft-ssd-servers" class="mobile-nav-item">
                            Minecraft Servers
                        </a>
                    </div>
                    <div class="col-xs-12" style="padding-right: 0;">
                        <a title="Hire a specialist" href="<?= ROOT_URL ?>hire-a-minecraft-server-specialist" class="mobile-nav-item">
                            Hire A Specialist
                        </a>
                    </div>
                    <div class="col-xs-12" style="padding-right: 0;">
                        <a rel="nofollow" title="Gizmoservers Support Tickets" href="https://billing.gizmoservers.com/supporttickets.php" target="_blank" class="mobile-nav-item">
                            Tickets
                        </a>
                    </div>
                    <div class="col-xs-12" style="padding-right: 0;">
                        <a  title="Gizmoservers F.A.Q." href="<?= ROOT_URL ?>frequently-asked-questions" class="mobile-nav-item">
                            F.A.Q.
                        </a>
                    </div>
                    <div class="col-xs-12" style="padding-right: 0;">
                        <a rel="nofollow" title="Gizmoservers Control Panel" href="https://panel.gizmoservers.com/index.php?r=site/page&view=home" target="_blank" class="mobile-nav-item">
                            Game Panel
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <a title="Gizmoservers" href="<?php echo ROOT_URL; ?>"><img alt="GizmoServers Logo" src="<?php echo ROOT; ?>images/logo_white-400x160.png" id="logo"></a>

            <!-- MOBILE NAVIGATION-->
            <div class="topnav">
                <a id="do-not-click" href="javascript:void(0);" class="icon hamburder-active" onclick="myFunction()">
                    <i id="do-not-click" class="fa fa-bars"></i>
                </a>
            </div>

            <nav>
                <ul>
                <?php $collections = Collection::all(); ?>

                    <?php foreach($collections as $collection): ?>
                        <?php $id = $collection->getId(); ?>
                        <?php $name = $collection->getName(); ?>
                        <?php $showInHeader = $collection->getShowInHeader(); ?>
                        <?php $categories = Category::all($id); ?>

                        <?php if ($showInHeader == '1'): ?>
                            <?php if (count($categories) == 1): ?>
                                <?php $category = $categories[0]; ?>
                                <?php $categoryID = $category->getId(); ?>
                                    <li>
                                        <a
                                            title="Gizmoservers Minecraft Servers"
                                            class="header-service"
                                            href='<?= ROOT_URL; ?>minecraft-ssd-servers'
                                        >
                                            <?= $name; ?>
                                        </a>
                                    </li>
                                <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <li>
                        <a title="Gizmoservers Hire a Specialist" href="<?php echo ROOT_URL; ?>hire-a-minecraft-server-specialist">Hire a specialist</a>
                    </li>
                    <li class="dropdown-icon">
                        <div class="">
                            <a href="#" class="dropdown-controller">SUPPORT<i class="fas fa-caret-down"></i></a>
                            <div class="dropdown-content">
                                <ul id="dropdown">
                                    <li>
                                        <a rel="nofollow" title="Gizmoservers Tickets" href="https://billing.gizmoservers.com/supporttickets.php" target="_blank">TICKETS</a>
                                    </li>
                                    <li>
                                        <a title="Gizmoservers F.A.Q." href="<?php echo ROOT_URL; ?>frequently-asked-questions">FAQ</a>
                                    </li>
<!--                                    <li>-->
<!--                                        <a title="Gizmoservers Contact" href="--><?php //echo ROOT_URL; ?><!--index">CONTACT</a>-->
<!--                                    </li>-->
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown-icon">
                        <a href="#">PANELS<i class="fas fa-caret-down"></i></a>
                        <ul id="dropdown">
                            <li>
                                <a rel="nofollow" title="Gizmoservers Client Panel" href="https://billing.gizmoservers.com/clientarea.php" target="_blank">CLIENT PANEL</a>
                            </li>
                            <li>
                                <a rel="nofollow" title="Gizmoservers Multicraft" href="https://panel.gizmoservers.com/" target="_blank">GAME PANEL</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a rel="nofollow" title="Gizmoservers Client Panel" href="https://billing.gizmoservers.com/clientarea.php" target="_blank" id="client_area_btn"><button class="white">CLIENT AREA</button></a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>