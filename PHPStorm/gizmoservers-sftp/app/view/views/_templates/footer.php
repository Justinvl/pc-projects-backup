<?php

  $div = '<div class="col-lg-4 col-sm-6 col-xs-12" style="padding-right: 1rem">';

?>

<footer>
    <div class="row">
        <div class="col-lg-3 col-sm-3 col-xs-12">
            <div class="widget2" style="padding-top: 0 !important;">
                <img src="<?php echo ROOT; ?>images/gs_white_button-500x500.png" height="100" width="100">
                <div class="social-media-footer">
                    <ul class="social-media-icons">
                        <li><a rel="nofollow" title="Gizmoservers Twitter" href="https://twitter.com/gizmoserversllc" target="_blank" class="social-media-btn"><button><i class="fab fa-twitter"></i></button></a></li>
                        <li><a rel="nofollow" title="Gizmoservers Facebook" href="https://www.facebook.com/gizmoservers" class="social-media-btn" target="_blank"><button><i class="fab fa-facebook-f"></i></button></a></li>
                        <li><a rel="nofollow" title="Gizmoservers Discord" href="https://discord.gg/52wD3Py" target="_blank" class="social-media-btn"><button><i class="fab fa-discord"></i></button></a></li>
                        <li><a rel="nofollow" title="Gizmoservers Youtube" href="https://www.youtube.com/channel/UComUiimpzJeLIUwh59TsoUg" target="_blank" class="social-media-btn"><button><i class="fab fa-youtube"></i></button></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-sm-9 col-xs-12">
        <div class="row">
            <?= $div ?>
                <div class="widget">
                    <h4>
                        Gizmoservers
                    </h4>
                    <a rel="nofollow" title="Gizmoservers Affiliates" href="https://billing.gizmoservers.com/affiliates.php" target="_blank">Affiliates</a>
                    <a title="Gizmoservers Partnership" href="<?= ROOT_URL; ?>become-a-partner">Partnership</a>
                    <a title="Gizmoservers Partners" href="<?= ROOT_URL; ?>become-a-partner">Partners</a>
                    <a title="Gizmoservers About" href="<?= ROOT_URL; ?>about">About</a>
                    <a title="Gizmoservers News & Press" href="<?= ROOT_URL; ?>">News & Press</a>
                    <a title="Gizmoservers Jobs" href="<?= ROOT_URL;?>jobs">Jobs</a>
                </div>
            </div>
            <?= $div ?>
                <div class="widget">
                    <h4>
                        Support
                    </h4>
                    <a title="Gizmoservers F.A.Q." href="<?php echo ROOT_URL;?>frequently-asked-questions">F.A.Q.</a>
                    <a rel="nofollow" title="Multicraft Guide" href="http://multicraft.org/site/userguide/index" target="_blank">Multicraft Guide</a>
                    <a rel="nofollow" title="Gizmoservers Tickets" href="https://billing.gizmoservers.com/supporttickets.php" target="_blank">Tickets</a>
                    <a rel="nofollow" title="Gizmoservers Knowledgebase" href="https://billing.gizmoservers.com/index.php?rp=/knowledgebase" target="_blank">Knowledgebase</a>
                    <a rel="nofollow" title="Gizmoservers Technical Issue" href="https://billing.gizmoservers.com/supporttickets.php" target="_blank">Technical Issues</a>
                    <a rel="nofollow" title="Gizmoservers Billing Issue" href="https://billing.gizmoservers.com/supporttickets.php" target="_blank">Billing Issues</a>
                </div>
            </div>
            <?= $div ?>
                <div class="widget">
                    <h4>
                        Services
                    </h4>
                    <?php
                    $collections = Collection::all();

                    foreach($collections as $collection)
                    {
                        $id = $collection->getId();
                        $name = $collection->getName();
                        $categories = Category::all($id);

                        if(count($categories) == 1)
                        {
                            $category = $categories[0];
                            $categoryID = $category->getId();?>

                            <a title="Minecraft Servers" href='<?php echo ROOT_URL; ?>minecraft-ssd-servers' class="new"><?php echo $name; ?>

                                <?php if($name == "Minecraft"){ ?>
                                    <span class="badge badge-info">Popular</span>
                                <?php } ?>
                            </a>

                        <?php }
                        else{ ?>
                            <a <?php if ($id == "teamvisionary"){echo 'href="https://teamvisionary.net/" target="_blank"';} ?> href='<?php echo ROOT_URL; ?>collection/<?php echo $id; ?>'><?php echo $name; ?></a>

                            <?php if($name == "Minecraft"){ ?>
                                <span class="badge badge-info">Popular</span>
                            <?php } ?>

                            <?php ;
                        }
                    }
                    ?>
                    <a href="<?php echo ROOT_URL;?>hire-a-minecraft-server-specialist">Hire A Specialist</a>
                    <a rel="nofollow" href="https://teamvisionary.net/" target="_blank">Professional Minecraft Builds</a>
                </div>
            </div>
            <?= $div ?>
                <div class="widget">
                    <h4>
                        Legal
                    </h4>
                    <a title="Gizmoservers Privacy Policy" href="<?php echo ROOT_URL;?>terms">Privacy Policy</a>
                    <a title="Gizmoservers Terms of service" href="<?php echo ROOT_URL;?>terms">Terms of service</a>
                    <a rel="nofollow" title="Minecraft EULA" href="https://account.mojang.com/documents/minecraft_eula" target="_blank">Minecraft EULA</a>
                    <a title="Gizmoservers GDPR & AVG" href="<?php echo ROOT_URL;?>terms" target="_blank">GDPR & AVG</a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-12">
                <div class="widget">
                    <h4>
                        Panels
                    </h4>
                    <a rel="nofollow" title="Gizmoservers Multicraft" href="https://panel.gizmoservers.com/index.php?r=site/page&view=home" target="_blank">Game Panel</a>
                    <a rel="nofollow" title="Gizmoservers Client Dashboard" href="https://billing.gizmoservers.com/clientarea.php" target="_blank">Client Dashboard</a>
                    <a rel="nofollow" title="Gizmoservers Ticket Panel" href="https://billing.gizmoservers.com/supporttickets.php" target="_blank">Ticket Panel</a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-12 badges">
                <img src="<?php echo ROOT; ?>images/partners/minecraft-90x100.png" height="90" width="90">
                <img src="<?php echo ROOT; ?>images/gdpr.png" height="100" width="100"><br>
                <img src="<?php echo ROOT; ?>images/trusted-300x70.png" height="60" width="250">
                <div class="reviews" itemprop="review" itemscope itemtype="http://schema.org/Review">
                    <!-- DATA FROM https://www.trustedsite.com/insights/ -->
                    <div class="center" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<section id="copy">
    &copy; Gizmoservers LLC 2019 | Minecraft Server Hosting |  All right reserved
</section>
<!--<script src="--><?php //echo ROOT; ?><!--lib/jquery-ui-1.12.1.custom/jquery-ui.min.js" defer></script>-->
<!--<script src="--><?php //echo ROOT; ?><!--lib/jquery-ui-1.12.1.custom/jquery-ui.min.js" defer></script>-->

<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/formjs/1.1.1/formjs.min.js"></script>-->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130644782-1"></script>

<!--<script src="vendor/animsition/js/animsition.min.js"></script>-->
<script src="<?php echo ROOT; ?>js/main.js" defer></script>
<script src="<?php echo ROOT; ?>js/form.js" defer></script>