<section id="home">
    <div class="bg-image"></div>
    <section id="topper" class="home">
        <div class="container">
            <div id="left" class="object animated zoomIn">
                <h1 id="hero-text" style="text-transform: uppercase !important;">
                    Minecraft<br>
                    Server<br>
                    Hosting
                </h1>
                <p>
                    Your own Minecraft server up in several minutes.
                </p>
                <div class="row">
                    <div class="col-lg-5 col-xs-12">
                        <a title="Hire a Minecraft server" target="_blank" href="<?= ROOT_URL ?>minecraft-ssd-servers">
                            <button class="blue">Buy a server</button>
                        </a>
                    </div>
<!--                    <div class="col-lg-3 col-xs-12">-->
<!--                        <button class="white">ABOUT</button>-->
<!--                    </div>-->
                </div>
            </div>
            <div class="index-steve">
                <img alt="Gizmoservers Steve" src="<?= ROOT ?>images/3d/3.png">
            </div>
<!--            <p id="down">-->
<!--                <a href="#">&#11167;</a>-->
<!--            </p>-->
        </div>
    </section>
    <section class="shaped-divider">
        <svg viewBox="0 0 100 100" preserveAspectRatio="none" class="hero-divider">
            <polygon class="svg--sm" fill="white" points="0,0 30,100 65,21 90,100 100,75 100,100 0,100"/>
            <polygon class="svg--lg" fill="white" points="0,0 15,100 33,21 45,100 50,75 55,100 72,20 85,100 95,50 100,80 100,100 0,100" />
        </svg>
    </section>
    <section class="features">
        <div class="">
            <?php $div = '<div class="col-lg-3 col-md-4 col-xs-12">'; ?>
            <div id="features">
                <div class="row">

                    <div class="col-xs-12">
                        <h2 style="margin-top: 60px" class="small-text text-centered">The journey to<br><span class="bold-text">your own server</span></h2>
                        <p class="company-info extra-text black text-justified">
                            Gizmo Servers is a reliable Minecraft server hosting provider and a one stop solution to your needs.
                            Our operations are expanded to provide you a competitively priced Minecraft server, dedicated server or modpack server.
                        </p>
                    </div>

                    <?= $div; ?>
                        <div class="feature">
                            <h3>
                                <i class="fas fa-comments fa-3x"></i><br>
                                SUPPORT
                            </h3>
                            <p>Gizmoservers provides fast, friendly, and professional support from our trained staff with fast response times.</p>
                        </div>
                    </div>
                    <?= $div; ?>
                        <div class="feature foreground">
                            <h3>
                                <i class="fas fa-server fa-3x margin-20"></i><br>
                                Powerful Hardware
                            </h3>
                            <p>
                                We use cutting edge i7 gaming CPU and blazing fast solid state drives providing lightning fast servers.
                            </p>
                        </div>
                    </div>
                    <?= $div; ?>
                        <div class="feature foreground">
                            <h3>
                                <i class="fab fa-get-pocket fa-3x margin-20"></i><br>
                                Instant Setup
                            </h3>
                            <p>
                                Get your server within a matter of seconds after your purchase. No waiting, you can start immediately.
                            </p>
                        </div>
                    </div>
                    <?= $div; ?>
                        <div class="feature">
                            <h3>
                                <i class="fas fa-magic fa-3x margin-20"></i><br>
                                Automated Backups
                            </h3>
                            <p>
                                We externally backup all our minecraft servers so you can rest assured your data will never get lost.
                            </p>
                        </div>
                    </div>
                    <?= $div; ?>
                        <div class="feature">
                            <h3>
                                <i class="fas fa-gamepad fa-3x margin-20"></i><br>
                                Easy Controls
                            </h3>
                            <p>
                                The control panel is not only powerful, it is also easy to use for people without any type of experience.
                            </p>
                        </div>
                    </div>
                    <?= $div; ?>
                        <div class="feature">
                            <h3>
                                <i class="fas fa-copy fa-3x margin-20"></i><br>
                                Free FTP Access
                            </h3>
                            <p>
                                Access your files to customize to your hearts content.
                            </p>
                        </div>
                    </div>
                    <?= $div; ?>
                        <div class="feature">
                            <h3>
                                <i class="fas fa-mosque fa-3x"></i><br>
                                Hosting with quality
                            </h3>
                            <p>
                                We are a server hosting that stands for quality. Nothing special.
                            </p>
                        </div>
                    </div>
                    <?= $div; ?>
                        <div class="feature">
                            <h3>
                                <i class="fas fa-cube fa-3x margin-20"></i><br>
                                One Click Modpacks
                            </h3>
                            <p>
                                Install and play the best modpacks within seconds with one click installation.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="features-steve">
                    <img alt="Gizmoservers Steve" src="<?php echo ROOT ?>images/3d/4-900x500.png">
                </div>
            </div>
        </div>
    </section>
    <section id="information">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 center-lg">
                    <h2 style="margin-top: 60px" class="small-text text-centered">Our path to<br><span class="bold-text">success</span></h2>
                    <p class="panel-info extra-text text-justified">
                        We as a Minecraft server host will provide you with <b class="breset">100% uptime</b>, which only meets the highest requirements.
                        In addition, we also maintain an <b class="breset">affordable price</b> and are committed to raise the bar in the Minecraft server
                        hosting industry, with <b class="breset">no drawbacks and errors</b>. Our <a href="https://gizmoservers.com/frequently-asked-questions" style="color: #0e90dd">customer care</a>
                        staff is a group of <b class="breset">experienced professionals</b>, who can ensure the best experience for every client. One
                        of our prime priorities is to provide gamers with a great performance. This is not our marketing and promotion
                        strategy in fact it is <b class="breset">our quality of performance</b>.
                    </p>
                    <p class="panel-info extra-text text-justified">
                        We provide our valued clients with <b class="breset">Lightning fast Solid State Drives (SSD)</b> that ensure
                        guaranteed and accelerated Minecraft world loading. After finalizing your order, we deliver the
                        server <b class="breset">instantly</b>. Each Minecraft server is protected by our <b class="breset">480Gbps DDoS protection</b>. More over, these
                        servers have automatic mitigation systems that allow us to ensure unbeatable percentage uptime with DDoS Protection.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section id="extras">
        <div class="container">
<!--            <ul>-->
<!--                <li>Install the popular modpacks like FTB and Tekkit in one click</li>-->
<!--                <li>All our servers are 100% protected against all DDOS attacks</li>-->
<!--                <li>More than just support, we'll 24/7 support to all our services </li>-->
<!--                <li>After you paid your server, it will be up and running instantly</li>-->
<!--                <li>You can access the files with our FTP management</li>-->
<!--                <li>Do not worry about lost files. We backup everything each day</li>-->
<!--                <li>You can easily manage your server with our user friendly control panel</li>-->
<!--            </ul>-->
            <div class="row">
                <div class="col-lg-5 col-md-5 col-xs-12 center-lg">
                    <h2 class="small-text">Control your server with our<br><span class="bold-text">Control Panel</span></h2>
                    <div class="extra-field">
                        <ul>
                            <li>One click backup's whenever you want</li>
                            <li>Install your custom jar's easily</li>
                            <li>Responsive design. Mobile friendly</li>
                            <li>BungeeCord friendly</li>
                            <li>Supports multiple servers</li>
                            <li>Easy to use console</li>
                            <li>Control multiple minecraft servers</li>
                        </ul>
                    </div>
                    <div class="button-field">
                        <a title="Hire a Minecraft Server" href="<?= ROOT_URL ?>minecraft-ssd-servers"><button class="white rounded left">
                                Choose your package
                            </button></a>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-xs-12">
                    <div class="multicraft-showmodal">
                        <img alt="Multicraft Panel" src="<?php echo ROOT; ?>images/multicraft_panel.png">
                    </div>
                </div>
                <div class="col-xs-12">
                    <h3 style="margin-top: 60px" class="small-text text-centered">But we got<br><span class="bold-text">More</span></h3>
                    <p class="panel-info extra-text text-justified">
                        Our Minecraft servers are also powered by the easy-to-use Multicraft (2.0) control panel.
                        This helps our clients with a powerful as well as a simple control solution their servers.<br>
                        <b class="breset">Multi-User Minecraft Server Hosting</b> is also provided, as you don’t play alone. Managing your servers
                        shouldn’t be done alone, so our control panel allows you <b class="breset">multiple accounts</b>. This way, others
                        can also have the access to the control panel. In order to keep your Minecraft hosting latency at
                        its lowest, the servers will be connected to the internet through <b class="breset">premium bandwidth</b> carriers and 1Gbps connections.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section id="partners">
<!--        <div class="center">-->
            <div class="row center-xs">
<!--                <ul>-->
    <!--            <div class="col-lg-2 col-md-5 col-xs-12 center-lg"></div>-->
                <?php $partners = ['mollie-200x50' => '160', 'buycraft' => '160', 'multicraft' => '200', 'paypal-200x50' => '150'] ?>

                <?php foreach($partners as $image => $width) { ?>
                    <?php $image_src = ROOT . "images/partners/" . $image . '.png'; ?>
    <!--                <div class="col-lg-2 col-md-5 col-xs-12 center-lg">-->
                    <div class="col-lg-2 col-xs-12">
                        <img alt="Partner <?= $image?>" class="greyscale" src="<?php echo $image_src; ?>" width="<?php echo $width; ?>" height="60">
                    </div>
                <?php } ?>
<!--                </ul>-->
            </div>
<!--        </div>-->
    </section>
    <section id="google-reviews">
        <div class="container">

            <img alt="Google Logo" src="<?php echo ROOT; ?>images/google-200x200.png" alt="" height="150px">
            <h2>Let us know your experience</h2>
            <a rel="nofollow" target="_blank" id="google-review-btn" href="https://www.google.com/search?ludocid=17407866934760304284&q=GizmoServers%20LLC&_ga=2.186529513.1932934499.1539984728-1582218365.1533461408#fpstate=lie&lrd=0x887f8f9bc9ea84cb:0xf1952abf0fa7ee9c,3,,,"><button class="blue rounded">
                Write a google review
                </button>
            </a>
        </div>
    </section>
    <div style="width: 100%; border: 0">

    </div>
</section>