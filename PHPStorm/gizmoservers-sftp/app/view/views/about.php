<?php
/**
 * Created by PhpStorm.
 * User: justi
 * Date: 3/4/2019
 * Time: 9:33 PM
 */

?>



<section id="sub-header">
    <div class="container relative">
        <div class="row">
            <div class="col-xs-12 center-xs">
                <div class="breadcrumb">
                    <div class="breadcrumb-item">
                        GizmoServers
                    </div>
                    <div class="breadcrumb-item">
                        About
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="sub-header-title">
                    <h1>About us</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="about">
    <div class="container">
        <div id="topper" style="height: 100%;margin-top: 0px !important;">
            <div class="row content" style="margin-right: 0">
                <div class="col-lg-12 col-xs-12">
                    <p style="font-size: 20px;">GizmoServers was founded in 2012 as a project of passion.
                        We started out with very modest beginnings.
                        Our goal was to provide affordable server hosting to the masses to help grow the Minecraft community,
                        and facilitate a respect for the emerging server community.  As time passed, we grew with community,
                        providing new products, services to help keep updated with the demands of the Minecraft marketplace.
                        To this day, we strive to stand out amongst the competition with our top rated server hardware,
                        excellent customer service, and many years of professional experience in the industry.
                        We value all of our clients, and are so glad you have chosen us!
                    </p>
                    <hr style="margin: 20px 0 20px 0">
                    <p style="font-size: 20px;">
                        Rent the best Minecraft servers with the cheapest prices and best customer service available.
                        We offer MCPE server hosting as well, giving you options that other companies don’t always offer.
                        GizmoServers is rated as the #1 Top Minecraft and MCPE server hosting sites on the net.
                        We also offer amazing high quality Minecraft mod support at no additional cost.
                        We will host Hytale servers for you at insanely good prices.
                        Hytale server hosting and management is our new priority in the ver expanding server community.
                        We pride ourselves in being the #1 Hytale server hosting company and build team,
                        offering our services to people around the world.  Using only state-of-the-art technology,
                        our Minecraft MCPE and Hytale servers will never lag.  Use our robust panel to gain access
                        to your files and customize your server the way you want.
                    </p>
                    <?= $no_message; ?>
                </div>
                <div class="col-xs-12">
                    <img src="<?php echo ROOT?>images/Creeper-Peeking-Through-1-300x288.png" alt="Creeper Image" style="margin-top: 50px">
                </div>
            </div>
        </div>
    </div>
</section>