<?php

class View
{

    private $file;
    private $args;
    private $title;
    private $description;
    private $keywords;

    public function __construct($file,
                                $title,
                                $keywords = "Rent Minecraft server, Minecraft Server Hosting, Cheap MCPE Server Hosting, Best Minecraft Pocket Edition Server Hosting, Best Minecraft Server Host, Minecraft Server, Modded Minecraft server host",
                                $description = "Begin your own Minecraft Server at GizmoServers. We are one of the cheapest minecraft server hosting companies out there.. It doesn't matter if you want a Spigot, Bukkit or modpack server. We got it. Just come and take a look..",
                                $args = [])
    {
        $this->file = $file;
        $this->args = $args;
        $this->title = $title;
        $this->keywords = $keywords;
        $this->description = $description;
    }

    public function getArgs()
    {
        return $this->args;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function render()
    {
        $database = new Database();
        $is_html = false;

        define("TITLE", $this->title);
        define("KEYWORDS", $this->keywords);
        define("SHORT_NAME", ucwords(str_replace('_', ' ', str_replace("ssd", "SSD", $this->file))));
        define("DESCRIPTION", $this->description);

        if (strpos($this->file, "index")){
            define("PAGE_URL", ROOT_URL);
        }else{
            define("PAGE_URL", ROOT_URL.$this->file);
        }

        include('views/_templates/header.php');

        if(!Config::$maintenance)
        {
            include("views/$this->file.php");
        }
        else
        {
            include('views/maintenance.php');
        }

        include('views/_templates/footer.php');
    }
}