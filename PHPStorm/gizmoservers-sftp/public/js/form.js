$(document).ready(function(){

    $('.slide_item').each(function () {
        $(this).slideToggle();
    });


    var previous_sec = null;

    $('#partner-job-select').change(function () {
        var val = $(this).val();
        console.log(val);
        if (previous_sec != null) {
            $('#slide_' + previous_sec).slideUp(700, function () {
                $('#slide_' + val).slideDown();
                previous_sec = val;
            });
        }else{
            console.log('#slide_'+val);
            $('#slide_' + val).slideDown(700, function () {
                previous_sec = val;
            });
        }
    });

    // Validation
    $('.form-input').each(function () {
        console.log('1');
        $(this).on('propertychange input', function (e) {

            var type = $(this).attr('type');
            var val = $(this).val();
            var success = false;

            switch (type) {
                case 'text':
                    if (val.left > 2){
                        success = true;
                    }
                    break;
                case 'email':
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                    if (regex.test(val)) {
                        success = true;
                    }
                    break;
                case 'url':
                    var regex = /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;

                    if (regex.test(val)) {
                        success = true;
                    }
                    break;
            }

            if (success){
                $(this).removeClass('error');
                $(this).addClass('success');
            }else{
                $(this).addClass('error');
                $('#partner-apply-submit').addClass('disabled');
            }

            if (val == null || val == ""){
                $(this).removeClass('error');
                $(this).removeClass('success');
            }
        });
    });



});