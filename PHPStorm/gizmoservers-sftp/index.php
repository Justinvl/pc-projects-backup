<?php
/**
 * Created by PhpStorm.
 * User: justi
 * Date: 8-11-2018
 * Time: 21:55
 */
?>

<?php
require('app/App.php');
require('app/controller/Controller.php');
require('app/view/View.php');

require('Config.php');
require('app/modules/Database.php');
require('app/modules/Collection.php');
require('app/modules/Category.php');
require('app/modules/Product.php');
require('app/modules/Faq.php');

session_start();
define('ROOT', Config::$root);
define('ROOT_URL', str_replace('public/', '', Config::$root));

$app = new App();
